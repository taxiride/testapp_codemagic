import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/models/passager.dart';
import 'package:taxiride_ios/screens/carpooling/card_passager.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:taxiride_ios/widget/layout/filter_box.dart';

import 'card_trip.dart';
import 'filter_screen.dart';

class FindPassagerScreen extends StatefulWidget{
  final List<Passager> passagers;

  const FindPassagerScreen({Key key, this.passagers}) : super(key: key);

  _FindPassagerState createState() => _FindPassagerState();

}

class _FindPassagerState extends State<FindPassagerScreen>{
  @override
  Widget build(BuildContext context) {

    return Container(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: (){
            Navigator.pop(context);
          }),
          title: Text("FIND PASSAGERS",style: TextStyle(color: Colors.black),),
          backgroundColor: Colors.white,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FilterBox(
                  label: "FILTER STATUS",
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute( builder: (_) => FilterScreen(titre: "TRIP INVITATIONS",)));
                  }),
            ),
            Container(
                height: MediaQuery.of(context).size.height*0.64,
                width: MediaQuery.of(context).size.width,
                child: (widget.passagers.length==0)?Center(
                  child: Text("No Passagers Found",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold,fontSize: 16),),
                ):ListView.builder(
                    itemCount: (widget.passagers==null)?0:widget.passagers.length,
                    itemBuilder: (context,index){
                      Passager pass = widget.passagers[index];
                      return CardPassager(args: pass,);
                    })
            ),
            Container(
              width: MediaQuery.of(context).size.width*0.8,
              padding: EdgeInsets.all(10),
              child: BlackButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(Icons.refresh,color: Colors.white,),
                    Text("REFRESH FEED",style: TextStyle(color: Colors.white,),)
                  ],
                ),
                onPressed: (){},
              ),
            )
          ],
        ),
      ),
    );

  }

}