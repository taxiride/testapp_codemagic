import 'package:flutter/material.dart';

import 'base_button.dart';

class SecondaryButton extends BaseButton{

  final Function onPressed;
  final Widget child;
  final String text;
  final EdgeInsets padding;

  SecondaryButton({this.padding, @required this.onPressed, this.child, this.text})
      : super(
      padding: padding,
      onPressed: onPressed,
      color: Colors.red,
      text: text,
      child: child);

}