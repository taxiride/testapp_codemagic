import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';

class FindAllPendingTravelOrBookingRequest extends AbstractAuthenticatedRequest{
  final String token;
  final String emailOrOhone;
  final String language;

  FindAllPendingTravelOrBookingRequest(this.token, this.emailOrOhone, this.language):super(token);

  @override
  String getUrl() {
    return "/api/carpooling/findAllPendingTravelOrBooking/$emailOrOhone/$language";
  }

  @override
  String getVerb() {
    // TODO: implement getVerb
    return "GET";
  }

  @override
  Map<String, String> getParams() {
    // TODO: implement getParams
    return {
      "emailOrPhone":emailOrOhone,
      "language":language
    };
  }



  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;

    if(response.httpResponse.statusCode==200){
      print(jsonDecode(data));
      print("------- tout c est bient passe -------------");
      List<PoolingTrip> pooling = PoolingTrip.listFromJson(jsonDecode(data)["object"]);
      return pooling;
    }else{
      print(jsonDecode(data));
      print("------- la requete a echouer-------------");
      return [];

    }
  }


}