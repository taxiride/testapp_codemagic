import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/profile.dart';

class SaveProfilPersonnalInformation extends AbstractAuthenticatedRequest{
  final Profil profil;
  final String token;

  SaveProfilPersonnalInformation(this.profil, this.token):super(token);

  @override
  String getUrl() {
    return "/api/profile/saveProfilePersonnalInformation";
  }

  @override
  String getVerb() {
    return "POST";
  }

  @override
  getBody() {
    return {
      "address": profil.address,
      "birthDate": profil.birthDate,
      "city": profil.city,
      "codeTripOption":profil.codeTripOption,
      "country": profil.country,
      "defaultRequestRadius": profil.defaultRequestRadius,
      "defaultTravelOption": profil.defaultTravelOption??"VIP",
      "driverOperatingCityCode": profil.driverOperatingCityCode,
      "driverOperatingCountryCode": profil.driverOperatingCountryCode,
      "email": profil.email,
      "externalReferalCode": profil.externalReferalCode,
      "firstName": profil.firstName,
      "gender": profil.gender??"M",
      "id": profil.id,
      "language": profil.language??"fr",
      "lastName": profil.lastName,
      "minimalNotificationDistance": profil.minimalNotificationDistance??1,
      "phoneNumber": profil.phoneNumber,
      "profession": profil.profession??"",
      "referalCode": profil.referalCode,
      "referalCodeUsed": profil.referalCodeUsed,
      "status": profil.status,
      "subscribeToEmail": profil.subscribeToEmail??false,
      "subscribeToPush": profil.subscribeToPush??false,
      "subscribeToSMS": profil.subscribeToSMS??false,
      "userId": profil.userId
    };

  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    print("------------------- voici le statut code ------------------------");
    print(response.httpResponse.statusCode);
    print(data);

  }


}