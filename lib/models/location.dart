class Location{
  String country;
  String countryCode;
  String city;
  double lat;
  double lon;

  Location({this.country,this.countryCode,this.city,this.lat,this.lon});

  Location.fromJson(Map<String, dynamic> json){
    country = json['country'];
    countryCode = json['countryCode'];
    city = json['city'];
    lat = json['lat'];
    lon = json['lon'];
  }
}