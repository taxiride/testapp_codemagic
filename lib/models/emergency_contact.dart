class EmergencyContact{
  int contactType;
  int id;
  String language;
  String name;
  String phone;
  String phoneNumber;
  int userId;

  EmergencyContact({
    this.contactType,
    this.id,
    this.language,
    this.name,
    this.phone,
    this.phoneNumber,
    this.userId
});

  EmergencyContact.fromJson(Map<String, dynamic> json){
    contactType = json['contactType'];
    id = json['id'];
    language = json['language'];
    name = json['name'];
    phone = json['phone'];
    phoneNumber = json['phoneNumber'];
    userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['contactType'] = this.contactType;
    data['id'] = this.id;
    data['language'] = this.language;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['phoneNumber'] = this.phoneNumber;
    data['userId'] = this.userId;
    return data;

  }

  static List<EmergencyContact> listFromJson(List<dynamic> json) {
    return List<EmergencyContact>.from(json.map((v) => EmergencyContact.fromJson(v)));
  }


  }