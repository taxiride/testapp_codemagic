class PoolingTrip{
  int id;
  String countryCode;
  String departureCity;
  String arrivalCity;
  double defaultTravelCost;
  int defaultTravelDuration;
  int defaultTravelDistance;
  String travelDate;
  int nbOfInvitations;
  int status;

  PoolingTrip({this.id,
    this.countryCode,
    this.departureCity,
    this.arrivalCity,
    this.defaultTravelCost,
    this.defaultTravelDuration,
    this.defaultTravelDistance,
    this.travelDate,
    this.nbOfInvitations,
    this.status
    });

  PoolingTrip.fromJson(Map<String, dynamic> json){
    id = json['id'];
    countryCode = json['countryCode'];
    departureCity = json['departureCity'];
    arrivalCity = json['arrivalCity'];
    defaultTravelCost = json['defaultTravelCost'];
    defaultTravelDuration = json['defaultTravelDuration'];
    defaultTravelDistance = json['defaultTravelDistance'];
    travelDate = json['travelDate'];
    nbOfInvitations = json['nbOfInvitations'];
    status = json['status'];

  }

  static List<PoolingTrip> listFromJson(List<dynamic> json) {
    return List<PoolingTrip>.from(json.map((v) => PoolingTrip.fromJson(v)));
  }
}