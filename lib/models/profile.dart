import 'package:flutter/cupertino.dart';

class Profil{
  String createUser;
  String updateUser;
  int id;
  String email;
  String birthDate;
  String firstName;
  String lastName;
  String defaultTravelOption;
  bool active;
  String name;
  String imageUrl;
  String provider;
  String phoneNumber;
  String profession;
  String address;
  int userId;
  bool subscribeToEmail;
  String externalReferalCode;
  double minimalNotificationDistance;
  double latitude;
  double longitude;
  bool subscribeToPush;
  String emailOrPhone;
  String nomLieu;
  String language;
  bool subscribeToSMS;
  String city;
  String defaultRequestRadius;
  String driverOperatingCountryCode;
  String driverOperatingCityCode;
  String codeTripOption;
  String gender;
  String referalCode;
  bool referalCodeUsed;
  int status;
  String verificationCode;
  String contryCode;
  double newBalance;
  bool isRefreshActive;
  String defaultPaymentmode;
  bool refreshActive;
  String country;

  Profil({
    this.createUser,
    this.lastName,
    this.active,
    this.updateUser,
    this.id,
    this.email,
    this.firstName,
    this.birthDate,
    this.name,
    this.imageUrl,
    this.provider,
    this.phoneNumber,
    this.subscribeToSMS,
    this.externalReferalCode,
    this.userId,
    this.minimalNotificationDistance,
    this.profession,
    this.address,
    this.latitude,
    this.longitude,
    this.emailOrPhone,
    this.nomLieu,
    this.subscribeToEmail,
    this.language,
    this.defaultTravelOption,
    this.defaultRequestRadius,
    this.city,
    this.subscribeToPush,
    this.driverOperatingCityCode,
    this.driverOperatingCountryCode,
    this.codeTripOption,
    this.gender,
    this.referalCode,
    this.referalCodeUsed,
    this.status,
    this.verificationCode,
    this.contryCode,
    this.newBalance,
    this.isRefreshActive,
    this.defaultPaymentmode,
    this.refreshActive,
    this.country});

  Profil.fromJson(Map<String, dynamic> json){
    createUser = json['createUser'];
    lastName = json['lastName'];
    active = json['active'];
    updateUser = json['updateUser'];
    id = json['id'];
    email = json['email'];
    firstName = json['firstName'];
    name = json['name'];
    birthDate = json['birthDate'];
    imageUrl = json['imageUrl'];
    provider = json['provider'];
    userId = json['userId'];
    phoneNumber = json['phoneNumber'];
    profession = json['profession'];
    address = json['address'];
    subscribeToEmail = json['subscribeToEmail'];
    externalReferalCode = json['externalReferalCode'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    emailOrPhone = json['emailOrPhone'];
    subscribeToPush = json['subscribeToPush'];
    minimalNotificationDistance = json['minimalNotificationDistance'];
    nomLieu = json['nomLieu'];
    subscribeToSMS = json['subscribeToSMS'];
    language = json['language'];
    defaultTravelOption = json['defaultTravelOption'];
    defaultRequestRadius = json['defaultRequestRadius'];
    city = json['city'];
    driverOperatingCountryCode = json['driverOperatingCountryCode'];
    driverOperatingCityCode = json['driverOperatingCityCode'];
    codeTripOption = json['codeTripOption'];
    gender = json['gender'];
    referalCode = json['referalCode'];
    referalCodeUsed = json['referalCodeUsed'];
    status = json['status'];
    verificationCode = json['verificationCode'];
    contryCode = json['contryCode'];
    newBalance = json['newBalance'];
    isRefreshActive = json['isRefreshActive'];
    defaultPaymentmode = json['defaultPaymentmode'];
    refreshActive = json['refreshActive'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['country']= this.country;
    data['refreshActive']=this.refreshActive;
    data['defaultPaymentmode'] = this.defaultPaymentmode;
    data['isRefreshActive'] = this.isRefreshActive;
    data['newBalance'] = this.newBalance;
    data['contryCode'] = this.contryCode;
    data['verificationCode'] = this.verificationCode;
    data['status'] = this.status;
    data['referalCodeUsed'] = this.referalCodeUsed;
    data['referalCode'] = this.referalCode;
    data['gender'] = this.gender;
    data['birthDate'] = this.birthDate;
    data['userId'] = this.userId;
    data['subscribeToSMS'] = this.subscribeToSMS;
    data['minimalNotificationDistance'] = this.minimalNotificationDistance;
    data['codeTripOption'] = this.codeTripOption;
    data['externalReferalCode'] = this.externalReferalCode;
    data['driverOperatingCountryCode']= this.driverOperatingCountryCode;
    data['driverOperatingCityCode'] = this.driverOperatingCityCode;
    data['gender'] = this.gender;
    data['codeTripOption'] = this.codeTripOption;
    data['createUser'] = this.createUser;
    data['defaultRequestRadius'] = this.defaultRequestRadius;
    data['lastName'] = this.lastName;
    data['active'] = this.active;
    data['subscribeToPush'] = this.subscribeToPush;
    data['updateUser'] = this.updateUser;
    data['id'] = this.id;
    data['email'] = this.email;
    data['firstName'] = this.firstName;
    data['name'] = this.name;
    data['imageUrl']=this.imageUrl;
    data['subscribeToEmail'] = this.subscribeToEmail;
    data['provider']=this.provider;
    data['phoneNumber']=this.phoneNumber;
    data['defaultTravelOption'] = this.defaultTravelOption;
    data['profession'] = this.profession;
    data['address'] = this.address;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['emailOrPhone'] = this.emailOrPhone;
    data['nomLieu'] = this.nomLieu;
    data['language'] = this.language;
    data['city'] = this.city;

    return data;

  }





}