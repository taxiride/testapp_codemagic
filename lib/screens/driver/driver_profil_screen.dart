import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DriverProfilScren extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    final MediaQueryData _mq = MediaQuery.of(context);

    return MaterialApp(
      home: Scaffold(
        body: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
                minHeight: _mq.size.height - _mq.padding.top - 73),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 2),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          child: Image.asset('images/ellipse_auth.png',
                            height: MediaQuery.of(context).size.height*0.5,
                            width: MediaQuery.of(context).size.width,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),

                      Align(
                        alignment: Alignment.topLeft,
                        child: GestureDetector(
                            child:Container(
                              margin: const EdgeInsets.only(top: 32, left: 16),
                              child: Icon(
                                Icons.close,
                                color: Colors.white,
                              ),
                            ),
                            onTap: (){
                              Navigator.pop(context);
                            }
                        ),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child:  Container(
                          margin: const EdgeInsets.only(top: 32),
                          child: Text('DRIVER PROFILE', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, fontFamily: 'fonts/Poppins-Bold.ttf'),

                          ),
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}