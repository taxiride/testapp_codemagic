import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/ride.dart';

class PostRidesRequest extends AbstractAuthenticatedRequest{
  final Ride ride;
  final String token;
  PostRidesRequest(this.ride,this.token) : super(token);

  @override
  String getUrl() {
    return "/api/rides/postRide";
  }

  @override
  String getVerb() {
    return "POST";
  }

  @override
  getBody() {

    return {
      "arrivalPoint": ride.arrivalPoint,
      "availableCashAmount": ride.availableCashAmount,
      "codeOption": ride.codeOption,
      "codePays": ride.codePays,
      "codeVille": ride.codeVille,
      "departurePoint": ride.departurePoint,
      "endPointLatitude": ride.endPointLatitude,
      "endPointLongitude": ride.endPointLongitude,
      "estimatedDuration": ride.estimatedDuration,
      "estimatedTripDistance": ride.estimatedTripDistance,
      "estimatedTripLength": ride.estimatedTripLength,
      "id": ride.id,
      "language": ride.language,
      "phoneNumber": ride.phoneNumber,
      "postDate": ride.postDate,
      "rayon": ride.rayon,
      "riderLattitude": ride.riderLattitude,
      "riderLongitude": ride.riderLongitude,
      "seatNumber": ride.seatNumber,
      "startPointLatitude": ride.startPointLatitude,
      "startPointLongitude": ride.startPointLongitude,
      "tripCost": ride.tripCost,
      "tripTimeOut": ride.tripTimeOut
    };

  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print("----------- save ride succefful ------------");
      print(data);
    }else{
      print("----------- error error error ------------");

    }
  }

}