import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/emergency_contact.dart';

class SaveProfilSafetyNumberRequest extends AbstractAuthenticatedRequest{
  final String token;
  final EmergencyContact emergencyContact;

  SaveProfilSafetyNumberRequest(this.token, this.emergencyContact):super(token);

  @override
  String getUrl() {
    return "/api/profile/saveProfileSafetyNumber";
  }

  @override
  String getVerb() {
   return "POST";
  }

  @override
  getBody() {

    return {
      {
        "contactType": emergencyContact.contactType.toString(),
        "language": emergencyContact.language,
        "name": emergencyContact.name,
        "phone": emergencyContact.phone,
        "phoneNumber": emergencyContact.phoneNumber
      }
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
   dynamic data = response.httpResponse.body;
   if(response.httpResponse.statusCode == 200){
     print("----------save contact emergency succeffull ----------");
     EmergencyContact contact = EmergencyContact.fromJson(jsonDecode(data));
    // print(data);
   }else{
     print("---------- error error error ----------");
    // print(data);
   }
  }

}