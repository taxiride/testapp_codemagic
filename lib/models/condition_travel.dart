class ConditionTravel{
  String travelConditionValue;
  String travelConditionDescription;
  bool checked;

  ConditionTravel({this.travelConditionValue,this.travelConditionDescription,this.checked=false});

  ConditionTravel.fromJson(Map<String, dynamic> json){
    travelConditionValue = json['travelConditionValue'];
    travelConditionDescription = json['travelConditionDescription'];
    checked = ((json['travelConditionValue']=="Pas de chien")||(json['travelConditionValue']=="Pas de bavardage"))?true:false;
  }

  static List<ConditionTravel> listFromJson(List<dynamic> json) {
    return List<ConditionTravel>.from(json.map((v) => ConditionTravel.fromJson(v)));
  }
}