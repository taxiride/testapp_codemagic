import 'package:taxiride_ios/api/error/ApiErrorType.dart';

import '../abstract/AbstractApiError.dart';

class ParseError extends AbstractApiError{
  String message = "Unable to parse the body of the request";
  ParseError({this.message}) : super(0,message, ApiErrorType.parseError);

}