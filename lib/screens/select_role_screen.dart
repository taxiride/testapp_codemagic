import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/update_role_request.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/providers/rides_provider.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/ridescreens/navigation_driver.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class SelectRoleScreen extends StatefulWidget{


  @override
  _SelectRoleScreenState createState() => _SelectRoleScreenState();

}

class _SelectRoleScreenState extends State<SelectRoleScreen>{
  bool btn_rider = false;
  bool btn_driver = false;


  @override
  Widget build(BuildContext context) {
    double hauteur = MediaQuery.of(context).size.height;
    double largeur = MediaQuery.of(context).size.width;
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    RidesProvider ridesProvider = Provider.of<RidesProvider>(context);

    final ProgressDialog pr = ProgressDialog(context);
    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              Image.asset('images/ellipse_auth.png',
                fit: BoxFit.cover,
              ),
              Positioned(
                  top: 20,
                  child: Row(
                    children: [
                      IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: null),
                      Container(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.23),
                        child: Text("GET STARTED",style: TextStyle(),),
                      )

              ],)),

              Positioned(
                  top: 150,
                  left: 20,
                  child: Column(
                children: [
                  Container(

                    child: Text("How would you",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 37),),),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text("like to continue ?",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 37),),),
                ],
              ))

            ],
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(28),
                child: InkWell(
                  onTap: (){
                    setState(() {
                      btn_rider=true;
                      btn_driver = false;
                    });
                  },
                  child: Container(
                    child: Column(
                      children: [
                        (btn_rider)?Image.asset('images/rider.png',color: Colors.yellow,width: largeur*0.25,height: hauteur*0.25,):Image.asset('images/rider.png',width: largeur*0.25,height: hauteur*0.25,color: Colors.blueGrey,),
                        Container(child: Text("Rider",style: TextStyle(fontWeight: FontWeight.bold),)),
                      ],
                    ),
                   // child: (btn_rider)?Image.asset('images/rider.png',color: Colors.yellow,width: largeur*0.25,height: hauteur*0.25,):Image.asset('images/rider.png',width: largeur*0.25,height: hauteur*0.25,color: Colors.blueGrey,),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(28.0),
                child: InkWell(
                  onTap: (){
                    setState(() {
                      btn_driver = true;
                      btn_rider = false;


                    });

                  },
                  child: Container(
                    child: Column(
                      children: [
                        (btn_driver)?Image.asset('images/driver.png',color: Colors.yellow,):Image.asset('images/driver.png',color: Colors.blueGrey,),
                        Container(child: Text("Driver",style: TextStyle(fontWeight: FontWeight.bold),)),
                      ],
                    ),
                   // child: (btn_driver)?Image.asset('images/driver.png',color: Colors.yellow,):Image.asset('images/driver.png',color: Colors.blueGrey,),
                  ),
                ),
              )
            ],
          ),


          (btn_rider || btn_driver)?Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: largeur*0.8,
              child: PrimaryButton(
                text: "Continue",
                onPressed: () async {
                  pr.show();
                  await ridesProvider.findIdTrip();
                  print(ridesProvider.tripId);
                  if(ridesProvider.tripId > 0){
                    await ridesProvider.findTripInfosById(authProvider.token,ridesProvider.tripId);
                    await ridesProvider.checkCurrentRide(authProvider);
                  }

                  final prefs = await SharedPreferences.getInstance();
                  String token = prefs.getString("access_token");
                  String username = prefs.getString("username");
                  print("----- username -------");
                  print(authProvider.phone);
                  authProvider.findProfilByPhoneNumber(username, token);
                  if(btn_driver){
                    // role selectionner est driver_role
                    setRoleUser(1);
                    authProvider.setRole(1);
                    Toast.show("the modification worked [Driver]", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                    UpdateRoleRequest updateRole = UpdateRoleRequest(token,"ROLE_DRIVER",username);
                    updateRole.parseResult(await ApiClient.execOrFail(updateRole));
                    pr.hide();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => NavigationDriverPage(isRideAccepted:false )));
                  }else if(btn_rider){
                    // role selectionner est rider_role
                    setRoleUser(0);
                    authProvider.setRole(0);
                    Toast.show("the modification worked [RIDER]", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                    UpdateRoleRequest updateRole = UpdateRoleRequest(token,"ROLE_RIDER",username);
                    updateRole.parseResult(await ApiClient.execOrFail(updateRole));
                    pr.hide();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => NavigationRide('rider', 'login' )));
                  }
                  //await ridesProvider.findIdTrip();
                  //print(ridesProvider.tripId);
                  //if(ridesProvider.tripId > 0){
                    //await ridesProvider.findTripInfosById(authProvider.token,ridesProvider.tripId);
                    //await ridesProvider.checkCurrentRide(authProvider);
                  //}
                  pr.hide();
                //  Navigator.push(context, MaterialPageRoute(builder: (context) => NavigationRide('rider', 'login',)));

                  }
              ),
            ),
          ):SizedBox(height: 2,),
        ],
      ),
    );
  }

  void setRoleUser(int value) async {
    final prefs = await SharedPreferences.getInstance();
   await prefs.setInt('role',value);
    await prefs.setBool('is_connected',true);
  }

}