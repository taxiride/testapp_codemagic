import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/models/apiresponse/profileresp.dart';
import 'package:http/http.dart' as http;


class UserProfile extends StatefulWidget{
    UserProfile(String role, String phoneNumber);

    @override
    _UserProfileState createState() => _UserProfileState();
}

//contruit chaque ligne de la page de profil, on y attache également les controlleurs pour gérer l'affichage dynamique

Column _buildProfileColumn(String title, String defaultInfo, String image, String checkIcon)  {

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
            Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                    Image.asset(image, ),
                    Column(
                        children: <Widget>[
                            Row(
                                children: <Widget>[
                                    Text(title, style: TextStyle(color: Colors.grey, fontSize: 12,fontFamily: 'fonts/Poppins-Regular.ttf'),),
                                    if(checkIcon.isNotEmpty)   Image.asset(checkIcon),
                                ],
                            ),

                            Text(defaultInfo, style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold, fontFamily: 'fonts/Poppins-Bold.ttf'),),
                        ],
                    ),
                    Align(
                        alignment: Alignment.centerRight,
                        child: GestureDetector(
                             child: Image.asset('images/icon_edit.png'),
                        ),
                    ),

                ],
            )
        ],
    );
}


Widget _buildProfileColumn2(String title, String defaultInfo, String image, String checkIcon)  {

    return Container(
        padding: const EdgeInsets.all(32),
       child: Row(
           children: <Widget>[
               Image.asset(image, ),
               Expanded(
                   child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                           Row(
                               children: <Widget>[
                                   Text(title, style: TextStyle(color: Colors.grey, fontSize: 12,fontFamily: 'fonts/Poppins-Regular.ttf'),),
                                   if(checkIcon.isNotEmpty)  Container(
                                       margin: const EdgeInsets.only(left: 16),
                                       child:  Image.asset(checkIcon),
                                   ),
                               ],
                           ),

                           Text(defaultInfo, style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold, fontFamily: 'fonts/Poppins-Bold.ttf'),),
                       ],
                   ),
               ),

               Align(
                   alignment: Alignment.centerRight,
                   child: GestureDetector(
                       child: Image.asset('images/icon_edit.png'),
                   ),
               ),

           ],
       ),
    );
}


Future<Profile> findByPhoneNumber() async {

    final prefs =await SharedPreferences.getInstance();

    var uri =Uri.https('app.taxiride.biz:81', '/api/profile/findbyphonenumber/'+prefs.get('username')+'/fr');
    final http.Response response = await http.get(
        uri,
        headers: <String, String>{
            HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
        },

    );

    log('code'+response.statusCode.toString());

    if(response.statusCode == 200){
        Profile user = Profile.fromJson(json.decode(response.body));

        return user;
    }
    
}
Profile currentUserProfile;

List<ProfileEntry> data = <ProfileEntry>[
    ProfileEntry(
        'Contact Information',
        <Widget>[
            Container(
                margin: const EdgeInsets.only(bottom: 8, left: 8, top: 8),
                child: _buildProfileColumn2('Phone number', '', 'images/icon_phone.png', 'images/icon_success.png') ,
            ),
            Container(
                margin: const EdgeInsets.only(bottom: 8, left: 8),
                child:  _buildProfileColumn2('Email', '', 'images/icon_mail.png', 'images/icon_failed_close.png'),
            ),
            Container(
                margin: const EdgeInsets.only(bottom: 8, left: 8),
                child:  _buildProfileColumn2('Home address', 'Mvog Mbi', 'images/icon_home.png', 'images/icon_success.png'),
            ),

        ]
    ),
    ProfileEntry(
        'Preferences',
        <Widget>[

            Container(
                margin: const EdgeInsets.only(bottom: 16, left: 16, top: 8),
                child:   _buildProfileColumn('Language', '', 'images/icon_language.png', ''),
            ),
            Container(
                margin: const EdgeInsets.only(bottom: 8, left: 16),
                child:  _buildProfileColumn('Gender', '', 'images/icon_gender.png', ''),
            ),
            Container(
                margin: const EdgeInsets.only(bottom: 8, left: 16),
                child: _buildProfileColumn('Profession', '', 'images/icon_user.png', ''),
            ),
            Container(
                margin: const EdgeInsets.only(bottom: 8, left: 16),
                child: _buildProfileColumn('Default travel comfort', '', 'images/icon_volant.png', ''),
            ),
            Container(
                margin: const EdgeInsets.only(bottom: 8, left: 16),
                child:  _buildProfileColumn('Default request proximity', '', 'images/icon_location.png', ''),
            ),

        ]
    ),
    ProfileEntry(
        'Other Information'
    ),
];


class _UserProfileState extends State<UserProfile>{

    Future<Profile> userProfile;
    @override
    void initState(){
        super.initState();
        userProfile = findByPhoneNumber();

    }

    @override
    Widget build(BuildContext context) {

        final ProgressDialog pr = ProgressDialog(context);

        pr.style(
            message: 'Please wait...',
            backgroundColor: Colors.white,
            progressWidget: CircularProgressIndicator(),
            elevation: 10.0,
        );
        void _goToProfile(){


        }

        return MaterialApp(
            home: Scaffold(
                body: Column(
                    children: <Widget>[
                        Stack(
                            children: <Widget>[
                                Image.asset('images/ellipse_auth.png',
                                    fit: BoxFit.cover,
                                ),
                                Align(
                                    alignment: Alignment.topLeft,
                                    child: GestureDetector(
                                        child:Container(
                                            margin: const EdgeInsets.only(top: 32, left: 16),
                                            child: Icon(
                                                Icons.chevron_left,
                                                color: Colors.black,
                                            ),
                                        ),
                                        onTap: (){
                                            Navigator.pop(context);
                                        }
                                    ),
                                ),
                                Align(
                                    alignment: Alignment.topCenter,
                                    child:  Container(
                                        margin: const EdgeInsets.only(top: 32),
                                        child: Text('MY PROFILE', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, fontFamily: 'fonts/Poppins-Bold.ttf'),

                                        ),
                                    ),
                                ),
                                Align(
                                    alignment: Alignment.topCenter,
                                    child: Container(
                                        margin: const EdgeInsets.only(top: 128),
                                        child: Image.asset('images/no_profile_image.png'),
                                    ),
                                ),

                            ],
                        ),
                        FutureBuilder<Profile>(
                          future: userProfile,
                          builder: (context, snapshot){
                              if(snapshot.hasData){
                           // currentUserProfile = snapshot.data;
                            data = _buildProfilEntry(snapshot.data);
                                  return Column(
                                    children: <Widget>[
                                        Center(
                                            child: Container(
                                                margin: const EdgeInsets.only(top: 16, bottom: 16),
                                                child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: <Widget>[

                                                        Text(snapshot.data.lastName + ' '+snapshot.data.firstName, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, fontFamily: 'fonts/Poppins-Regular.ttf')),

                                                    ],
                                                ),
                                            ),
                                        ),

                                        ListView.builder(
                                            scrollDirection:Axis.vertical,
                                            shrinkWrap: true,
                                            itemBuilder: (BuildContext context, int index) =>
                                                EntryItem(data[index]),
                                            itemCount: data.length,
                                        ),
                                    ],
                                  );
                              }

                              return CircularProgressIndicator();
                          },
                        ),


                    ],
                ),
            ),
        );
    }
}


class Entry{
    Entry(this.title, [this.children = const <Entry>[]]);

    final String title;
    final List<Entry> children;
}

//classe qui gère les différents élément de la liste
//un titre et un ensemble de widget
class ProfileEntry{

    ProfileEntry(this.title, [this.children = const <Widget>[]]);

    final String title;
    final List<Widget> children;
}

//les éléments du profil notamment la liste déroulable

List<ProfileEntry> _buildProfilEntry(Profile currentUserProfile){
    return <ProfileEntry>[
        ProfileEntry(
            'Contact Information',
            <Widget>[
                Container(
                    margin: const EdgeInsets.only(bottom: 8, left: 16, top: 8),
                    child: _buildProfileColumn('Phone number', currentUserProfile.phoneNumber, 'images/icon_phone.png', 'images/icon_success.png') ,
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 8, left: 16),
                    child:  _buildProfileColumn('Email', currentUserProfile.email, 'images/icon_mail.png', 'images/icon_failed_close.png'),
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 8, left: 16),
                    child:  _buildProfileColumn('Home address', 'Mvog Mbi', 'images/icon_home.png', 'images/icon_success.png'),
                ),

            ]
        ),
        ProfileEntry(
            'Preferences',
            <Widget>[

                Container(
                    margin: const EdgeInsets.only(bottom: 16, left: 16, top: 8),
                    child:   _buildProfileColumn('Language', currentUserProfile.language, 'images/icon_language.png', ''),
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 8, left: 16),
                    child:  _buildProfileColumn('Gender', currentUserProfile.gender, 'images/icon_gender.png', ''),
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 8, left: 16),
                    child: _buildProfileColumn('Profession', currentUserProfile.profession, 'images/icon_user.png', ''),
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 8, left: 16),
                    child: _buildProfileColumn('Default travel comfort', 'defaultTravelOption', 'images/icon_volant.png', ''),
                ),
                Container(
                    margin: const EdgeInsets.only(bottom: 8, left: 16),
                    child:  _buildProfileColumn('Default request proximity', 'defaultRequestRadius', 'images/icon_location.png', ''),
                ),

            ]
        ),
        ProfileEntry(
            'Other Information'
        ),
    ];
}

class EntryItem extends StatelessWidget{

    const EntryItem(this.entry);

    final ProfileEntry entry;

    Widget _buildTiles(ProfileEntry root){
        if(root.children.isEmpty) return ListTile(title: Text(root.title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),);
        return ExpansionTile(
            key: PageStorageKey<ProfileEntry>(root),
            title: Text(root.title, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            children: root.children,
        );
    }

    @override
    Widget build(BuildContext context) {
        return _buildTiles(entry);
    }

}