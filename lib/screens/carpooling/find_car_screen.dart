import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/models/taximen_car.dart';
import 'package:taxiride_ios/screens/carpooling/card_car_taximen.dart';
import 'package:taxiride_ios/screens/carpooling/card_trip.dart';
import 'package:taxiride_ios/screens/carpooling/filter_screen.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:taxiride_ios/widget/layout/filter_box.dart';

class FindCarScreen extends StatefulWidget{
  final List<TaximenCar> carDispos;

  const FindCarScreen({Key key, this.carDispos}) : super(key: key);

  @override
  _FindCarScreenState createState() => _FindCarScreenState();

}

class _FindCarScreenState extends State<FindCarScreen>{
  @override
  Widget build(BuildContext context) {
   return Container(
     child: Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: (){
          Navigator.pop(context);
        }),
        title: Text("FIND A CAR",style: TextStyle(color: Colors.black),),
        backgroundColor: Colors.white,
      ),
       body: Column(
         children: [
           Padding(
             padding: const EdgeInsets.all(8.0),
             child: FilterBox(
                 label: "FILTER STATUS",
                 onTap: () {
                   Navigator.push(context, MaterialPageRoute( builder: (_) => FilterScreen(titre: "FIND A CAR",)));
                 }),
           ),
           Container(
             height: MediaQuery.of(context).size.height*0.64,
             width: MediaQuery.of(context).size.width,
             child: (widget.carDispos.length==0)?Center(
               child: Text("No Cars Found",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold,fontSize: 16),),
             ):ListView.builder(
                 itemCount: (widget.carDispos==null)?0:widget.carDispos.length,
                 itemBuilder: (context,index){
                   TaximenCar tamenCar = widget.carDispos[index];
                   return CardCarTaximen(args: tamenCar,);
                 })
           ),
           Container(
             width: MediaQuery.of(context).size.width*0.8,
             padding: EdgeInsets.all(10),
             child: BlackButton(
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                 children: [
                   Icon(Icons.refresh,color: Colors.white,),
                   Text("REFRESH FEED",style: TextStyle(color: Colors.white,),)
                 ],
               ),
               onPressed: (){},
             ),
           )
         ],
       ),
     ),
   );
  }

}