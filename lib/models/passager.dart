class Passager{
  int bookingId;
  int travelId;
  int riderId;
  int driverId;
  String bookingDate;
  String departureStartDate;
  String departureEndDate;
  String startCity;
  String departure;
  String endCity;
  String arrival;
  int bookedSeatCount;
  int riderBookingCount;
  String riderFirstName;
  int distance;
  double clientCost;

  Passager({
    this.bookingId,
    this.riderId,this.driverId,
    this.bookingDate,
    this.departureStartDate,
    this.travelId,
    this.departureEndDate,
    this.startCity,this.departure,this.endCity,this.arrival,this.bookedSeatCount,
    this.riderFirstName,this.distance,this.clientCost
});

  Passager.fromJson(Map<String,dynamic> json){
    bookingId = json['bookingId'];
    riderId = json['riderId'];
    driverId = json['driverId'];
    bookingDate = json['bookingDate'];
    departureStartDate = json['departureStartDate'];
    travelId = json['travelId'];
    departureEndDate = json['departureEndDate'];
    startCity = json['startCity'];
    departure = json['departure'];
    endCity = json['endCity'];
    arrival = json['arrival'];
    bookedSeatCount =json['bookedSeatCount'];
    riderFirstName = json['riderFirstName'];
    distance = json['distance'];
    clientCost = json['clientCost'];
  }

  static List<Passager> listFromJson(List<dynamic> json) {
    return List<Passager>.from(json.map((v) => Passager.fromJson(v)));
  }
}