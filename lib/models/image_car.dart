class ImageCar{
  int id;
  String pictureURL;
  int pictureNumber;

  ImageCar({this.id,this.pictureURL,this.pictureNumber});

  ImageCar.fromJson(Map<String, dynamic> json){
    id = json['id'];
    pictureURL = json['pictureURL'];
    pictureNumber = json['pictureNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['pictureNumber'] = this.pictureNumber;
    data['pictureURL'] = this.pictureURL;
    return data;
  }


  static List<ImageCar> listFromJson(List<dynamic> json) {
    return List<ImageCar>.from(json.map((v) => ImageCar.fromJson(v)));
  }
}