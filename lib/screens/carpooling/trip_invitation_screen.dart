import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:taxiride_ios/widget/layout/filter_box.dart';

import 'card_trip.dart';
import 'filter_screen.dart';

class TripInvitation extends StatefulWidget{
  final List<dynamic> tripsDispo;

  const TripInvitation({Key key, this.tripsDispo}) : super(key: key);

  _TripInvitationState createState() => _TripInvitationState();

}

class _TripInvitationState extends State<TripInvitation>{
  @override
  Widget build(BuildContext context) {

    return Container(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: (){
            Navigator.pop(context);
          }),
          title: Text("TRIP INVITATIONS",style: TextStyle(color: Colors.black),),
          backgroundColor: Colors.white,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FilterBox(
                  label: "FILTER STATUS",
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute( builder: (_) => FilterScreen(titre: "TRIP INVITATIONS",)));
                  }),
            ),
            Container(
              height: MediaQuery.of(context).size.height*0.64,
              width: MediaQuery.of(context).size.width,
              child: (widget.tripsDispo.length==0)?Center(
                child: Text("No Trip Found",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold,fontSize: 16),),
              ):ListView.builder(
                  itemCount: (widget.tripsDispo==null)?0:widget.tripsDispo.length,
                  itemBuilder: (context,index){
                    return CardTrip();
                  })
            ),
            Container(
              width: MediaQuery.of(context).size.width*0.8,
              padding: EdgeInsets.all(10),
              child: BlackButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(Icons.refresh,color: Colors.white,),
                    Text("REFRESH FEED",style: TextStyle(color: Colors.white,),)
                  ],
                ),
                onPressed: (){},
              ),
            )
          ],
        ),
      ),
    );

  }

}