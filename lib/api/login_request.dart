import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';

import 'abstract/AbstractApiRequest.dart';

class LoginRequest extends AbstractApiRequest{
  final String password;
  final String username;

  LoginRequest({this.password, this.username});

  @override
  String getUrl() {
    return "/api/auth/login";
  }

  @override
  String getVerb() {
    return "POST";
  }

  @override
  getBody() {
    return {
      "password":password,
      "username":username
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
    print("-------- debut -----------");
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print("tout c est bien passe lors de la connection");
      Map<String,dynamic> responce = jsonDecode(data);
      return response;
    }else{
      print("----- la connection a echouer mr ------");
    }
  }



}