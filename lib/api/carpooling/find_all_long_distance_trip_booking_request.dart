import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/passager.dart';

class FindAllLongDistanceTripBookingRequest extends AbstractAuthenticatedRequest{
  final String token;
  final int bookingStatus;
  final String endCity;
  final String language;
  final String startCity;
  final String startDate;

  FindAllLongDistanceTripBookingRequest(this.token, this.bookingStatus, this.endCity, this.language, this.startCity, this.startDate):super(token);


  @override
  String getUrl() {
    return "/api/carpooling/findAllLongDistanceTripBooking/$startCity/$endCity/$startDate/$bookingStatus/fr";
  }

  @override
  String getVerb() {
    return "GET";
  }


  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
     // print(jsonDecode(data)["object"]);
      List<Passager> passagers = Passager.listFromJson(jsonDecode(data)["object"]);
      print("---------- tout c 'est bien passe ---------");
      return passagers;
    }else{
      print("---------- requete echouer ---------");

    }
  }


}