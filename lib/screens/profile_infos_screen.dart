import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/find_confidenceor_alert_number_by_phone_request.dart';
import 'package:taxiride_ios/api/save_profile_personnal_information_request.dart';
import 'package:taxiride_ios/api/save_profile_safety_number_request.dart';
import 'package:taxiride_ios/models/emergency_contact.dart';
import 'package:taxiride_ios/models/profile.dart';
import 'package:taxiride_ios/models/validator.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/screens/driver/add_car_screen.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:http/http.dart' as http;
import 'package:taxiride_ios/widget/button/secondary_button.dart';
import 'package:toast/toast.dart';

class ProfileInfosScreen extends StatefulWidget{
  final Profil profile;

  const ProfileInfosScreen({Key key, this.profile}) : super(key: key);

  @override
  _ProfileInfosScreenState createState() => _ProfileInfosScreenState();

}

class _ProfileInfosScreenState extends State<ProfileInfosScreen>{
  bool active_contact = false;
  bool active_preference = false;
  bool active_orther = false;
  int role ;
  File image;
  final myController = TextEditingController();
  final nameController = TextEditingController();
  Profil _profil;
  String _token,_phone;
  bool langue = false;
  String comfort = "VIP";
  String sexe = "M";
  int type_upload =0 ;
  List<EmergencyContact> emergengy = [];
  List<EmergencyContact> trusted_contact = [];
  @override
  void initState() {
    // TODO: implement initState
    getRole();
    super.initState();
   // _profil = Profil();
    _profil = widget.profile;
    print(_profil.email);
    AuthProvider auth = Provider.of<AuthProvider>(context,listen: false);
   findEmergencyPhone(auth);
  }

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
   return MaterialApp(
     home: Scaffold(
       body: SingleChildScrollView(
         child: Column(
           children: [
             Padding(
               padding: const EdgeInsets.only(bottom: 0.8),
               child: Stack(
                 children: <Widget>[
                   Image.asset('images/ellipse_auth.png',
                     fit: BoxFit.cover,
                   ),
                   Align(
                     alignment: Alignment.topLeft,
                     child: GestureDetector(
                         child:Container(
                           margin: const EdgeInsets.only(top: 32, left: 16),
                           child: Icon(
                             Icons.chevron_left,
                             color: Colors.black,
                           ),
                         ),
                         onTap: (){
                           Navigator.pop(context);
                         }
                     ),
                   ),
                   Align(
                     alignment: Alignment.topCenter,
                     child:  Container(
                       margin: const EdgeInsets.only(top: 32),
                       child: Text('MY PROFILE', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, fontFamily: 'fonts/Poppins-Bold.ttf'),

                       ),
                     ),
                   ),
                   Align(
                     alignment: Alignment.topCenter,
                     child: Column(
                       children: [
                         (widget.profile.imageUrl == null)?InkWell(
                           onTap: (){
                             //getImage();
                             findImageDialog();
                           },
                           child:(image == null)?Container(
                             margin: const EdgeInsets.only(top:128),
                             child:Image.asset('images/no_profile_image.png'),
                           ):Container(
                             margin: const EdgeInsets.only(top:128),
                             width: 100,
                             height: 100,
                             decoration: BoxDecoration(
                               shape: BoxShape.circle,
                             ),
                             child: CircleAvatar(
                               backgroundImage: FileImage(image),
                             ),//Image.file(image,),

                           ),
                         ):InkWell(
                           onTap: (){
                            // getImage();
                             findImageDialog();
                           },
                          child:(image == null)? Container(
                            width: 100,
                            height: 100,
                            margin: const EdgeInsets.only(top:128),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: CircleAvatar(
                              radius: 100,
                              backgroundImage: NetworkImage('https://app.taxiride.biz:81/api/commons/download/'+widget.profile.imageUrl) ,
                            ),
                            ):Container(
                            margin: const EdgeInsets.only(top:128),
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child: CircleAvatar(
                              backgroundImage: FileImage(image),
                            ),//Image.file(image,),
                          )
                         ),

                         Container(
                           padding: EdgeInsets.only(top: 10),
                           child: Center(
                             child: Text("${widget.profile.firstName}",style: TextStyle(fontWeight: FontWeight.bold),),
                           ),
                         )
                       ],
                     ),
                   ),

                 ],
               ),
             ),
             Container(
               padding: EdgeInsets.all(5),
               height: MediaQuery.of(context).size.height*0.4,
               child: SingleChildScrollView(
                 
                 child: Column(
                   children: [
                     ExpansionPanelList(
                       expansionCallback: (index,expanded){
                         print("index : ${index}");
                         setState(() {
                           if(index == 0){
                             active_contact = !active_contact;
                             active_preference = false;
                             active_orther = false;
                           }else if(index == 1){
                             active_contact = false;
                             active_preference = ! active_preference;
                             active_orther = false;
                           }else{
                             active_contact = false;
                             active_preference = false;
                             active_orther = !active_orther;
                             findEmergencyPhone(authProvider);

                           }

                         });
                       },
                       children: [
                         ExpansionPanel(headerBuilder: (BuildContext context,bool isExpanded){
                           return ListTile(title: Text("Contact Informations",style: TextStyle(fontWeight: FontWeight.bold),),);
                         }, body: Column(
                           children: [
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("phone number",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: Text("${_profil.phoneNumber}"),
                               leading: Icon(Icons.phone,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){
                                // _showDialog("Change Phone Number", "phone number",1);
                               },),
                             ),
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("email",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: Text("${_profil.email}"),
                               leading: Icon(Icons.email,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){ _showDialog("Change Email", "email",2);},),
                             ),
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("home adress",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: (_profil.address==null)?Text("No adresse valid"):Text("${_profil.address}"),
                               leading: Icon(Icons.home,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){ _showDialog("Change Home Adresse", "adresse",3);},),
                             ),
                           ],
                         ),
                         isExpanded: active_contact),
                         ExpansionPanel(headerBuilder: (BuildContext context,bool isExpanded){
                           return ListTile(title: Text("Preferences",style: TextStyle(fontWeight: FontWeight.bold),),);
                         }, body: Column(
                           children: [
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("language",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: (widget.profile.language==null)?Text("No Language select"):Text("${widget.profile.language}"),
                               leading: Icon(Icons.language,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){
                                // _showDialog("Update Language", "langue", 4);
                               },),
                             ),
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("gender",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: Text(_profil.gender),
                               leading: Icon(Icons.people_outline,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){},),
                             ),
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("proffession",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: (widget.profile.profession==null)?Text("No Proffession valid"):Text("${widget.profile.profession}"),
                               leading: Icon(Icons.perm_contact_calendar,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){ _showDialog("Update proffession", "proffession", 6);

                               },),
                             ),
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("Default travel Comfort",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: (_profil.defaultTravelOption==null)?Text("no value"):Text("${_profil.defaultTravelOption}"),
                               leading: Icon(Icons.directions_car,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){
                                 openDialogConfort();
                               },),
                             ), Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("Default request proximity",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: (_profil.minimalNotificationDistance==null)?Text("0 km"):Text("${_profil.minimalNotificationDistance} km"),
                               leading: Icon(Icons.album,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){
                                 _showDialogPhone("Change Minimal distance", "distance", 8);
                               },),
                             ),
                           ],
                         ),
                             isExpanded: active_preference),

                         ExpansionPanel(headerBuilder: (BuildContext context,bool isExpanded){
                           return ListTile(title: Text("Other Information",style: TextStyle(fontWeight: FontWeight.bold),),);
                         }, body: Column(
                           children: [
                             Divider(height: 5,color: Colors.black,),
                             ListTile(
                               title: Text("Sponsorship Code",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: (_profil.referalCode ==null)?Text("No referalcode valid"):Text("${_profil.referalCode}"),
                               leading: Icon(Icons.email,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){
                               },),
                             ),
                             Divider(height: 5,color: Colors.black,),
                             (role == 0) ? ListTile(
                               title: Text("Emergency Phone Number",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: ( emergengy.length == 0)?Text("No Contact found "):
                               Column(
                                 children: emergengy.where((element) => true).map<Widget>((e){
                                   return ListTile(
                                     title: Text(e.phone),
                                     trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){
                                     //  _openDialogueUpdateContact(e);
                                     },),
                                   );
                                 }).toList(),
                               ),
                               leading: Icon(Icons.phone,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.add_circle,color:Colors.black),onPressed: (){
                                 //_showDialogPhone("Change Emergy Number", "emergy number", 10);
                                 if(emergengy.length<3){
                                   _openDialogueSaveContact(0);
                                 }else{
                                   Toast.show("Contact Complete", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                                 }

                               },),
                             ):SizedBox(height: 2,),

                             (role == 0)? Divider(height: 5,color: Colors.black,):SizedBox(height: 2,),
                             (role == 0)?ListTile(
                               title: Text("Trusted Phone Number",style: TextStyle(color: Colors.green,fontSize: 15),),
                               subtitle: (trusted_contact.length==0)?Text("No Contact found"):Column(
                                 children: trusted_contact.where((element) => true).map<Widget>((e){
                                   return ListTile(
                                     title: Text(e.phone),
                                     trailing: IconButton(icon: Icon(Icons.edit),onPressed: (){},),
                                   );
                                 }).toList(),
                               ),
                               leading: Icon(Icons.phone,color: Colors.green,),
                               trailing: IconButton(icon: Icon(Icons.add_circle,color:Colors.black),onPressed: (){
                                // _showDialogPhone("Trusted Phone Number", "trusted number", 11);
                                 if(trusted_contact.length<3){
                                   _openDialogueSaveContact(1);
                                 }else{
                                   Toast.show("Contact Complete", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                                 }

                               },),
                             ):SizedBox(height: 2,),
                           ],
                         ),
                             isExpanded: active_orther),

                       ],
                     ),
                   ],
                 ),
               ),

             ),

             Container(
               width: MediaQuery.of(context).size.width*0.8,
               padding: EdgeInsets.all(10),
               child: (role == 1)?PrimaryButton(
                 text: 'Add Your Car',
                 onPressed: (){
                   Navigator.push(
                       context,
                       MaterialPageRoute(builder: (context)=>AddCarScreen()
                       )
                   );
                 },
               ):SizedBox(),
             )
           ],
         ),
       ),
     ),
   );
  }

   getRole() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.get("access_token");
    String phone = prefs.getString('username');
    setState(() {
       role = prefs.getInt('role');
       _token = token;
       _phone = phone;
    });
  }

  Future<void> getImage() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.get("access_token");
    String phone = prefs.getString('username');
   // findImageDialog();
      var profil = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        image = profil;
        type_upload = 0;
        print(image);

      });

    uploadPhotoProfil(image, token, phone);
  }

  Future<void> getImageCamera() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.get("access_token");
    String phone = prefs.getString('username');
    // findImageDialog();
    var profil = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      image = profil;
      type_upload = 0;
      print(image);

    });

    uploadPhotoProfil(image, token, phone);
  }

  Future<Map<String, dynamic>> uploadPhotoProfil(File image,String token,String phoneNumber) async {
    String url = "https://app.taxiride.biz:81/api/profile/uploadProfilePhoto";

    final Uri uri = Uri.parse(url);
    final String prefix = "";
    Uri uri2 = Uri.https("app.taxiride.biz:81", prefix +"/api/profile/saveProfileCabImage");
    Map<String, String> headers = {
      'Authorization':'Bearer ' + token};
    print('Bearer ' + token);
    final http.MultipartRequest request = http.MultipartRequest("POST", uri);
    request.headers['Authorization'] ='Bearer ' + token ;
    final file = await http.MultipartFile.fromPath('file',image.path);
    request.files.add(file);
    request.fields['language'] = 'fr';
    request.fields['phoneNumber'] = phoneNumber;
    final http.StreamedResponse response = await request.send();
    final rep = await http.Response.fromStream(response);
    print('statusCode => ${response.statusCode}');
    print(rep.body);

    if(response.statusCode != 200 ){
    //  Toast.show("error Save Image", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
     print(" ------------ file not save  ----------------");
      return null;
    }else{
     // Toast.show("Save Succefull", context,duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
      print(" ------------ file save succefful ----------------");
    }
    final Map<String,dynamic> responseData = json.decode(rep.body);

    return responseData;

  }

  void _openDialogueSaveContact(int type){
    showDialog(context: context,builder: (BuildContext context){
      return Container(
        height: MediaQuery.of(context).size.height*0.5,
        child: AlertDialog(
          title: new Text("Save Urgent Phone"),
          actions: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child:TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Name...',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child:TextField(
                    controller: myController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Phone...',
                    ),
                  ),
                ),
                Divider(height: 8,color: Colors.black,),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      PrimaryButton(text: 'Save',onPressed: (){
                        EmergencyContact emergency_contact = EmergencyContact(contactType: type,language: "en",name: nameController.text.toString(),phoneNumber: _phone,phone: myController.text.toString());
                        if(Validator.isNotEmpty(myController.text) && Validator.isNotEmpty(nameController.text)){
                          saveContactEmergency(emergency_contact);
                          setState(() {
                            if(type == 0){emergengy.add(emergency_contact);}else{trusted_contact.add(emergency_contact);}
                          });
                        }else{
                          Toast.show("Champ invalid", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                        }

                       myController.text ="";
                       nameController.text ="";
                       // findEmergencyPhone();
                        Navigator.of(context).pop();
                      },),
                      SecondaryButton(text: 'Close',onPressed: (){
                        Navigator.of(context).pop();
                      },)
                    ],
                  ),

                )

              ],
            )
          ],

        ),
      );
    });
  }

  void _openDialogueUpdateContact(EmergencyContact contact){
    showDialog(context: context,builder: (BuildContext context){
      return Container(
        height: MediaQuery.of(context).size.height*0.5,
        child: AlertDialog(
          title: new Text("Update Urgent Phone"),
          actions: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child:TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      hintText: contact.name,
                      border: OutlineInputBorder()
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child:TextField(
                    controller: myController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      hintText: contact.phone,
                      border: OutlineInputBorder(),
                      labelText: 'Phone...',
                    ),
                  ),
                ),
                Divider(height: 8,color: Colors.black,),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      PrimaryButton(text: 'Save',onPressed: (){
                        String tmp = contact.phone;
                        int pos = 0;

                        if(Validator.isNotEmpty(myController.text) && Validator.isNotEmpty(nameController.text)){
                          contact.name = nameController.text.toString();
                          contact.phone = myController.text.toString();
                          saveContactEmergency(contact);
                          setState(() {
                            if(contact.contactType == 0){
                              emergengy.forEach((element) {
                                if(element.phone == tmp){emergengy.insert(pos,contact);}
                                pos++;
                              });
                            }else{
                              trusted_contact.forEach((element) {
                                if(element.phone == tmp){emergengy.insert(pos,contact);}
                                pos++;
                              });}
                          });
                        }else{
                          Toast.show("Champ invalid", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                        }

                        myController.text ="";
                        nameController.text ="";
                        // findEmergencyPhone();
                        Navigator.of(context).pop();
                      },),
                      SecondaryButton(text: 'Close',onPressed: (){
                        Navigator.of(context).pop();
                      },)
                    ],
                  ),

                )

              ],
            )
          ],

        ),
      );
    });

  }

  void _showDialogPhone(String title,String champ,int position){

    showDialog(context : context,builder: (BuildContext context){
      return AlertDialog(
        title: new Text("Update Information Profil"),
        content: new Text(title),
        actions: [
          Column(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width*0.9,
                child:TextField(
                  controller: myController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: champ,
                  ),
                ),
              ),

              Container(
                padding: EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    PrimaryButton(text: 'Save',onPressed: (){
                      if(Validator.isNotEmpty(myController.text)){
                        setState((){
                          if(position == 8){
                            _profil.minimalNotificationDistance = double.parse(myController.text.toString());
                          }
                          if(position == 9){_profil.phoneNumber=myController.text.toString();}
                          else if(position == 10){
                            if(Validator.isEmail(myController.text)){_profil.email = myController.text;}else{
                              Toast.show("Bad Email !!", context,duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                            }
                          }
                          else if(position == 11){_profil.address = myController.text;}

                          updateProfile(_profil);
                          Navigator.of(context).pop();
                          myController.text="";
                        });

                      }else{
                        Toast.show("Champ Invalid", context,duration: Toast.LENGTH_LONG,gravity: Toast.TOP);
                      }

                    },),
                    SecondaryButton(text: 'Close',onPressed: (){Navigator.of(context).pop();},)
                  ],
                ),
              )
            ],

          )

        ],

      );
    });

  }
  void findImageDialog(){
    showDialog(context: context,builder:(BuildContext context){
      return AlertDialog(
        title: new Text("Select Image With"),
        actions: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width*0.9,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: (){
                        setState(() {
                          getImageCamera();
                          Navigator.of(context).pop();
                        });
                      },
                      child: Container(
                          padding: EdgeInsets.all(20),
                          child: Text("CAMERA",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),)),
                    ),
                    InkWell(
                      onTap: (){
                        setState(() {
                          //type_upload = 1;
                          getImage();
                          Navigator.of(context).pop();
                        });
                      },
                      child: Container(
                          padding: EdgeInsets.all(20),
                          child: Text("GALERY",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),)),
                    )
                  ],
                ),
              ),
              Divider(height: 8,color: Colors.black,),
              Container(
                padding: EdgeInsets.all(15),
                child: SecondaryButton(text: 'Cancel',onPressed: (){Navigator.of(context).pop();},),

              )

            ],
          )
        ],
      );
    });
  }

  void openDialogConfort(){
    showDialog(context: context,builder: (BuildContext context){
      return AlertDialog(
        title: new Text("Update Information Profil"),
        content:new Text("select travel confort"),
        actions: [
          Container(
            padding: EdgeInsets.all(10),
            width: MediaQuery.of(context).size.width*0.9,
            child:  Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap: (){
                          setState(() {
                            _profil.defaultTravelOption = "CLASSIQUE";
                            updateProfile(_profil);
                            Navigator.of(context).pop();
                          });
                        },
                        child: Container(
                            padding: EdgeInsets.all(20),
                            child: Text("CLASSIQUE",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),)),
                      ),
                      InkWell(
                        onTap: (){
                          setState(() {
                            _profil.defaultTravelOption = "VIP";
                            updateProfile(_profil);
                            Navigator.of(context).pop();
                          });
                        },
                        child: Container(
                            padding: EdgeInsets.all(20),
                            child: Text("VIP",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),)),
                      )
                    ],
                  ),
                ),
                Divider(height: 8,color: Colors.black,),
                Container(
                  padding: EdgeInsets.all(15),
                  child: SecondaryButton(text: 'Cancel',onPressed: (){Navigator.of(context).pop();},),

                )

              ],
            )
          ),

        ],
      );
    });
  }

  void _showDialog(String title,String champ,int position) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Update Information Profil"),
          content: new Text(title),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child:(position == 1) ?TextField(
                    controller: myController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: champ,
                    ),
                  ):TextField(
                    controller: myController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: champ,
                    ),
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      PrimaryButton(text: 'Save',onPressed: (){
                        if(Validator.isNotEmpty(myController.text)){
                          setState((){
                            if(position == 1){_profil.phoneNumber=myController.text.toString();}
                            else if(position == 2){
                              if(Validator.isEmail(myController.text)){_profil.email = myController.text;}else{
                                Toast.show("Bad Email !!", context,duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                              }
                            }
                            else if(position == 3){_profil.address = myController.text;}
                            else if(position == 6){ _profil.profession = myController.text;}
                            updateProfile(_profil);
                            Navigator.of(context).pop();
                            myController.text="";
                          });
                        }else{
                          Toast.show("Champ Invalid !!", context,duration: Toast.LENGTH_LONG,gravity: Toast.TOP);
                        }

                      },),
                      SecondaryButton(text: 'Close',onPressed: (){Navigator.of(context).pop();},)
                    ],
                  ),
                )
              ],
            ),

          ],
        );
      },
    );
  }

  updateProfile(Profil profil) async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    SaveProfilPersonnalInformation saveProfilPersonnalInformation = SaveProfilPersonnalInformation(profil, token);
    saveProfilPersonnalInformation.parseResult( await ApiClient.execOrFail(saveProfilPersonnalInformation));
  }
  saveContact(EmergencyContact contact) async {
    SaveProfilSafetyNumberRequest saveNumber = SaveProfilSafetyNumberRequest(_token,contact);
   EmergencyContact emergencyContact = saveNumber.parseResult(await ApiClient.execOrFail(saveNumber));

  }

  void saveContactEmergency(EmergencyContact contact) async {
   //await pr.show();

    final http.Response response = await http.post(
      'https://app.taxiride.biz:81/api/profile/saveProfileSafetyNumber',
      headers: <String, String>{
        'Authorization': 'Bearer '+_token,
        'Content-Type':'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'contactType': contact.contactType.toString(),
        'language': contact.language,
        'name': contact.name,
        'phone': contact.phone,
        'phoneNumber': contact.phoneNumber

      }),
    );

    print('code---------- '+ response.statusCode.toString());

   // await pr.hide();

    final prefs = await SharedPreferences.getInstance();

    if(response.statusCode == 200){
      print("----------- le body ---------"+response.body);

      Toast.show("save Succefull", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
     // Navigator.push(context, MaterialPageRoute(builder: (context) => SelectRoleScreen()));


      //return resp;
    }else{
      Toast.show("SAVE Lost", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
      throw Exception('Error');
    }

  }

  findEmergencyPhone(AuthProvider authProvider) async {
   // final prefs = await SharedPreferences.getInstance();
    //String token = prefs.getString('access_token');
    //String phone = prefs.getString('username');

    print(authProvider.role);
    if(authProvider.role == 0){
      FindConfidenceOrAlertNumberRequest findConfidenceOrAlertNumberRequest = FindConfidenceOrAlertNumberRequest(authProvider.token,authProvider.profil.language,authProvider.phone,"0");
      emergengy = findConfidenceOrAlertNumberRequest.parseResult(await ApiClient.execOrFail(findConfidenceOrAlertNumberRequest));
      FindConfidenceOrAlertNumberRequest findTrustedOrAlertNumberRequest = FindConfidenceOrAlertNumberRequest(authProvider.token,authProvider.profil.language,authProvider.phone,"1");
      trusted_contact = findConfidenceOrAlertNumberRequest.parseResult(await ApiClient.execOrFail(findTrustedOrAlertNumberRequest));

    }

  }


}