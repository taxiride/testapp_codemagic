import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/models/trip_args.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';

class CardTrip extends StatefulWidget{
  final TripArgs args;

  const CardTrip({Key key, this.args}) : super(key: key);
  
  _CardTripState createState() => _CardTripState();
  
}

class _CardTripState extends State<CardTrip>{
  @override
  Widget build(BuildContext context) {
     return Container(

       child: Card(
         shadowColor: Colors.grey,
         shape: cardShape(),
         child: Column(
           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
           children: [
             Container(
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                 children: [
                   Card(
                     shadowColor: Colors.grey,
                     shape: cardShape(),
                     child: Column(
                       children: [
                         Padding(
                           padding: const EdgeInsets.all(25.0),
                           child: Container(
                             width: 75,
                             height: 75,
                             margin: EdgeInsets.all(1),
                             padding: EdgeInsets.all(2),
                             decoration: BoxDecoration(
                                 border: Border.all(width: 1),
                                 borderRadius: BorderRadius.circular(25),
                                 color: Colors.white70
                             ),
                             child: Icon(Icons.account_circle),
                           ),
                         ),
                         Text("Mark Obiyolo",style: TextStyle(fontWeight: FontWeight.bold),),
                         Container(
                           padding: EdgeInsets.all(20),
                           child: Row(
                             children: [
                               Icon(Icons.star,color: Colors.orange,),
                               Icon(Icons.star,color: Colors.orange,),
                               Icon(Icons.star,color: Colors.orange,),
                               Icon(Icons.star,color: Colors.orange,),
                               Icon(Icons.star,color: Colors.orange,)
                             ],
                           ),
                         )

                       ],
                     ),




                   ),
                   Container(
                     width: MediaQuery.of(context).size.width*0.5,
                     height: MediaQuery.of(context).size.height*0.5,
                     child: Column(
                       children: [
                         ListTile(
                           title: Text("Pick Up Point",style: TextStyle(fontSize: 10),),
                           subtitle: Text("Zenitn Bank-Abuja",style: TextStyle(fontWeight: FontWeight.bold),),
                         ),
                         //Divider(height: 8,color: Colors.black,),
                         ListTile(
                           title: Text("Drop Off",style: TextStyle(fontSize: 10)),
                           subtitle: Text("Minna,Niger State",style: TextStyle(fontWeight: FontWeight.bold)),
                         ),
                         //Divider(height: 8,color: Colors.black,),
                         ListTile(
                           title: Text("Date/Time",style: TextStyle(fontSize: 10)),
                           subtitle: Text("21 Feb 2020: 9:30am",style: TextStyle(fontWeight: FontWeight.bold)),
                         ),
                         //Divider(height: 8,color: Colors.black,),
                         ListTile(
                           title: Text("Seats Left",style: TextStyle(fontSize: 10)),
                           subtitle: Text("2 on 4",style: TextStyle(fontWeight: FontWeight.bold)),
                         ),
                       ],
                     ),
                   )
                 ],
               ),
             ),

             Container(
               padding: EdgeInsets.all(20),

               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: [
                   Container(
                     child: Card(
                       shadowColor: Colors.grey,
                       shape: cardShape(),
                       child: Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: Column(
                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                           children: [
                             Text("Trip Price"),
                             Text("N 200",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                             Text("CAN BID PRICE")
                           ],
                         ),
                       ),
                     ),
                   ),
                   Container(
                     child: PrimaryButton(
                       text: 'REQUEST TO JOIN',
                       onPressed: (){},
                     ),
                   )
                 ],
               ),
             )
           ],
         ),
       ),
     );
  }
  
}