
import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';

class UpdateRoleRequest extends AbstractAuthenticatedRequest{
  final String role;
  final String username;
  final String token;

  UpdateRoleRequest(this.token,this.role, this.username):super(token);

  @override
  String getUrl() {
   return "/api/auth/updateRoles";

  }

  @override
  String getVerb() {
   return "POST";
  }

  @override
  getBody() {
    return
        {
          "role":role,
          "username":username
        };

  }

  @override
  parseResult(AbstractApiResponse response) {
    print("------- resultat de la mise a jour du role ----------");
    print(response.httpResponse.statusCode);
    dynamic data = response.httpResponse.body;
    print(data);
  }


}