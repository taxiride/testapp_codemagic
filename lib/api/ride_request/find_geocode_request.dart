import 'dart:convert';

import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:taxiride_ios/models/matrix.dart';
import 'package:taxiride_ios/models/ville.dart';

class GeocodeRequest{

  Future<Ville> getNamePosition(Position position) async {
    final http.Response response = await http.get(
      'https://graphhopper.com/api/1/geocode?point='+position.latitude.toString()+','+position.longitude.toString()+'&locale=fr&debug=true&reverse=true&key=d289c710-3054-40aa-8ee5-851d605f1fa5',
      headers: <String, String>{
        'Content-Type':'application/json; charset=UTF-8',
      },
    );
    if(response.statusCode == 200){
      print('------------ name position user -----------------------');
      Map<String,dynamic> hits_name = jsonDecode(response.body);
      List<dynamic> all_villes = hits_name['hits'];
      //String name_city = hits_name['hits'][0].name;
      all_villes.forEach((element) {
        Map<String,dynamic> data = element;
      //  print(data);
      //  print(data['name']);
        //print("***************************");
      });
     // print(all_villes.length);
      Ville ville = Ville(name: all_villes[0]['name'],city: all_villes[0]['city'],country: all_villes[0]['country']);
      return ville;
    }else{
      return null;

    }

  }

  getMatrix(Position position,double to_latitude,double to_longitude,String to_adresse_name,String from_adrese_name) async {
    print(to_latitude);
    print(to_longitude);
    print(from_adrese_name);
    print(to_adresse_name);
    final http.Response response = await http.post(
      'https://graphhopper.com/api/1/matrix?key=d289c710-3054-40aa-8ee5-851d605f1fa5',
      headers: <String, String>{
        'Content-Type':'application/json; charset=UTF-8',
      },
        body: jsonEncode(<String,dynamic>{
          'from_points': [position.latitude,position.longitude],
          'to_points':[to_latitude,to_longitude],
          'from_point_hints':from_adrese_name,
          'to_point_hints':to_adresse_name,
          'out_array':["times","distances"],
          'vehicle':"car"

        }),
    );

    if(response.statusCode == 200){
      print('------------ api Matrix succeffull -----------------------');
      print(response.statusCode);
      print(response.body);
    }else{
      print(response.statusCode);
      print(response.body);
      print('------------ api Matrix Lost lost-----------------------');

    }

  }

  Future<Matrix> getCalculMatrix(Position position,double to_latitude,double to_longitude,String to_adresse_name,String from_adrese_name) async {

    final http.Response response = await http.get(
      'https://graphhopper.com/api/1/matrix?point='+position.latitude.toString()+','+position.longitude.toString()+'&point='+to_latitude.toString()+','+to_longitude.toString()+'&type=json&vehicule=car&debug=true&out_array=weights&out_array=times&out_array=distances&key=d289c710-3054-40aa-8ee5-851d605f1fa5',
      headers: <String, String>{
        'Content-Type':'application/json; charset=UTF-8',
      },
    );

    if(response.statusCode == 200){
      Map<String,dynamic> matrix = jsonDecode(response.body);
      dynamic distance = matrix['distances'][0][1];
      dynamic temps = matrix['times'][0][1];
      print(response.statusCode);
      Matrix matrice = Matrix(distance: double.parse((distance/1000).toStringAsFixed(1)) ,time:(temps/60).ceil());
     // print(matrice.distance);
      //print(matrice.time);
     // print(response.body);
      return matrice;
    }else{
      print(response.statusCode);
      print(response.body);
      print('------------ api Matrix Lost lost -----------------------');
      return null;

    }

  }
}