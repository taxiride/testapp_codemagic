import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/taximen_car.dart';

class FindAllLongDistanceTripRequest extends AbstractAuthenticatedRequest{
  final String token;
  final String endCity;
  final String language;
  final String startCity;
  final String startDate;

  FindAllLongDistanceTripRequest(this.token, this.endCity, this.language, this.startCity, this.startDate):super(token);

  @override
  String getUrl() {
     return "/api/carpooling/findAllLongDistanceTrip/$startCity/$endCity/$startDate/$language";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print(jsonDecode(data));
      List<dynamic> responce = jsonDecode(data)["object"];
      print("---------- tout c 'est bien passe ---------");
      List<TaximenCar> taximensCar = TaximenCar.listFromJson(responce);
      return taximensCar;
    }else{
      print("---------- requete echouer ---------");
      return [];

    }
  }

}