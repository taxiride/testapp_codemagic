
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/models/taximen_car.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';

class CardCarTaximen extends StatefulWidget{
  final TaximenCar args;

  const CardCarTaximen({Key key, this.args}) : super(key: key);

  _CardTripState createState() => _CardTripState();

}

class _CardTripState extends State<CardCarTaximen>{
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Card(
        shadowColor: Colors.grey,
        shape: cardShape(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Card(
                    shadowColor: Colors.grey,
                    shape: cardShape(),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(0),
                          child: Container(
                            width: 80,
                            height: 80,
                            margin: EdgeInsets.all(1),
                            padding: EdgeInsets.all(0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            child:  (widget.args.driverImageUrl != null)?CircleAvatar(
                              radius: 100,
                              backgroundImage: NetworkImage('https://app.taxiride.biz:81/api/commons/download/'+widget.args.driverImageUrl) ,
                            ):Icon(Icons.account_circle),
                          ),
                        ),
                        Text(widget.args.driverName,style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            children: [
                              Icon(Icons.star,color: Colors.orange,),
                              Icon(Icons.star,color: Colors.orange,),
                              Icon(Icons.star,color: Colors.orange,),
                              Icon(Icons.star,color: Colors.orange,),
                              Icon(Icons.star,color: Colors.orange,)
                            ],
                          ),
                        )

                      ],
                    ),




                  ),
                  Container(
                    width: MediaQuery.of(context).size.width*0.5,
                    height: MediaQuery.of(context).size.height*0.5,
                    child: Column(
                      children: [
                        ListTile(
                          title: Text("Pick Up Point",style: TextStyle(fontSize: 10),),
                          subtitle: Text(widget.args.departure,style: TextStyle(fontWeight: FontWeight.bold),),
                        ),
                        //Divider(height: 8,color: Colors.black,),
                        ListTile(
                          title: Text("Drop Off",style: TextStyle(fontSize: 10)),
                          subtitle: Text(widget.args.arrivalAddress,style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        //Divider(height: 8,color: Colors.black,),
                        ListTile(
                          title: Text("Date/Time",style: TextStyle(fontSize: 10)),
                          subtitle: Text(widget.args.departureDate,maxLines:1,style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        //Divider(height: 8,color: Colors.black,),
                        ListTile(
                          title: Text("Seats Left",style: TextStyle(fontSize: 10)),
                          subtitle: Text(widget.args.reservationCount.toString()+" on "+widget.args.availableSeatCount.toString(),style: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        
                      ],
                    ),
                  )
                ],
              ),
            ),

            Container(
              padding: EdgeInsets.all(20),

              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Card(
                      shadowColor: Colors.grey,
                      shape: cardShape(),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text("Trip Price"),
                            Text(widget.args.seatCost.toString()+" N",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                            Text("CAN BID PRICE")
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: PrimaryButton(
                      text: 'REQUEST TO JOIN',
                      onPressed: (){},
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}