import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/post_lond_distance_trip_dto.dart';

class PostLongDistanceTripBookingRequest extends AbstractAuthenticatedRequest{
  final PostLongDistanceTripDto postLongDistanceTripDto;
  final String token;
  final String phone;

  PostLongDistanceTripBookingRequest(this.postLongDistanceTripDto, this.token,this.phone):super(token);

  @override
  String getUrl() {
    return "/api/carpooling/postLongDistanceTripBooking";
  }

  @override
  String getVerb() {
   return "POST";
  }

  @override
  getBody() {
    return {
    "departurePeriodEndDate": postLongDistanceTripDto.departureDate,
    "departurePeriodStartDate": postLongDistanceTripDto.departureDate,
    "emailOrPhone": phone,
    "endCity": postLongDistanceTripDto.endCity,
    "language": "fr",
    "numberOfPlacesRequired": postLongDistanceTripDto.numberOfSeat,
    "seatCost": postLongDistanceTripDto.cost,
    "startCity": postLongDistanceTripDto.startCity,
    "travelId": 0
  };
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print(jsonDecode(response.httpResponse.body));
      // cette response contient le routeId et le travelId
      Map<String,dynamic> obj = jsonDecode(response.httpResponse.body)["object"];

      print("----------- tout c'est bien Monsieur --------------------");
      return obj["travelId"];
    }else{
      print(jsonDecode(response.httpResponse.body));
      print(response.httpResponse.statusCode);
      print("--------------------- SAVE LOST ------------------------");
    }
  }
  
  
}