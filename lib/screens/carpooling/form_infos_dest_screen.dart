import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/carpooling/find_all_distance_trip_request.dart';
import 'package:taxiride_ios/api/carpooling/find_all_long_distance_trip_booking_request.dart';
import 'package:taxiride_ios/api/carpooling/find_all_user_panding_invitation_request.dart';
import 'package:taxiride_ios/api/carpooling/post_long_distance_trip_booking_request.dart';
import 'package:taxiride_ios/api/carpooling/save_car_pooling.dart';
import 'package:taxiride_ios/api/carpooling/save_long_distance_trip_request.dart';
import 'package:taxiride_ios/api/ride_request/find_geocode_request.dart';
import 'package:taxiride_ios/api/save_cab_profil_information_request.dart';
import 'package:taxiride_ios/models/condition_travel.dart';
import 'package:taxiride_ios/models/location.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';
import 'package:taxiride_ios/models/post_lond_distance_trip_dto.dart';
import 'package:taxiride_ios/models/taximen_car.dart';
import 'package:taxiride_ios/models/validator.dart';
import 'package:taxiride_ios/models/ville.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/providers/carpooling_provider.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/screens/carpooling/find_car_screen.dart';
import 'package:taxiride_ios/screens/carpooling/trip_invitation_screen.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:taxiride_ios/widget/button/secondary_button.dart';
import 'package:toast/toast.dart';

class FormInfosDestScreen extends StatefulWidget{

  @override
  _FormInfoDestState createState() => _FormInfoDestState();
  
}

class _FormInfoDestState extends State<FormInfosDestScreen>{



  bool stepOne_form = true;
  bool searchVille = false,selectDate=false,formStep2=false,formStep3=false,formStep4=false;
  TextEditingController selectCity = TextEditingController();
  TextEditingController arrivalCity = TextEditingController();
  TextEditingController myController = TextEditingController();
  List<PoolingTrip> listCity ;
  List<ConditionTravel> conditions;
  List<ConditionTravel> conditionsChecked = [];
  String ville = "";
  String destination ="",depart="",date="",time="",dateValidity="";
  Location current_position;
  var geolocator = Geolocator();
  Ville ville_rider;
  Locale myLocale;
  int prix=0,nbrePlace=0;
  String locationPick="";
  DateTime dateTime;

  String tokenKey;
  int travelId;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AuthProvider authProvider = Provider.of<AuthProvider>(context,listen:false);
    CarPoolingProvider carPoolingProvider = Provider.of<CarPoolingProvider>(context,listen: false);
    _getCurrentUserPosition();
     carPoolingProvider.getLocation(authProvider);
     //postLongDistanceTripDto = PostLongDistanceTripDto();


  }

  void _getCurrentUserPosition() async{


   Position position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
   // _getAddressFromLatLng(position);
   // print(position.latitude.toString() + ', ' + position.longitude.toString());
    GeocodeRequest geocodeRequest = GeocodeRequest();
    Ville riderPosition =  await geocodeRequest.getNamePosition(position);

    setState(() {
      ville_rider = riderPosition;
    });


  }
  @override
  Widget build(BuildContext context) {
    CarPoolingProvider carPoolingProvider = Provider.of<CarPoolingProvider>(context);
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    List<PoolingTrip> Cities = carPoolingProvider.poolingTrips ;

    final ProgressDialog pr = ProgressDialog(context);
    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

    return SingleChildScrollView(

      child: Container(
        
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            //premier formulaire contenant les informations de destination
            Visibility(
              visible: stepOne_form,
              child: Card(
                shadowColor: Colors.grey,
                shape: cardShape(),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 8,left: 2),
                      child: Container(
                        child: Text("PUBLISH A TRIP",style: TextStyle(fontWeight: FontWeight.bold,),),),
                    ),
                    Padding(
                      padding:const EdgeInsets.all(8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width*0.9,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Destination Town",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((destination == "")?"Enter destination town":destination,style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                              onTap: () async {
                                pr.show();
                                final prefs = await SharedPreferences.getInstance();
                                String token = prefs.getString("access_token");
                               // _getCurrentUserPosition();
                                //ici on affecte les informations concernant sa position courante : ville,pays,codePays
                               current_position = carPoolingProvider.location;
                               print(token);
                               listCity = await  carPoolingProvider.findArrivalCitie(token,current_position.countryCode,current_position.city,"fr");

                               conditions = await carPoolingProvider.getConditionsTravel(token,"fr");
                               // ajouter ces 2 premieres conditions par defaut dans la liste
                               conditions.forEach((element) {
                                 if(element.travelConditionValue =='Pas de chien' || element.travelConditionValue=='Pas de bavardage'){
                                   setState(() {
                                     conditionsChecked.add(element);

                                   });
                                 }
                               });

                               pr.hide();

                                //print(ville_rider.name);
                               setState(() {
                                 stepOne_form = false;
                                 searchVille = true;
                                 if(carPoolingProvider.poolingTrips == null){
                                   Toast.show("Your Token is EXpired", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                                 }else{}
                                  // listCity = carPoolingProvider.poolingTrips;
                                   //print(listCity.length);

                               });

                              },
                            ),
                            Divider(height: 8,color: Colors.black,),
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Arrival Town",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((depart=="")?"Enter departure town":depart,style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                            ),
                            Divider(height: 8,color: Colors.black,),
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Prefered Travel Date",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((date == "")?"Enter Prefered travel date":date,style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                              onTap: () async {
                                DateTime selectDate = DateTime.now();
                                final DateTime picked = await showDatePicker(
                                    context: context,
                                    initialDate: selectDate,
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2025)
                                );
                                if(picked != null && picked != selectDate){
                                  setState(() {
                                   // date = picked.toString();
                                    dateTime = picked;
                                    date = picked.year.toString()+"-"+picked.month.toString()+"-"+picked.day.toString();
                                    print(date);


                                  });
                                }
                              },
                            ),
                            Divider(height: 8,color: Colors.black,),
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Prefered Travel Time",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((time=="")?"Enter Prefered travel Time":time,style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                              onTap: () async {
                                TimeOfDay timePic = TimeOfDay.now();
                                TimeOfDay pickerTime = await showTimePicker(
                                    context: context,
                                    initialTime: timePic);
                                if(pickerTime != null && pickerTime != timePic){
                                  setState(() {
                                   // time = pickerTime.toString();
                                    String heure = (pickerTime.hour<10)?"0"+pickerTime.hour.toString():pickerTime.hour.toString();
                                    String minute = (pickerTime.minute<10)?"0"+pickerTime.minute.toString():pickerTime.minute.toString();
                                      time = heure+":"+minute;


                                    print(time);
                                  });
                                }

                              },
                            ),
                            Divider(height: 8,color: Colors.black,),
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Destination Town",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((dateValidity=="")?"Enter validity period":dateValidity,style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                              onTap: () async {
                                DateTime selectDate = DateTime.now();
                                final DateTime picked = await showDatePicker(
                                    context: context,
                                    initialDate: selectDate,
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2025)
                                );
                                if(picked != null && picked != selectDate){
                                  setState(() {
                                    dateValidity = picked.toString();


                                  });
                                }
                              },
                            ),
                            Divider(height: 8,color: Colors.black,)
                          ],
                        ),
                      ), ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SecondaryButton(child: Icon(Icons.close,color: Colors.white,),onPressed: (){
                           // Navigator.pop(context);
                          },),
                          BlackButton(child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("NEXT",style: TextStyle(color: Colors.white),),
                              Icon(Icons.navigate_next,color: Colors.white,)
                            ],
                          ),onPressed: (){
                            if((destination=="")||(depart=="")||(date=="")||(time=="")){
                              Toast.show("Please enter all informations", context,duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                            }else{
                              setState(() {
                                // postLongDistanceTripDto.departureDate = date+time;
                                stepOne_form = false;
                                formStep2 = true;

                              });

                            }



                          },)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Visibility(
              visible: searchVille,
              child: (ville == "")?Container(
                child: Column(
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: AutoCompleteTextField<PoolingTrip>(
                            controller: selectCity,
                            suggestions: listCity,
                            style: TextStyle(color: Colors.black,fontSize: 16),
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30)
                                )
                            ),
                            itemFilter: (item,query){
                              //print(carPoolingProvider.poolingTrips.length);
                              return item.arrivalCity.toString().toLowerCase().startsWith(query.toLowerCase());
                            },
                            itemSorter: (a,b){
                              return a.arrivalCity.toString().compareTo(b.arrivalCity);
                            },
                            itemSubmitted: (item){
                              print("-------------------------");
                              selectCity.text = item.arrivalCity;
                              print(selectCity.text.toString());
                            },
                            clearOnSubmit: false,
                            itemBuilder: (context,item){
                              return Container(
                                padding: EdgeInsets.all(5),
                                child: ListTile(
                                  leading: Icon(Icons.near_me,color: Colors.yellow,),
                                  title: Text(item.arrivalCity,style: TextStyle(color: Colors.black),),
                                  trailing: Icon(Icons.add,color: Colors.orange,),
                                  onTap: (){
                                    setState(() {
                                      ville = item.arrivalCity;

                                    });
                                  },

                                ),
                              );
                            },

                          ),
                        ),
                      ],
                    ),
                    (Validator.isNotEmpty(selectCity.text.toString()))?Container(
                      padding: EdgeInsets.all(10),
                      child: PrimaryButton(
                        text: "SEARCH",
                        onPressed: (){
                          setState(() {
                            ville = selectCity.text.toString();
                          });
                        },
                      ),
                    ):SizedBox(height: 5,)

                  ],
                ),

              ):SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height*0.4,
                  margin: EdgeInsets.all(10),
                  child: Card(
                    shadowColor: Colors.grey,
                    shape: cardShape(),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Text("Enter Your Destination",style: TextStyle(fontWeight: FontWeight.bold),),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: TextFormField(
                            controller: arrivalCity ,
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.white
                            ),
                            cursorColor: Colors.black,
                          ),
                          

                        ),
                       Container(
                         padding: EdgeInsets.all(20),
                         child: PrimaryButton(text: "valid",onPressed: (){
                           setState(() {
                             print(ville_rider.city);

                             destination = ville+"-"+arrivalCity.text.toString();
                             searchVille = false;
                             stepOne_form = true;
                             depart = ville_rider.city;
                             print(depart);
                             print("--------------------------");
                           });
                         },),
                       )
                       ,
                      ],
                    ),
                  ),
                ),
              ),

            ),
            Visibility(
              visible: formStep2,
              child: Card(
                shadowColor: Colors.grey,
                shape: cardShape(),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 8,left: 2),
                      child: Container(
                        child: Text("PUBLISH A TRIP",style: TextStyle(fontWeight: FontWeight.bold,),),),
                    ),
                    Padding(
                      padding:const EdgeInsets.all(8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width*0.9,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Price",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((prix == 0)?"Your Price":prix.toString(),style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                              onTap: (){
                                _showDialog("Enter your Price","price", 0);
                                setState(() {

                                });

                              },
                            ),
                            Divider(height: 8,color: Colors.black,),
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Prefered Pick Up Location",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((locationPick == "")?"Enter pick up location":locationPick,style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                              onTap: (){
                                _showDialog("Pick Up Location","pickUp name", 1);
                                setState(() {
                                  //postLongDistanceTripDto.departure = locationPick;

                                });

                              },
                            ),
                            Divider(height: 8,color: Colors.black,),
                            ListTile(
                              leading: Icon(Icons.near_me,color: Colors.orange,),
                              title: Text("Number Of Seats",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                              subtitle: Text((nbrePlace== 0)?"Enter number of seats":nbrePlace.toString(),style: TextStyle(fontSize: 15),),
                              trailing: Icon(Icons.mode_edit),
                              onTap: (){
                                _showDialog("Number Seat","place", 2);
                                setState(() {
                                //  postLongDistanceTripDto.numberOfSeat = nbrePlace;

                                });

                              },
                            ),
                            Divider(height: 8,color: Colors.black,),

                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SecondaryButton(child: Icon(Icons.close,color: Colors.white,),onPressed: (){
                            setState(() {
                              formStep2=false;
                              stepOne_form=true;
                            });
                          },),
                          PrimaryButton(child: Row(
                            children: [
                              Text("Publish & Find a car",style: TextStyle(color: Colors.white),),
                              Icon(Icons.navigate_next,color: Colors.white,)
                            ],
                          ),onPressed: () async {
                            if((prix==0)||(locationPick=="")||(nbrePlace==0)){
                              Toast.show("Information not Complete", context,duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

                            }else{
                              pr.show();
                              String mytoken = await authProvider.tokenKey;
                              final prefs = await SharedPreferences.getInstance();
                              String myphone = await prefs.getString('username');

                              PostLongDistanceTripDto postTrip = PostLongDistanceTripDto(
                                  startCity: current_position.city,
                                  endCity: ville,
                                  country: current_position.country,
                                  departureLatitude: current_position.lat,
                                  departureLongitude: current_position.lon,
                                  arrival: arrivalCity.text.toString(),
                                  departureDate:date+"T"+time+":00.336Z",
                                  cost: prix,
                                  departure: locationPick,
                                  numberOfSeat: nbrePlace,

                              );
                              PostLongDistanceTripBookingRequest postLongdistancebk = PostLongDistanceTripBookingRequest(postTrip,mytoken,myphone);
                              int idTravel = await postLongdistancebk.parseResult(await ApiClient.execOrFail((postLongdistancebk)));
                              if(idTravel != null){
                                await prefs.setInt("riderIdTravel",idTravel);
                                await prefs.setString("riderDepart",current_position.city);
                                await prefs.setString("riderArriver",ville);
                                await prefs.setString("riderDate",date);
                                setState(() {
                                  // on sauvegarde l'id du voyage
                                  travelId = idTravel;
                                  formStep2=false;
                                  pr.hide();
                                  formStep4 = true;
                                });

                              }

                            }

                          },)
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
            Visibility(
              visible: formStep3,
              child: Card(
                shadowColor: Colors.grey,
                shape: cardShape(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,

                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 8,left: 2),
                      child: Container(
                        child: Text("PUBLISH A TRIP",style: TextStyle(fontWeight: FontWeight.bold,),),),
                    ),
                    Divider(height: 8,color: Colors.black,),
                    Padding(
                      padding:const EdgeInsets.all(8.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width*0.9,
                        height: MediaQuery.of(context).size.height*0.6,
                        child: ListView.builder(
                            itemCount: (conditions != null)?conditions.length+1:0,
                            itemBuilder: (context,index){
                              if(index<conditions.length){
                                ConditionTravel element = conditions[index];
                                return  ListTile(
                                  leading: Icon(Icons.info,color: Colors.orange,),
                                  title: Text("Travel condition here",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                                  subtitle: Text(element.travelConditionValue,style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                                  trailing: (element.checked)?Icon(Icons.check_circle,color: Colors.green,):Icon(Icons.check),
                                  onTap: (){
                                    setState(() {
                                      element.checked = !element.checked;
                                      testCondition(element);


                                    });
                                  },
                                );
                              }
                              return ListTile(
                                title: Text("Custom condition"),
                                subtitle: Text("Enter condition"),
                                leading: Icon(Icons.info,color: Colors.orange,),
                                trailing: Icon(Icons.edit),
                                onTap: (){
                                  _showDialog("Add your Condition", "custum condition", 3);
                                },
                              );
                            })
                      ),
                    ),
                    Container(

                      padding: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SecondaryButton(child: Icon(Icons.arrow_back_ios,color: Colors.white,),onPressed: (){
                            setState(() {
                              formStep3 = false;
                              formStep2 = true;
                            });
                          },),
                          BlackButton(text: "PUBLISH & FIND PAS",onPressed: () async {

                            pr.show();
                            String mytoken = await authProvider.tokenKey;
                            final prefs = await SharedPreferences.getInstance();
                            String myphone = await prefs.getString('username');
                            List<String> travelConditions = [];
                            conditionsChecked.forEach((element) {
                              travelConditions.add(element.travelConditionValue);
                            });
                              PostLongDistanceTripDto postTrip = PostLongDistanceTripDto(
                              startCity: current_position.city,
                              endCity: ville,
                              country: current_position.country,
                              departureLatitude: current_position.lat,
                              departureLongitude: current_position.lon,
                              arrival: arrivalCity.text.toString(),
                              departureDate:date+"T"+time+":00.336Z",
                              cost: prix,
                              departure: locationPick,
                              numberOfSeat: nbrePlace,
                              travelCondition: travelConditions
                            );
                            PostLongDistanceTripBookingRequest postLongdistancebk = PostLongDistanceTripBookingRequest(postTrip,mytoken,myphone);
                            int idTravel = await postLongdistancebk.parseResult(await ApiClient.execOrFail((postLongdistancebk)));
                            print(idTravel);
                            setState(() {
                              // on sauvegarde l'id du voyage
                              travelId = idTravel;
                             formStep3=false;
                              pr.hide();
                            formStep4 = true;
                            });

                          },)
                        ],
                      ),
                    )

                  ],
                ),

              ),
            ),
            Visibility(
              visible: formStep4,
              child: Column(
                children: [
                  Card(
                    shadowColor: Colors.grey,
                    shape: cardShape(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,

                      children: [
                    Padding(
                          padding: const EdgeInsets.only(top: 8,left: 2),
                          child: Container(
                            child: Text("PENDING TRIP",style: TextStyle(fontWeight: FontWeight.bold,),),),
                        ),
                        Padding(
                          padding:const EdgeInsets.all(8.0),
                          child: Container(
                            width: MediaQuery.of(context).size.width*0.9,
                            child: Column(
                              children: [
                                ListTile(
                                  leading: Icon(Icons.near_me,color: Colors.orange,),
                                  title: Text("To",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                                  subtitle: Text((ville=="")?"End City not found":ville),
                                  trailing: Icon(Icons.mode_edit),
                                  onTap: (){
                                    setState(() {
                                     // stepOne_form = false;
                                      //searchVille = true;
                                    });
                                  },
                                ),
                                Divider(height: 8,color: Colors.black,),
                                ListTile(
                                  leading: Icon(Icons.place,color: Colors.orange,),
                                  title: Text("From",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                                  subtitle: Text((current_position==null)?"start city not found":current_position.city),
                                  trailing: Icon(Icons.mode_edit),
                                  onTap: (){
                                    setState(() {
                                      stepOne_form = false;
                                      searchVille = true;
                                    });
                                  },
                                ),

                                Divider(height: 8,color: Colors.black,),
                                ListTile(
                                  leading: Icon(Icons.place,color: Colors.orange,),
                                  title: Text("Date/Time",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                                  subtitle: Text(date+" at "+time),
                                  trailing: Icon(Icons.mode_edit),
                                  onTap: (){
                                    setState(() {
                                      stepOne_form = false;
                                      searchVille = true;
                                    });
                                  },
                                ),

                                Divider(height: 8,color: Colors.black,),

                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width*0.8,
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: PrimaryButton( child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      color: Colors.black,
                                      shape: BoxShape.circle
                                  ),
                                  child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Center(child: Text("3",style: TextStyle(color: Colors.white),)),
                                )
                                ),
                                Container(
                                  child: Text("TRIP INVITATIONS",style: TextStyle(color: Colors.white),),
                                )
                              ],
                            ),
                              onPressed: () async {
                                pr.show();
                                String mytoken = await authProvider.tokenKey;
                                final prefs = await SharedPreferences.getInstance();
                                String myphone = await prefs.getString('username');

                              //FindAllLongDistanceTripRequest findLongdistancetrip = FindAllLongDistanceTripRequest(mytoken,ville,"fr",current_position.city,date);
                              //List<dynamic> trips =  await findLongdistancetrip.parseResult(await ApiClient.execOrFail(findLongdistancetrip));
                              FindAllUserPandingInvitationRequest findallUser = FindAllUserPandingInvitationRequest(mytoken,myphone,"fr",travelId);
                              List<dynamic> trips = await findallUser.parseResult(await ApiClient.execOrFail(findallUser));
                              pr.hide();
                             Navigator.push(context, MaterialPageRoute( builder: (_) => TripInvitation(tripsDispo: trips,)));
                            },),
                          ),
                        )

                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width*0.53,
                    child: BlackButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(Icons.near_me,color: Colors.orange,),
                          Text("FIND A CAR",style: TextStyle(color: Colors.white),)
                        ],
                      ),
                      onPressed: () async {
                        pr.show();
                        String mytoken = await authProvider.tokenKey;
                        FindAllLongDistanceTripRequest findLongdistancetrip = FindAllLongDistanceTripRequest(mytoken,ville,"fr",current_position.city,date);
                        List<TaximenCar> taximensCar =  await findLongdistancetrip.parseResult(await ApiClient.execOrFail(findLongdistancetrip));
                        pr.hide();
                     //   Navigator.push(context, MaterialPageRoute( builder: (_) => TripInvitation(tripsDispo: trips,)));
                       Navigator.push(context, MaterialPageRoute( builder: (_) => FindCarScreen(carDispos: taximensCar,)));

                      },
                    ),
                  )
                ],
              ) ,
            )


          ],
        ),
      ),
    );


  }

  void _showDialog(String title,String champ,int position) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Trip Information"),
          content: new Text(title),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child: TextField(
                    controller: myController,
                    keyboardType: (position%2==0)?TextInputType.number:TextInputType.text,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: champ,
                    ),
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      PrimaryButton(text: 'Save',onPressed: (){

                        setState((){
                          if(position==0){
                            prix = int.parse(myController.text.toString());
                          }else if(position == 1){
                            locationPick = myController.text.toString();
                          }else if(position == 2){
                            nbrePlace = int.parse(myController.text.toString());
                          }else if(position == 3){
                            ConditionTravel cond = ConditionTravel(travelConditionDescription: myController.text.toString(),travelConditionValue: myController.text.toString(),checked: false);
                            conditions.add(cond);
                          }

                          Navigator.of(context).pop();
                          myController.text="";

                        });

                        // saveCabInformation(position);
                      },),
                      SecondaryButton(text: 'Close',onPressed: (){Navigator.of(context).pop();},)
                    ],
                  ),
                )
              ],
            ),

          ],
        );
      },
    );
  }

  void testCondition(ConditionTravel conditionTravel){
    if(conditionsChecked.length > 0){
      if(conditionsChecked.contains(conditionTravel)){
        // si la liste contient l element taper on le retire de la liste des elements checker
       setState(() {
         conditionsChecked.remove(conditionTravel);
       });
      }else{
        // sinon on  l'ajoute a la  liste des elements checker
        setState(() {
          conditionsChecked.add(conditionTravel);
        });
      }
    }else{
      setState(() {
        conditionsChecked.add(conditionTravel);
      });

    }
    print(conditionsChecked.length);
  }


  
}