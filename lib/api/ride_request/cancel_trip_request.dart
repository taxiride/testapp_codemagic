import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';

class CancelTripRequest extends AbstractAuthenticatedRequest{
  final String token;
  final int idtrip;
  final String phone;

  CancelTripRequest(this.token, this.idtrip,this.phone):super(token);

  @override
  String getUrl() {
    return "/api/rides/cancelTrip";
  }

  @override
  String getVerb() {
    return "POST";
  }

  @override
  getBody() {
     return {
       "emailOrPhone": phone,
       "language": "fr",
       "tripId": idtrip
     };
  }

  @override
  parseResult(AbstractApiResponse response) {
   dynamic data = response.httpResponse.body;
   print(response.httpResponse.statusCode);
   print(data);
  }


}