import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
//import 'package:location/location.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:country_code_picker/country_code_picker.dart';
import 'login.dart';
import 'package:taxiride_ios/models/apiresponse/resp.dart';


class RegisterPage extends StatefulWidget{
  RegisterPage();

  @override
  _RegisterStatePage createState() => _RegisterStatePage();
}

class _RegisterStatePage extends State<RegisterPage>{

  final _registerkey = GlobalKey<FormState>();
  final _personnalInfoKey = GlobalKey<FormState>();
  bool isDriverSelected = false;
  bool isRiderSelected = true;
  //Location location = new Location();
  Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
  Position position;
  //LocationData _locationData;
  String phoneCode = "+237";


  _getCurrentLocation() {
    
    geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((Position p) {

      setState(() {
        position = p;
      });
    });

  }

  Card _buildRoundCardForTextField(String hint, t, TextEditingController controller){
    return Card(
//      shadowColor: Colors.grey,
//      elevation: 8,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildRoundTextField(hint, t, controller)
          ],
        ),
      ),
    );
  }

  TextFormField _buildRoundTextField(String hint, t, TextEditingController controller){
    return TextFormField(
      validator: (value){
        if(value.isEmpty){
          return 'This field is required';
        }
      },
      decoration: new InputDecoration(
          border: InputBorder.none,
          filled: true,
          hintText: hint,
          fillColor: Colors.white
      ),
      keyboardType: t,
      cursorColor: Colors.black,
      controller: controller,
    );
  }

   changeRiderColor(bool isSelected){
    setState(() {
      isRiderSelected = isSelected;
      if(isRiderSelected) changeDriverColor(false);
    });
   
  }

  changeDriverColor(bool isSelected){
    setState(() {
        isDriverSelected = isSelected;
        if(isDriverSelected) changeRiderColor(false);
    });
   
  }

//construit la colonne pour choisir un rôle
  Column _buildRoleColumn(String label, String image, GlobalKey _key){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        RaisedButton(
          child: Column(
            children: <Widget>[
              Image.asset(image,
                width: 122,
                height: 122,
              ) ,
              Container(
                margin: const EdgeInsets.only(top: 8),
                child: Text(
                  label,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w800,
                      
                      fontFamily: 'fonts/Poppins-Bold.ttf'
                  ),
                ),
              ),
            ],
          ),

        ),

      ],

    );
  }

  void _navivgateToMain(){
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => NavigationRide('rider', 'register' )));
  }

  @override
  Widget build(BuildContext context) {

    final phonecontroller  = TextEditingController();
    final emailcontroller  = TextEditingController();
    final passcontroller  = TextEditingController();
    final firstnamecontroller  = TextEditingController();
    final lasnamecontroller  = TextEditingController();
    final codecontroller  = TextEditingController();

    final ProgressDialog pr = ProgressDialog(context);

    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

    //bouton pour continuer lorsqu'on a choisit un rôle
    //il est désactivé par défaut
    RaisedButton _buildRoundChooseRoleButton(Color col, String label){
      return RaisedButton(

          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side:BorderSide(color: col  ),

          ),
          onPressed: (){
            _navivgateToMain();
          },
          color: col,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(left: 16),
                  child: Center(child: Text( label, style: TextStyle(fontSize: 14, color: Colors.white, fontFamily: 'fonts/Poppins-Regular.ttf')),)
              )
            ],
          )
      );

    }

    //choix d'un rôle
    void _chooseRole(){
      Navigator.of(context).push(
          MaterialPageRoute<void>(
              builder: (BuildContext ctx2){

                return Scaffold(
                  body: Column(
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 64),
                        child: Center(
                          child: Text('GET STARTED', style: TextStyle(fontSize: 16,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Poppins' ),
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(top: 16, bottom: 25),
                            child: Center(
                              child: Text('How would like to continue', style: TextStyle(fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Poppins' ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                RaisedButton(
                                  color: isRiderSelected ? Colors.amber:Colors.white,
                                  child:  Image.asset('images/icon_rider.png',
                                    width: 122,
                                    height: 122,
                                  ) ,
                                  onPressed: (){
                                    isRiderSelected = !isRiderSelected;
                                    changeRiderColor(isRiderSelected);
                                  },
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 8),
                                  child: Text(
                                    'Rider',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: isRiderSelected ? Colors.amber:Colors.black,
                                        fontFamily: 'fonts/Poppins-Bold.ttf'
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                RaisedButton(
                                  color: isDriverSelected ? Colors.amber:Colors.white,
                                  child:  Image.asset('images/driver.png',
                                    width: 122,
                                    height: 122,
                                  ) ,
                                  onPressed: (){
                                    isDriverSelected = !isDriverSelected;
                                    changeDriverColor(isDriverSelected);
                                  },
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 8),
                                  child: Text(
                                    'Driver',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: isDriverSelected ? Colors.amber:Colors.black,
                                        fontFamily: 'fonts/Poppins-Bold.ttf'
                                    ),
                                  ),
                                ),
                              ],
                            ),

                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 25),
                        child:  _buildRoundChooseRoleButton(Colors.green, 'Go'),
                      ),
                    ],
                  ),
                );
              }
          )
      );
    }

    RaisedButton _buildRoundPersoInfoButton(Color col, String label){
      return RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side:BorderSide(color: col  ),

          ),
          onPressed: (){
            //récupération de la localisation courante
            _chooseRole();
          },
          color: col,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(left: 16),
                  child: Center(child: Text( label, style: TextStyle(fontSize: 14, color: Colors.white, fontFamily: 'Poppins')),)
              )
            ],
          )
      );

    }

    //affiche le formulaire pour entrer les informations personnlles
    void _enterPersonalInfomation(){

      //_getCurrentLocation();
      String val = 'M';
      Navigator.of(context).push(
          MaterialPageRoute<void>(
              builder: (BuildContext ctx1){
                return Scaffold(
                  body: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Form(
                        key: _personnalInfoKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(top: 32),
                              child: Center(
                                child: Text('GET STARTED', style: TextStyle(fontSize: 16, fontFamily: 'fonts/Poppins-Bold.ttf' ),
                                ),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(left: 16, top: 32),
                              child: Column(

                                children: <Widget>[

                                  Text('Welcome', style: TextStyle(
                                    fontSize: 28,
                                    fontFamily: 'fonts/Poppins-Bold.ttf',
                                    fontWeight: FontWeight.bold,
                                  ),
                                  ),
                                  Text(

                                    'Whats your name',
                                    style: TextStyle(fontSize: 12,fontFamily: 'fonts/Poppins-Regular.ttf'),
                                  ),
                                ],
                              ),
                            ),

                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  margin: const EdgeInsets.only(top: 60, left: 16, right: 16),
                                  child: _buildRoundCardForTextField('First name', TextInputType.text, firstnamecontroller),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 25, bottom: 25, left: 16, right: 16),
                                  child: _buildRoundCardForTextField('Last name', TextInputType.text, lasnamecontroller),
                                ),
                                Container(
                                  width: 240,
                                  margin: const EdgeInsets.only(top: 25, bottom: 25, left: 16, right: 16),
                                  child: DropdownButtonFormField<String>(
                                    value: val,
                                    icon: Icon(Icons.keyboard_arrow_down),
                                    iconSize: 14,
                                    elevation: 16,
                                    onChanged: (String newVal){
                                      setState(() {
                                        val = newVal;
                                      });
                                    },
                                    items: <String>['M','F']
                                        .map<DropdownMenuItem<String>>((String value){
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                            _buildRoundPersoInfoButton(Colors.green, 'Continue'),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }
          )
      );
    }

    _callVerifyCode(String codeEntered, String phone) async{
      
      await pr.show();
      final http.Response response = await http.post('https://app.taxiride.biz:81/api/auth/registerconfirmationbyphone',
        headers: <String, String>{
          'Content-Type':'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'code': codeEntered,
          'phoneNumber':phone
        }),
      );

      await pr.hide();

      if(response.statusCode == 200){

        _enterPersonalInfomation();

        Resp resp = Resp.fromJson(json.decode(response.body));

//        if(resp.success){
//          _enterPersonalInfomation();
//        }

      }else{
        log('error ');
      }
    }

    //construit le bouton pour vérifier le code, et définit l'action qui suit le click
    RaisedButton _buildRoundVerifyCodeButton(Color col, String label){
      return RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side:BorderSide(color: col  ),

          ),
          onPressed: (){
           // _enterPersonalInfomation();
            _callVerifyCode(codecontroller.text, phonecontroller.text);
          },
          color: col,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(left: 16),
                  child: Center(child: Text( label, style: TextStyle(fontSize: 14, color: Colors.white, fontFamily: 'fonts/Poppins-Regular.ttf')),)
              )
            ],
          )

      );

    }

    //page pour entrer le cde
    void _enterCode(){
      Navigator.of(context).push(
          MaterialPageRoute<void>(
              builder: (BuildContext ctx){
                return Scaffold(
                  body: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 32),
                        child: Center(
                          child: Text('GET STARTED', style: TextStyle(fontSize: 16, fontFamily: 'fonts/Poppins-Bold.ttf' ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 16, top: 32),
                        child:  Text('Enter your OTP code below',
                          style: TextStyle(fontSize: 28, fontFamily: 'fonts/Poppins-Bold.ttf'),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 60, left: 16, right: 16),
                        child: _buildRoundCardForTextField('Enter the code', TextInputType.text, codecontroller),
                      ),
                      _buildRoundVerifyCodeButton(Colors.green, 'Validate code'),
                      GestureDetector(
                        child:  Container(
                          margin: const EdgeInsets.only(top: 25, right: 16),
                          child: Center(
                            child:Text('Resend code in 10 minutes',
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                );

              }
          )
      );
    }


    //on ouvre la page de profil en passant l'adresse email, le nom
    // _navigateToChooseRole();
    _callRegister(String phone, String email, String pass) async{
      await pr.show();
      final http.Response response = await http.post('https://app.taxiride.biz:81/api/auth/register',
        headers: <String, String>{
          'Content-Type':'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'email': email,
          'phoneNumber':phone,
          'password': pass,
          'contryCode':phoneCode
        }),
      );

      log('code register: '+ response.statusCode.toString());

      await pr.hide();

      if(response.statusCode == 200){
        _enterCode();
      }else{
        log('error ');
      }

    }
    //bouton avec forme arrondi
    RaisedButton _buildRoundRegisterButton(Color col, String label){
      return RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side:BorderSide(color: col  ),

          ),
          onPressed: (){
            if(_registerkey.currentState.validate() ){

              //_enterCode();
              _callRegister(phonecontroller.text, emailcontroller.text, passcontroller.text);
            }
          },
          color: col,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(left: 16),
                  child: Center(child: Text( label, style: TextStyle(fontSize: 14, color: Colors.white, fontFamily: 'fonts/Poppins-Regular.ttf')),)
              )
            ],
          )

      );

    }

    void _onCountryChange(CountryCode countryCode) {
      //TODO : manipulate the selected country code here
      print("New Country selected: " + countryCode.toString());
      phoneCode = countryCode.dialCode;
    }
    //construit les les différents textfields qui sont utilisés pour le register
    Widget roundTextField = Container(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
                child:_buildRoundCardForTextField('Phone number', TextInputType.phone, phonecontroller),
//                Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: <Widget>[
//                    CountryCodePicker(
//                      onChanged: _onCountryChange,
//                      showCountryOnly: false,
//                      showOnlyCountryWhenClosed: false,
//                      alignLeft: false,
//                    ),
//                    _buildRoundCardForTextField('Phone number', TextInputType.phone, phonecontroller),
//                  ],
//                ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 16, left: 16, right: 16),
              child: _buildRoundCardForTextField('Email adress', TextInputType.emailAddress, emailcontroller),
            ),
            Container(
              margin: const EdgeInsets.only(top: 16, bottom: 25, left: 16, right: 16),
              child: _buildRoundCardForTextField('Password', TextInputType.visiblePassword,passcontroller),
            )
          ],
        ),
      )
    );

    //ouvre la page de login
    void _navivgateToLogin(){
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LoginPage()));
    }

    //page de register
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Image.asset('images/ellipse_auth.png',
                fit: BoxFit.cover,
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: Icon(
                    Icons.chevron_left,
                    color: Colors.white,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child:   Container(
                  margin: const EdgeInsets.only(top: 32),
                  child:  Column(
                    children: <Widget>[
                      Text('Register', style: TextStyle(fontSize: 28,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'fonts/Poppins-Bold.ttf'),
                      ),
                      Text(
                        'Fill out the form below to enter on our system',
                        style: TextStyle(fontSize: 12,fontFamily: 'fonts/Poppins-Regular.ttf'),
                      ),
                    ],
                  ),
                ),
              ),

            ],
          ),

          Center(
            child: Form(
              key: _registerkey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  roundTextField,
                  _buildRoundRegisterButton(Colors.green, 'Continue')
                ],
              ),
            ),
          ),

          Align(
            alignment: Alignment.bottomCenter,
            child:    GestureDetector(
              onTap: _navivgateToLogin,
              child:  Container(
                margin: const EdgeInsets.only(top: 25, right: 16, bottom: 32),
                child: Center(
                  child:Text('Go to login',
                  ),
                ),
              ),
            ),
          ),


        ],
      ),
    );

  }
}