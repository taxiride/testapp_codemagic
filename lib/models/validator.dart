import 'email_validator.dart';

class Validator {

  static bool isEmail(String email) {
    return EmailValidator.validate(email.trim());
  }



  static bool isNotEmpty(String value) {
    return value != "" && value != null;
  }


}