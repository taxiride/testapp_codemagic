
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/find_attachment_by_phone_request.dart';
import 'package:taxiride_ios/api/find_cab_images_by_phone_request.dart';
import 'package:taxiride_ios/api/find_cab_information_by_phone_request.dart';
import 'package:taxiride_ios/api/find_file_by_url_request.dart';
import 'package:taxiride_ios/api/save_cab_profil_information_request.dart';
import 'package:taxiride_ios/api/save_profil_attachment_request.dart';
import 'package:taxiride_ios/models/file_attachment.dart';
import 'package:taxiride_ios/models/image_car.dart';
import 'package:taxiride_ios/models/vehicule.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:taxiride_ios/widget/button/secondary_button.dart';
import 'package:taxiride_ios/widget/layout/custom_images_picker.dart';
import 'package:toast/toast.dart';

class AddCarScreen extends StatefulWidget{

  @override
  _addCarScreenState createState() => _addCarScreenState();
}

class _addCarScreenState extends State<AddCarScreen>{
  final myController = TextEditingController();
  bool active_detail_car = false;
  bool active_document_car = false;
  List<File> _images = [];
  List<ImageCar> carImages = [];
  int position = 0;
  Future<List<ImageCar>> futur_images;
  final int maxImageCount = 5;
  bool btn_save = false;
  bool btn_doc = false;
  bool btn_edit = true;
  Vehicule _vehicule;
  Vehicule car;
  int active_btn = 1;
  List<String> carName = ["images/benz.png","images/bmw.jpg","images/corol.png","images/ferary.jpg",];

  List<String> filenames = ["Document.pdf","Document.pdf","Document.pdf","Document.pdf","Document.pdf","Document.pdf"];
  String name1;
  String name2;
  String name3;
  String name4;
  String name5;
  String name6;
  String type ,brand,model,matricule,constructor;





  Future<void> getImage() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.get("access_token");
    String phone = prefs.getString('username');
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      print(image);
      position ++;
      _images.add(image);
    });
    uploadImage(image, token, phone, position).then((value){
      print(value);
    });
  }

  Future<void> getImageCamera() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.get("access_token");
    String phone = prefs.getString('username');
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      print(image);
      position ++;
      _images.add(image);
    });
    uploadImage(image, token, phone, position).then((value){
      print(value);
    });
  }

  Future<void> getPdf(int position) async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    String phone = prefs.getString('username');

    File pdf = await ImagePicker.pickImage(source: ImageSource.gallery);
    String filename = pdf.path.split("/").last;
    setState((){
      if(position == 1){
        name1 = filename;
      }else if(position == 2 ){
        name2 = filename;
      }else if(position == 3 ){
        name3 = filename;
      }else if(position == 4){
        name4 = filename;
      }else if(position == 5 ){
        name5 = filename;
      }else if(position == 6){
        name6 = filename;
      }
    });
    SaveProfilAttachmentRequest saveProfilAttachmentRequest = SaveProfilAttachmentRequest();
    saveProfilAttachmentRequest.uploadImage(pdf, token, phone,position);
  }

  Future<void> getPdfWithCamera(int position) async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    String phone = prefs.getString('username');

    File pdf = await ImagePicker.pickImage(source: ImageSource.camera);
    String filename = pdf.path.split("/").last;
    setState((){
      if(position == 1){
        name1 = filename;
      }else if(position == 2 ){
        name2 = filename;
      }else if(position == 3 ){
        name3 = filename;
      }else if(position == 4){
        name4 = filename;
      }else if(position == 5 ){
        name5 = filename;
      }else if(position == 6){
        name6 = filename;
      }
    });
    SaveProfilAttachmentRequest saveProfilAttachmentRequest = SaveProfilAttachmentRequest();
    saveProfilAttachmentRequest.uploadImage(pdf, token, phone,position);
  }

  @override
   initState() {

   // findImagesCar();
    car = Vehicule();
    futur_images = findImagesCar();
    super.initState();

  }

  Future<List<ImageCar>> findImagesCar() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    String phone = prefs.getString('username');

    FindCabImagesByPhoneRequest findCabImagesByPhoneRequest = FindCabImagesByPhoneRequest(token,"en",phone);
    List<ImageCar>  rep = findCabImagesByPhoneRequest.parseResult(await ApiClient.execOrFail(findCabImagesByPhoneRequest));

    if(rep.length > 0){
      setState(() {
        btn_save = true;
      });
    }
    rep.forEach((element) async {
      print(element.pictureURL);

    });

    print(rep);
    return rep;
  }


  @override
  Widget build(BuildContext context) {
    final MediaQueryData _mq = MediaQuery.of(context);
     return MaterialApp(
       home: Scaffold(
         body: SingleChildScrollView(
           child: ConstrainedBox(
             constraints: BoxConstraints(
                 minHeight: _mq.size.height - _mq.padding.top - 73),
             child: Column(
               children: [
                 Padding(padding: const EdgeInsets.only(bottom: 2),
                 child: Stack(
                   children: [
                     Image.asset('images/ellipse_auth.png',
                       height: MediaQuery.of(context).size.height*0.5,
                       width: MediaQuery.of(context).size.width,
                       fit: BoxFit.cover,
                     ),
                     Align(
                       alignment: Alignment.topLeft,
                       child: GestureDetector(
                           child:Container(
                             margin: const EdgeInsets.only(top: 32, left: 16),
                             child: Icon(
                               Icons.chevron_left,
                               color: Colors.black,
                             ),
                           ),
                           onTap: (){
                             Navigator.pop(context);
                           }
                       ),
                     ),
                     Align(
                       alignment: Alignment.topCenter,
                       child:  Container(
                         margin: const EdgeInsets.only(top: 32),
                         child: Text('ADD YOUR CAR', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, fontFamily: 'fonts/Poppins-Bold.ttf'),

                         ),
                       ),
                     ),

                     Align(
                       alignment: Alignment.topCenter,
                       child: FutureBuilder(
                         future: futur_images,
                         builder: (BuildContext context,AsyncSnapshot<List<ImageCar>> snapshot){
                           if(snapshot.hasData && snapshot.data.length > 0){
                             print("-------------- data found ----------");
                             List<ImageCar> images = snapshot.data;
                             return Container(
                               padding: const EdgeInsets.only(top: 100),
                               height: MediaQuery.of(context).size.height*0.35,
                               child: ListView.builder(
                                   scrollDirection: Axis.horizontal,
                                 itemCount: images.length,
                                 itemBuilder: (context,index){
                                  String path_image = 'https://app.taxiride.biz:81/api/commons/download/'+images[index].pictureURL;
                                  print(path_image);
                                   return Container(
                                     margin: EdgeInsets.all(1),
                                     padding: EdgeInsets.all(2),
                                     decoration: BoxDecoration(
                                       border: Border.all(width: 1),
                                       borderRadius: BorderRadius.circular(15),
                                       color: Colors.white70
                                     ),
                                     width: MediaQuery.of(context).size.width*0.60,
                                     height: MediaQuery.of(context).size.height*0.20,
                                    child: Image.network(path_image),
                                   );
                                 }
                               ),
                             );
                           }else{
                             print("-------------- data not found ----------");
                            return Align(
                               alignment: Alignment.topCenter,
                               child: Container(
                                 padding: const EdgeInsets.only(top: 100),
                                 height: MediaQuery.of(context).size.height*0.35,
                                 child: (_images.length == 0)?ListView.builder(
                                   scrollDirection: Axis.horizontal,
                                   itemCount: carName.length,
                                   itemBuilder: (context,index){
                                     String namefile = "";
                                     return Container(
                                       margin: EdgeInsets.all(4),
                                       width: MediaQuery.of(context).size.width*0.60,
                                       height: MediaQuery.of(context).size.height*0.20,
                                       decoration: BoxDecoration(
                                         color: const Color(0xff7c94b6),
                                         image: const DecorationImage(
                                           image: AssetImage("images/no_profile_image.png"),
                                           fit: BoxFit.cover,
                                         ),
                                         border: Border.all(
                                           width: 1,
                                         ),
                                         borderRadius: BorderRadius.circular(15),
                                       ),
                                     );
                                   },

                                 ):ListView.builder(
                                   itemCount: _images.length,
                                   scrollDirection: Axis.horizontal,
                                   itemBuilder: (context,index){
                                     return  Container(
                                       margin: EdgeInsets.all(4),
                                       child: Image.file(_images[index]),
                                     );
                                   },


                                 ),
                               ),
                             );
                           }
                         }
                       )
                     ),
                   ],
                 ),
                 ),
                 (_images.length < 6)?((btn_save)?(active_btn==5)?PrimaryButton(text: 'Save Car Information',onPressed: (){saveCabInformation(active_btn);setState(() {
                   active_btn = 1;
                 });

                 },):SizedBox(height: 2,):PrimaryButton(
                   text: (_images.length==0)?'add image':'Add Again...',
                   onPressed: (){
                    // getImage();
                     findImageDialog(0);
                   },
                 )):PrimaryButton(
                   text: 'Complete',

                 ),

                 Container(
                   height: MediaQuery.of(context).size.height*0.4,
                   child: ListView(
                     children: [
                       ExpansionPanelList(
                           expansionCallback: (index,expanded){
                             setState(() {
                               if(index == 0){
                                 active_detail_car = true;
                                 active_document_car = false;
                                 findCabInformation();
                               }else{
                                 active_document_car = true;
                                 active_detail_car = false;
                                 findFileAttachment();
                               }
                             });


                           },
                         children: [
                           ExpansionPanel(headerBuilder: (BuildContext context,bool isExpanded){
                             return ListTile(title: Text("Car Details",style: TextStyle(fontWeight: FontWeight.bold),),);
                           }, body: Column(
                             children: [
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Vehicule Type",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: (type == null)?Text("No type valide"):Text(type),
                                 leading: Icon(Icons.directions_car,color: Colors.green,),
                                 trailing: (btn_edit)?IconButton(icon: Icon(Icons.edit),onPressed: (){
                                   _showDialog("Insert Vehicule Type","type",1);
                                 },):IconButton(icon: Icon(Icons.check,color: Colors.green,),),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Car Brand",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: (brand == null)?Text("No brand valide"):Text(brand),
                                 leading: Icon(Icons.directions_car,color: Colors.green,),
                                 trailing: (btn_edit)?IconButton(icon: Icon(Icons.edit),onPressed: (){
                                   _showDialog("Insert Car Brand","brand",2);
                                 },):IconButton(icon: Icon(Icons.check,color: Colors.green,),),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Car Model",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: (model == null)?Text("No model valide"):Text(model),
                                 leading: Icon(Icons.directions_car,color: Colors.green,),
                                 trailing: (btn_edit)?IconButton(icon: Icon(Icons.edit),onPressed: (){
                                   _showDialog("Insert Car Model","model",3);
                                 },):IconButton(icon: Icon(Icons.check,color: Colors.green,),),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Car Matricule",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: (matricule == null)?Text("No matricule valide"):Text(matricule),
                                 leading: Icon(Icons.directions_car,color: Colors.green,),
                                 trailing: (btn_edit)?IconButton(icon: Icon(Icons.edit),onPressed: (){
                                   _showDialog("Insert Car Matricule","matricule",4);
                                 },):IconButton(icon: Icon(Icons.check,color: Colors.green,),),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Car Constructor",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle:(constructor == null)?Text("No constructor valide"):Text(constructor),
                                 leading: Icon(Icons.directions_car,color: Colors.green,),
                                 trailing: (btn_edit)?IconButton(icon: Icon(Icons.edit),onPressed: (){
                                   _showDialog("Insert Car Constructor","constructor",5);
                                 },):IconButton(icon: Icon(Icons.check,color: Colors.green,),),
                               ),
                             ],
                           ),isExpanded: active_detail_car),

                           ExpansionPanel(headerBuilder: (BuildContext context,bool isExpanded){
                             return ListTile(title: Text("Car Documents",style: TextStyle(fontWeight: FontWeight.bold),),);
                           }, body: Column(
                             children: [
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Identity piece",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: Text((name1==null)?"No element":name1,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                                 leading: Icon(Icons.picture_as_pdf,color: Colors.green,),
                                 trailing:IconButton(icon: Icon(Icons.add_circle,color: Colors.black,),onPressed: (){
                                   //getPdf(1);
                                   findImageDialog(1);
                                 },),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Driver license",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: Text((name2==null)?"No element":name2,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                                 leading: Icon(Icons.picture_as_pdf,color: Colors.green,),
                                 trailing: IconButton(icon: Icon(Icons.add_circle,color: Colors.black,),onPressed: (){
                                   //getPdf(2);
                                   findImageDialog(2);},),
                               ),

                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("Gray card",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: Text((name3==null)?"No element":name3,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                                 leading: Icon(Icons.picture_as_pdf,color: Colors.green,),
                                 trailing:IconButton(icon: Icon(Icons.add_circle,color: Colors.black,),onPressed: (){
                                   //getPdf(3);
                                   findImageDialog(3);},),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("assurance",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: Text((name4==null)?"No element":name4,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                                 leading: Icon(Icons.picture_as_pdf,color: Colors.green,),
                                 trailing:IconButton(icon: Icon(Icons.add_circle,color: Colors.black,),onPressed: (){
                                   //getPdf(4);
                                   findImageDialog(4);},),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("technical visit",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: Text((name5==null)?"No element":name5,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                                 leading: Icon(Icons.picture_as_pdf,color: Colors.green,),
                                 trailing:IconButton(icon: Icon(Icons.add_circle,color: Colors.black,),onPressed: (){
                                   //getPdf(5);
                                   findImageDialog(5);},),
                               ),
                               Divider(height: 5,color: Colors.black,),
                               ListTile(
                                 title: Text("business card",style: TextStyle(color: Colors.green,fontSize: 15),),
                                 subtitle: Text((name6==null)?"No element":name6,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                                 leading: Icon(Icons.picture_as_pdf,color: Colors.green,),
                                 trailing: IconButton(icon: Icon(Icons.add_circle,color: Colors.black,),onPressed: (){
                                 //  getPdf(6);
                                   findImageDialog(6);},),
                               ),
                             ],
                           ),isExpanded: active_document_car),

                         ],
                       )
                     ],
                   ),
                 ),
               ],
             ),
           ),
         ),
       ),
     );
  }

  Future<Map<String, dynamic>> uploadImage(File image,String token,String phoneNumber,int pictureNumber) async {
   String url = "https://app.taxiride.biz:81/api/profile/saveProfileCabImage";
   // String url = "https://app.taxiride.biz:81/api/profile/saveProfileAttachements";
    final Uri uri = Uri.parse(url);
    final String prefix = "";
     Uri uri2 = Uri.https("app.taxiride.biz:81", prefix +"/api/profile/saveProfileCabImage");
    Map<String, String> headers = {
      'Authorization':'Bearer ' + token};
    print('Bearer ' + token);
    final http.MultipartRequest request = http.MultipartRequest("POST", uri);
    request.headers['Authorization'] ='Bearer ' + token ;
    final file = await http.MultipartFile.fromPath('file',image.path);
    request.files.add(file);
    request.fields['language'] = 'en';
    request.fields['phoneNumber'] = phoneNumber;
    request.fields['pictureNumber'] = pictureNumber.toString();
   // request.fields['number'] = pictureNumber.toString();

    final http.StreamedResponse response = await request.send();
    final rep = await http.Response.fromStream(response);
    print('statusCode => ${response.statusCode}');
    print(rep.body);

    if(response.statusCode != 200 ){
      Toast.show("error Save Image", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);

      return null;
    }else{
      Toast.show("Save Succefull", context,duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }
    final Map<String,dynamic> responseData = json.decode(rep.body);

    return responseData;

  }

  void _showDialog(String title,String champ,int position) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Car Information"),
          content: new Text(title),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child: TextField(
                    controller: myController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: champ,
                    ),
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      PrimaryButton(text: 'Save',onPressed: (){

                        setState((){
                          if(position == 1){
                            type = myController.text;
                            car.vehicleType = myController.text;
                          }else if(position == 2 ){
                            brand = myController.text;
                            car.brand = myController.text;
                          }else if(position == 3 ){
                            model = myController.text;
                            car.model = myController.text;
                          }else if(position == 4){
                            matricule = myController.text;
                            car.matriculationNumber = myController.text;

                          }else if(position == 5 ){
                            constructor = myController.text;
                            car.carconstructor = myController.text;
                            active_btn = position;

                          }
                          Navigator.of(context).pop();
                          myController.text="";

                        });

                       // saveCabInformation(position);
                      },),
                      SecondaryButton(text: 'Close',onPressed: (){Navigator.of(context).pop();},)
                    ],
                  ),
                )
              ],
            ),

          ],
        );
      },
    );
  }

  getFile(String url) async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    FindFileByUrlRequest file = FindFileByUrlRequest(url,token);
    file.parseResult(await ApiClient.execOrFail(file));
  }

  findFile(String path) async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    String url = "https://app.taxiride.biz:81/api/commons/download/$path";

    Response response = await get(url, headers: <String, String>{
      'Authorization':'Bearer ' + token,
    'Content-Type':'application/json; charset=UTF-8',
    });
    Map<String,dynamic> res = jsonDecode(response.body);

  }

  findFileAttachment() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    String phone = prefs.getString('username');
    FindAttachmentByPhone findAttachmentByPhone = FindAttachmentByPhone(token,"en",phone);
    List<FileAttachment> docs = findAttachmentByPhone.parseResult(await ApiClient.execOrFail(findAttachmentByPhone));
    if(docs.length>0){
      int pos = 0;
      docs.forEach((element) {
        String namefile = element.url.split("-").last;
        setState(() {
          pos ++;
          btn_doc = true;
          if(pos == 1){
            name1 = namefile;
          }else if(pos == 2){
            name2 = namefile;
          }else if(pos == 3 ){
            name3 = namefile;
          }else if(pos == 4){
            name4 = namefile;
          }else if(pos == 5){
            name5 = namefile;
          }else if(pos == 6){
            name6 = namefile;
          }
        });
      });
    }
  }

  findCabInformation() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    String phone = prefs.getString('username');
    FindCabInformationByPhoneRequest findCabInformationByPhoneRequest = FindCabInformationByPhoneRequest(token,"en",phone);
    _vehicule = findCabInformationByPhoneRequest.parseResult(await ApiClient.execOrFail(findCabInformationByPhoneRequest));
    print("---------------- information vehicule -----------------");
    print(_vehicule.phoneNumber);
    if(_vehicule != null){
      setState(() {
        type = _vehicule.vehicleType;
        brand = _vehicule.brand;
        model = _vehicule.model;
        matricule = _vehicule.matriculationNumber;
        constructor = _vehicule.carconstructor;
        btn_edit = false;
      });
    }
  }

  void findImageDialog(int pos){
    showDialog(context: context,builder:(BuildContext context){
      return AlertDialog(
        title: new Text("Select Image With"),
        actions: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width*0.9,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: (){
                        setState(() {
                          if(pos==0){
                            getImageCamera();
                          }else{
                            getPdfWithCamera(pos);

                          }

                          Navigator.of(context).pop();
                        });
                      },
                      child: Container(
                          padding: EdgeInsets.all(20),
                          child: Text("CAMERA",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),)),
                    ),
                    InkWell(
                      onTap: (){
                        setState(() {
                          if(pos == 0){
                            getImage();
                          }else{
                            getPdf(pos);
                          }

                          Navigator.of(context).pop();
                        });
                      },
                      child: Container(
                          padding: EdgeInsets.all(20),
                          child: Text("GALERY",style: TextStyle(color: Colors.blue,fontWeight: FontWeight.bold),)),
                    )
                  ],
                ),
              ),
              Divider(height: 8,color: Colors.black,),
              Container(
                padding: EdgeInsets.all(15),
                child: SecondaryButton(text: 'Cancel',onPressed: (){Navigator.of(context).pop();},),

              )

            ],
          )
        ],
      );
    });
  }

  saveCabInformation(int position) async {

    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('access_token');
    String phone = prefs.getString('username');
    if(position == 5 ){
      print("---------------- debut de sauvegarde --------------------");
      car.phoneNumber = phone;
      SaveCabProfilInformation saveCabProfilInformation = SaveCabProfilInformation(car,token);
      saveCabProfilInformation.parseResult(await ApiClient.execOrFail(saveCabProfilInformation));
      print("---------------- fin de sauvegarde --------------------");


    }else{

      print(" ________________ ne peut executer cette requete _____________________");
    }


  }

}