import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/vehicule.dart';

class FindCabInformationByPhoneRequest extends AbstractAuthenticatedRequest{
  final String token;
  final String language;
  final String phone;

  FindCabInformationByPhoneRequest(this.token,this.language,this.phone) : super(token);

  @override
  String getUrl() {
    return "/api/profile/findCabInformationByPhone/$phone/$language";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  Map<String, String> getParams() {

    return {
      "language":language,
      "phone":phone
    };

  }

  @override
  parseResult(AbstractApiResponse response) {
    if(response.httpResponse.statusCode == 200){
      dynamic data = response.httpResponse.body;
      Vehicule vehicule = Vehicule.fromJson(jsonDecode(data));
      print(response.httpResponse.statusCode);
      return vehicule;

    }

   return null;
  }

}