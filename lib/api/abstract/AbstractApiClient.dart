
import 'package:taxiride_ios/api/abstract/AbstractApiRequest.dart';
import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';

abstract class AbstractApiClient{
  Future<AbstractApiResponse>process(AbstractApiRequest request);

}