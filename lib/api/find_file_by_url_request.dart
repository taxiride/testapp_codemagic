import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';

class FindFileByUrlRequest extends AbstractAuthenticatedRequest{
  final String url;
  final String token;

  FindFileByUrlRequest(this.url, this.token):super(token);

  @override
  String getUrl() {
   return "/api/commons/download/$url";

  }

  @override
  String getVerb() {
    return "GET";
  }

  @override
  Map<String, String> getParams() {
    return {
      "url":url
    };

  }

  @override
  parseResult(AbstractApiResponse response) {
    //dynamic data = response.httpResponse.body;
    //Map<String,dynamic> rep = jsonDecode(data);
    //print(rep);
    print(response.httpResponse.statusCode);
   // return rep;

  }

}