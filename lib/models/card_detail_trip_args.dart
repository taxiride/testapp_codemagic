import 'package:taxiride_ios/models/profile.dart';

class CardDetailTripArgs{
  final String depart;
  final String arriv;
  final Profil driver;
  final bool isValided;

  CardDetailTripArgs({this.depart,this.arriv,this.driver,this.isValided});
}