import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/ride_request/cancel_trip_request.dart';
import 'package:taxiride_ios/api/ride_request/check_current_ride_request.dart';
import 'package:taxiride_ios/api/ride_request/find_trip_infos_request.dart';
import 'package:taxiride_ios/api/ride_request/post_rides_request.dart';
import 'package:taxiride_ios/models/ride.dart';
import 'package:taxiride_ios/models/trip_responce.dart';
import 'package:taxiride_ios/models/ville.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:http/http.dart' as http;

class RidesProvider with ChangeNotifier{

  int tripId;
  TripResponce tripResponce;
  String depart;
  String arriver;
  Ville  ville_courante;

  //un rider publie une demande de course dans la plateform
  Future<void> postTrip(AuthProvider authProvider,Ride ride) async {
    final prefs = await SharedPreferences.getInstance();
    final http.Response response = await http.post(
      'https://app.taxiride.biz:81/api/rides/postTrip',
      headers: <String,String>{
        'Authorization': 'Bearer '+authProvider.token,
        'Content-Type':'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String,dynamic>{
        "arrivalPoint": ride.arrivalPoint,
        "availableCashAmount": ride.availableCashAmount,
        "codeOption": ride.codeOption,
        "codePays": ride.codePays,
        "codeVille": ride.codeVille,
        "departurePoint": ride.departurePoint,
        "endPointLatitude": ride.endPointLatitude,
        "endPointLongitude": ride.endPointLongitude,
        "estimatedDuration": ride.estimatedDuration,
        "estimatedTripDistance": ride.estimatedTripDistance,
        "estimatedTripLength": ride.estimatedTripLength,
        "id": ride.id,
        "language": ride.language,
        "phoneNumber": ride.phoneNumber,
        "postDate": ride.postDate,
        "rayon": ride.rayon,
        "riderLattitude": ride.riderLattitude,
        "riderLongitude": ride.riderLongitude,
        "seatNumber": ride.seatNumber,
        "startPointLatitude": ride.startPointLatitude,
        "startPointLongitude": ride.startPointLongitude,
        "tripCost": ride.tripCost,
        "tripTimeOut": ride.tripTimeOut

      }),
    );

    if(response.statusCode == 200){
      print(response.body);
      print("--------------------- save succeffull ----------------------");
      Map<String,dynamic> json = jsonDecode(response.body);
      int id_trip = json['tripId'];
      prefs.setInt('id_trip',id_trip);
      tripId = id_trip;
      print(tripId);
      findIdTrip();
      notifyListeners();


    }else{

      print(response.body);
      print(response.statusCode);
      print(ride.language);

    }

  }

  // retourne identifiant d un trip d un user si c est encore en cour
  Future<int> findIdTrip() async {
    final prefs = await SharedPreferences.getInstance();
    if(prefs.getKeys().contains('id_trip')){
        tripId = prefs.getInt('id_trip');
        print(tripId);
        print("--------------------");
        notifyListeners();
    }else{
      // ici il n a plus de trip en cours
      tripId = 0;
      print(tripId);
      print("--------------------");
      notifyListeners();
    }
    return tripId;

  }
  // supprimer l'id du trip en cour (ex:trip annuler ,trip valider,trip terminer)
  forgetIdTrip() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('id_trip');
    prefs.remove('depart');
    prefs.remove('arriver');
    tripId = 0;
    notifyListeners();
  }

  setVilleRider(Ville ville){
    ville_courante = ville;
    notifyListeners();
  }

  findTripInfosById(String token,int id) async {
    final prefs = await SharedPreferences.getInstance();
    FindTripInfosRequest findTripInfosRequest = FindTripInfosRequest(token,id);
    TripResponce tripResponse = findTripInfosRequest.parseResult(await ApiClient.execOrFail(findTripInfosRequest));

    if(tripResponse != null){
      tripResponce = tripResponse;
      prefs.setString('depart',tripResponse.departureAddress);
      prefs.setString('arriver',tripResponse.arrivalAddress);
      notifyListeners();
    }else{
      forgetIdTrip();
    }

    return tripResponce;
  }

  cancelTripById(AuthProvider authProvider,int idTrip) async {
    String token = await authProvider.tokenKey;
    String phone = await authProvider.username;
    print(token);
    print(phone);
    CancelTripRequest cancelTripRequest = CancelTripRequest(token,idTrip,phone);
    cancelTripRequest.parseResult( await ApiClient.execOrFail(cancelTripRequest));
    forgetIdTrip();
  }

  checkCurrentRide(AuthProvider authProvider) async {
    final prefs = await SharedPreferences.getInstance();
    CheckCurrentRideRequest checkCurrentRideRequest = CheckCurrentRideRequest(authProvider);
    Map<String,dynamic> data = checkCurrentRideRequest.parseResult( await ApiClient.execOrFail(checkCurrentRideRequest));
    if(data != null){
      prefs.setBool("driver_check", data['driver']);
      prefs.setString('driver_email',data['driverEmail']);
    }
  }


}