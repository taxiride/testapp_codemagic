import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';

class FindAllAvailableDriverRequest extends AbstractAuthenticatedRequest{
  final String token;
  final String codepays;
  final double latitude;
  final double longitude;
  final int rayon;
  final int tripId;
  final String ville;

  FindAllAvailableDriverRequest(this.token, this.codepays, this.latitude, this.longitude, this.rayon, this.tripId, this.ville):super(token);

  @override
  String getUrl() {
    return "/api/rides/findAllAvailableDrivers/$tripId/$codepays/$ville/$longitude/$latitude/$rayon/fr";
  }

  @override
  String getVerb() {
    return "GET";
  }
  @override
  Map<String, String> getParams() {
    return {
      "codePays":codepays,
      "language":"fr",
      "latitude":latitude.toString(),
      "longitude":longitude.toString(),
      "rayon":rayon.toString(),
      "tripId":tripId.toString(),
      "ville":ville
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    print(data);
    print(response.httpResponse.statusCode);
  }






}