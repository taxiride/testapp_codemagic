import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'base_button.dart';

class BlackButton extends BaseButton{

  final Function onPressed;
  final Widget child;
  final String text;
  final EdgeInsets padding;

  BlackButton({this.padding, @required this.onPressed, this.child, this.text})
      : super(
      padding: padding,
      onPressed: onPressed,
      color: Colors.black,
      text: text,
      child: child);

}