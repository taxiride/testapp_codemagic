class Ride{
   String arrivalPoint;
   String availableCashAmount;
   String codeOption;
   String codePays;
   String codeVille;
   String departurePoint;
   double endPointLatitude;
   double endPointLongitude;
   int estimatedDuration;
   int estimatedTripDistance;
   int estimatedTripLength;
   int id;
   String language;
   String phoneNumber;
   String postDate;
   int rayon;
   int riderLattitude;
   int riderLongitude;
   int seatNumber;
   int startPointLatitude;
   int startPointLongitude;
   int tripCost;
   int tripTimeOut;

   Ride({
     this.availableCashAmount,
     this.arrivalPoint,
     this.codeOption,
     this.codePays,
     this.codeVille,
     this.departurePoint,
     this.endPointLatitude,
     this.endPointLongitude,
     this.estimatedDuration,
     this.estimatedTripDistance,
     this.estimatedTripLength,
     this.id,
     this.language,
     this.phoneNumber,
     this.postDate,
     this.rayon,
     this.riderLattitude,
     this.riderLongitude,
     this.seatNumber,
     this.startPointLatitude,
     this.startPointLongitude,
     this.tripCost,
     this.tripTimeOut
   });

   Ride.fromJson(Map<String, dynamic> json){
     arrivalPoint = json['arrivalPoint'];
     availableCashAmount = json['availableCashAmount'];
     codeOption = json['codeOption'];
     codePays = json['codePays'];
     codeVille = json['codeVille'];
     departurePoint = json['departurePoint'];
     endPointLatitude = json['endPointLatitude'];
     endPointLongitude = json['endPointLongitude'];
     estimatedDuration = json[estimatedDuration];
     estimatedTripLength = json['estimatedTripLength'];
     id= json['id'];
     language = json['language'];
     phoneNumber = json['phoneNumber'];
     postDate = json['postDate'];
     rayon = json['rayon'];
     riderLattitude = json['riderLattitude'];
     riderLongitude = json['riderLongitude'];
     seatNumber = json['seatNumber'];
     startPointLatitude = json['startPointLatitude'];
     tripCost = json['tripCost'];
     tripTimeOut = json['tripTimeOut'];

   }

   static List<Ride> listFromJson(List<dynamic> json) {
     return List<Ride>.from(json.map((v) => Ride.fromJson(v)));
   }

}