import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';

class CustomImagePicker extends StatefulWidget {
  final Function(List<File> image) onChanged;
  final int maxImageCount;


  const CustomImagePicker({Key key, this.onChanged, this.maxImageCount = 0})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return CustomImagePickerState();
  }
}

class CustomImagePickerState extends State<CustomImagePicker> {
  List<File> _images = [];
  //List<PickedFile> _images = [];
  final picker = ImagePicker();

  Future getImage() async {
    if (_images.length >= widget.maxImageCount) {
      print("------ nombres images superieur ----------");
      return;
    }
   // var image = await picker.getImage(source: ImageSource.gallery);
   //  var elt = picker.getImage(source: ImageSource.gallery).then((value){});
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      setState(() {
       _images.add(image);
      });
      widget.onChanged(_images);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        PrimaryButton(
         // expandText: true,
          child: Row(
            children: <Widget>[
              Flexible(
                fit: FlexFit.loose,
                child: Text(
                  "Add job images",
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          onPressed: () {
            getImage();
          },
        ),
        SizedBox(height: 8),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                  "max: ${widget.maxImageCount.toString()}, ${widget.maxImageCount - _images.length} left",
                  style: TextStyle(color: Colors.black))),
        ),
        SizedBox(height: 16),
        SizedBox(
          child: _images.isNotEmpty
              ? CarouselSlider(
              //enableInfiniteScroll: false,
             // viewportFraction: 0.8,
              items: List<Widget>.from(_images
                  .map(
                    (img) => CustomImagePickerCarrouselItem(
                  onPressed: () {
                    print('pressed');
                    setState(() {
                      _images.removeWhere((v) => v.path == img.path);
                    });
                    widget.onChanged(_images);
                  },
                  image: img,
                ),
              )
                  .toList()))
              : SizedBox.shrink(),
        )
      ],
    );
  }
}

class CustomImagePickerCarrouselItem extends StatelessWidget {
  final File image;
  final Function onPressed;
  const CustomImagePickerCarrouselItem({Key key, this.image, this.onPressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Stack(
        children: <Widget>[
          Image.file(image),
          Align(
              alignment: Alignment.topRight,
              child: IconButton(
                icon: Container(
                    decoration: BoxDecoration(
                        color: Colors.white, shape: BoxShape.circle),
                    child: Icon(Icons.remove_circle)),
                color: Colors.red,
                onPressed: () {
                  this.onPressed();
                },
              )),
        ],
      ),
    );
  }
}
