import 'dart:convert';

class TaximenCar{
  int travelId;
  int driverId;
  String postDate;
  String departureDate;
  String departure;
  double seatCost;
  int availableSeatCount;
  int reservationCount;
  String driverConditions;
  String driverName;
  double distance;
  String arrivalAddress;
  String driverImageUrl;
  String driverPhoneNumber;

  TaximenCar({this.travelId,
    this.driverId,
    this.postDate,
    this.departure,
    this.departureDate
  ,this.seatCost,
    this.availableSeatCount,
    this.reservationCount,
    this.driverConditions,
    this.driverName,
  this.distance,
    this.arrivalAddress,
    this.driverImageUrl,
    this.driverPhoneNumber});

  TaximenCar.fromJson(Map<String,dynamic> json){
    driverId = json['driverId'];
    postDate = json['postDate'];
    departure = json['departure'];
    departureDate = json['departureDate'];
    seatCost = json['seatCost'];
    availableSeatCount = json['availableSeatCount'];
    reservationCount = json['reservationCount'];
    driverConditions = json['driverConditions'];
    driverName = json['driverName'];
    distance = json['distance'];
    arrivalAddress = json['arrivalAddress'];
    driverImageUrl = json['driverImageUrl'];
    driverPhoneNumber = json['driverPhoneNumber'];
  }

  static List<TaximenCar> listFromJson(List<dynamic> json) {
    return List<TaximenCar>.from(json.map((v) => TaximenCar.fromJson(v)));
  }

  static List<String> listFromString(List<dynamic> json) {
    return List<String>.from(json.map((v) => TaximenCar.fromJson(v)));
  }


}