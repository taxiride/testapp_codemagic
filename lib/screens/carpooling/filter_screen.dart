import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/widget/layout/filter_box.dart';

class FilterScreen extends StatefulWidget{
   final String titre;

  const FilterScreen({Key key, this.titre}) : super(key: key);

  _FilterScreenState createState() => _FilterScreenState();

}

class _FilterScreenState extends State<FilterScreen>{

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.titre,style: TextStyle(color: Colors.black),),
        leading: IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.black,), onPressed: (){
          Navigator.pop(context);
        }),
        backgroundColor: Colors.white,

      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FilterBox(
                label: "FILTER STATUS",
                onTap: () {

                }),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: (){},
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text("ACTIVE TRIPS",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    ),
                  ),
                  InkWell(
                    onTap: (){},
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text("REQUESTED TO JOIN",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    ),
                  ),
                  InkWell(
                    onTap: (){},
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text("INVITED TO JOIN",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    ),
                  ),
                  InkWell(
                    onTap: (){},
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Text("CANCELED",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                    ),
                  )
                ],
              ),
            ),
          )

        ],
      ),
    );
  }

}