import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';

class AcceptTripRequest extends AbstractAuthenticatedRequest{
  final AuthProvider authProvider;
  final int distanceBeforePickup;
  final int durationBeforePickUp;
  final double latitude;
  final double longitude;
  final int requestid;
  final int tripID;

  AcceptTripRequest(this.authProvider, this.distanceBeforePickup, this.durationBeforePickUp, this.latitude, this.longitude, this.requestid, this.tripID):super(authProvider.token);

  @override
  String getUrl() {
   return "/api/rides/acceptTrip";
  }

  @override
  String getVerb() {
   return "POST";
  }

  @override
  getBody() {
    return {
      "distanceBeforePickup": distanceBeforePickup,
      "durationBeforePickUp": durationBeforePickUp,
      "emailorphone": authProvider.phone,
      "latitude": latitude,
      "longitude": longitude,
      "requestid": requestid,
      "tripID": tripID
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print("------- succefful ------------");
      print(data);
      print(response.httpResponse.statusCode);
    }else{
      print(data);
      print(response.httpResponse.statusCode);
      print("---------- error error error -------------");

    }
  }



}