import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart' ;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart'  as geoloc;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/carpooling/find_all_pending_travel_or_booking_request.dart';
import 'package:taxiride_ios/api/find_profil_by_phoneNumber_request.dart';
import 'package:taxiride_ios/models/apiresponse/o_s_m_places.dart';
import 'package:taxiride_ios/models/apiresponse/profileresp.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';
import 'package:taxiride_ios/models/profile.dart';
import 'package:taxiride_ios/ridescreens/auth/LandingAuth.dart';
import 'package:taxiride_ios/ridescreens/profil/profile.dart';
import 'package:http/http.dart' as http;
import 'package:taxiride_ios/screens/carpooling/car_pooling_screen.dart';
import 'package:taxiride_ios/screens/carpooling/pooling_driver_waiting_screen.dart';
import 'package:taxiride_ios/screens/profile_infos_screen.dart';
import 'package:taxiride_ios/widget/button/base_button.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:toast/toast.dart';
import 'auth/login.dart';
import 'find_rides.dart';

class NavigationDriverPage extends StatefulWidget{

  final bool isRideAccepted;
  NavigationDriverPage({this.isRideAccepted = false});

  @override
  _NavigationDriverState createState()=>_NavigationDriverState();

}


Future<Profile> findByPhoneNumber() async {

  final prefs =await SharedPreferences.getInstance();

  var uri =Uri.https('app.taxiride.biz:81', '/api/profile/findbyphonenumber/'+prefs.get('username')+'/fr');
  final http.Response response = await http.get(
    uri,
    headers: <String, String>{
      HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
    },

  );

  log('code'+response.statusCode.toString());

  if(response.statusCode == 200){
    Profile user = Profile.fromJson(json.decode(response.body));

    return user;
  }

}

TextFormField _buildRoundTextField(String hint, t, TextEditingController controller, bool obs){
  return TextFormField(
    controller: controller ,
    decoration: new InputDecoration(
        border: InputBorder.none,
        filled: true,
        hintText: hint,
        fillColor: Colors.white
    ),
    keyboardType: t,
    obscureText: obs,
    cursorColor: Colors.black,
  );
}


RoundedRectangleBorder cardShape(){

  return  RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(15),
        bottomRight: Radius.circular(15),
        topLeft: Radius.circular(15),
        topRight: Radius.circular(15.0)
    ),

  );

}

Profile currentUserProfile;

class _NavigationDriverState extends State<NavigationDriverPage>{
  bool isRideAccepted;
  _NavigationDriverState({this.isRideAccepted = false});

  List<OSMPlaces> places;
  Future<Profile> userProfile;
  GoogleMapController mapController;

  LatLng _center = new LatLng(3.8731589, 11.4626538);
  final GlobalKey<ScaffoldState> _navKey = new GlobalKey<ScaffoldState>();

  geoloc.Location location = new geoloc.Location();

  bool _serviceEnabled;
  geoloc.PermissionStatus _permissionGranted;

  OSMPlaces selectedPlaces;
  bool  isFromPlacesFound = false, isOSMPlacesSelected = false, showRideDetails = false, isRideStarted = false;

  Locale myLocale ;

  Position position;
  var geolocator = Geolocator();

  TextEditingController radiusController = new TextEditingController();

  //active les service de localisation et demande la permission pour accéder à la position courrante de l'utilisateur

  void _enableLocationService() async{

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == geoloc.PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != geoloc.PermissionStatus.granted) {
        //appel de la fonction pour récupérer la position
        //afichage de la map
        _getCurrentUserPosition();
        return;
      }
    }

  }

  void _getCurrentUserPosition() async{
    position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    print(position.latitude.toString() + ', ' + position.longitude.toString());
  }

  void _onMapCreated(GoogleMapController controller){
    mapController = controller;
   /// _center = LatLng(position.latitude, position.longitude);
  }

  //écoute le changement de position
  void _startListenningPosition(){
    var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);


  }

  @override
  void initState() {
    super.initState();
    //_getCurrentUserPosition();
    _enableLocationService();
    userProfile = findByPhoneNumber();

    _startListenningPosition();

  }

  void _findRide(String rad){
    Navigator.push(
        context,
        // MaterialPageRoute(builder: (context)=>UserProfile('','')
        MaterialPageRoute(builder: (context)=>FindRidesPage(radius: rad,)

        )
    );
  }


  @override
  Widget build(BuildContext context) {

    final String searchHint = 'Search Radius (km)';
    myLocale = Localizations.localeOf(context);

    final ProgressDialog pr = ProgressDialog(context);
    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

    //construit la carte qui s'affiche lorsque le rider est connecté comme rider
    Widget getStartedDriver =  Card(
      shape: cardShape(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Container(
            margin: const EdgeInsets.only(left: 16,top: 8),
            child:  Row(
              mainAxisSize: MainAxisSize.min,

              children: <Widget>[
                Image.asset(
                    'images/shape_get_started.png'
                ),

                Center(child: Text(searchHint , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins')),),

              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(left: 16, right: 16.0),
            child: Row(
              children: <Widget>[

                Expanded(
                  child: _buildRoundTextField('Enter Search Radius', TextInputType.text, radiusController, false),
                ),
                IconButton(
                  icon: Icon(Icons.close),
                  color: Colors.black26,
                  onPressed: (){

                  },
                ),
              ],
            ),
          ),


          Container(
            margin: const EdgeInsets.only(left: 16, right: 16.0),
            child: Divider(height: 1, color: Colors.grey,),
          ),

          Center(
            child: Container(
              margin: const EdgeInsets.only(top: 16.0),
              child: BaseButton(
                onPressed: (){
                  if(radiusController.text != null) _findRide(radiusController.text);
                  else Toast.show("Enter a radius please", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                },
                color: Colors.green,
                textColor: Colors.white,
                text: 'FIND RIDES',
              ),
            ),
          )

        ],
      ),

    );

    void navigateToProfile(Profil profil){
      Navigator.push(
          context,
          // MaterialPageRoute(builder: (context)=>UserProfile('','')
          MaterialPageRoute(builder: (context)=>ProfileInfosScreen(profile: profil,)

          )
      );
    }

    void logout() async {
      final prefs = await SharedPreferences.getInstance();

      await prefs.setBool('is_connected', false);
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AuthChoicePage()));
    }

    return MaterialApp(
      home:   Scaffold(
        key: _navKey,
        body:Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                  target: _center,
                  zoom: 18
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: const EdgeInsets.only(top: 16),
                  child: FloatingActionButton(
                    onPressed: ()=>_navKey.currentState.openDrawer(),
                    materialTapTargetSize: MaterialTapTargetSize.padded,
                    backgroundColor: Colors.amber,
                    child: const Icon(Icons.dehaze, size: 36.0, color: Colors.black,),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: !isRideAccepted,
              child:Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 180,
                  margin: const EdgeInsets.all(16.0),
                  child: getStartedDriver,
                ),
              ),
            ),

            Visibility(
              visible: isRideAccepted,
              child:   Align(
                alignment: Alignment.bottomCenter,
                child:  Container(
                  height: 75,
                  child:   Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Visibility(
                        visible: !isRideStarted && isRideAccepted,
                        child:   BaseButton(
                          onPressed: (){
                            setState(() {
                              isRideStarted = true;
                            });
                          },
                          textColor: Colors.white,
                          color: Colors.green,
                          text: 'Start trip',
                        ),
                      ),

                      BaseButton(
                        onPressed: (){
                          isRideStarted = false;
                          isRideAccepted = false;
                        },
                        textColor: Colors.white,
                        color: Colors.red,
                        text: 'Cancel trip',
                      ),

                      Visibility(
                        visible: isRideStarted && isRideAccepted,
                        child:   BaseButton(
                          onPressed: (){
                            setState(() {
                              isRideStarted = false;
                              isRideAccepted = false;
                            });
                          },
                          textColor: Colors.white,
                          color: Colors.black,
                          text: 'Report abuse',
                        ),
                      ),

                    ],

                  ),
                ),

              ),

            ),


          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              FutureBuilder<Profile>(
                future: userProfile,
                builder: (context, snapshot){
                  if(snapshot.hasData){
                    currentUserProfile = snapshot.data;
                    return  DrawerHeader(
                      child: Container(
                        margin: const EdgeInsets.only(top: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            if( snapshot.data.imageUrl != null)
                              Container(
                                width: 100,
                                height: 100,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      image: NetworkImage('https://app.taxiride.biz:81/api/commons/download/'+snapshot.data.imageUrl),
                                      fit: BoxFit.fill,
                                    )
                                ),
                              )
                            else Container(
                                width: 75,
                                height: 75,
                                decoration: BoxDecoration(
                                    color: Colors.yellow,
                                    shape: BoxShape.circle
                                ),
                                child: Icon(  Icons.person)),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text('Hi,', style: TextStyle(fontSize: 12, fontFamily: 'Poppins'),),
                              if(snapshot.data.firstName != null)  Text(snapshot.data.firstName, style: TextStyle(fontSize: 12, fontFamily: 'Poppins'),)
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }
                  return CircularProgressIndicator();
                },
              ),

              ListTile(
                title:  Text('Profile', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: () async {

                  final prefs =await SharedPreferences.getInstance();
                  String token = prefs.getString('access_token');
                  String username = prefs.getString('username');
                  FindProfileByPhoneNumber profile = FindProfileByPhoneNumber(token,username);
                  profile.findProfilByPhone().then((value){
                    print(value.firstName);
                    navigateToProfile(value);

                  });

                },
              ),
              ListTile(
                title:  Text('Payments', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Stats', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Promo code', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Settings', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Logout', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  logout();
                  Navigator.pop(context);
                },
              ),
              Container(

                padding: EdgeInsets.all(15),
                child: BlackButton(
                  text: "Carpooling",
                  onPressed: () async {

                    pr.show();
                    final prefs = await SharedPreferences.getInstance();
                    int role = await prefs.getInt('role');
                    String mytoken = await  prefs.getString('access_token');
                    String phone = await prefs.getString("username");
                    FindAllPendingTravelOrBookingRequest findallPendingBooking = FindAllPendingTravelOrBookingRequest(mytoken,phone,"fr");
                    List<PoolingTrip> poolinTrips = await findallPendingBooking.parseResult(await ApiClient.execOrFail(findallPendingBooking));

                    if(poolinTrips.length > 0){
                      print("tout c est bien passe========");
                      pr.hide();
                     Navigator.push(context, MaterialPageRoute(builder: (context)=> PoolingDriverWaitingScreen(listPoolings: poolinTrips,role: "driver",)));
                    }else{
                      pr.hide();
                      print("pas d element trouve");
                     Navigator.push(context, MaterialPageRoute(builder: (context)=>CarPoolingScreen(role: (role==0)?"rider":"driver",)));
                    }



                   // Navigator.push(context, MaterialPageRoute(builder: (context) => CarPoolingScreen(role: (role==0)?"rider":"driver",)));

                  },
                )

              )
            ],
          ),
        ),
      ),
    );

  }

}