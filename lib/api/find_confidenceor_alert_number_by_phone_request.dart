import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/emergency_contact.dart';

class FindConfidenceOrAlertNumberRequest extends AbstractAuthenticatedRequest{
  final String token;
  final String language;
  final String phone;
  final String type;

  FindConfidenceOrAlertNumberRequest(this.token, this.language, this.phone, this.type):super(token);

  @override
  String getUrl() {
    return "/api/profile/findConfidenceOrAlertNumberByPhone/$phone/$type/$language";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    print(data);
    if(response.httpResponse.statusCode==200){
      print(response.httpResponse.statusCode);
      print(data);
      return EmergencyContact.listFromJson(jsonDecode(data));
      //return EmergencyContact.listFromJson(jsonDecode(data));
    }else{
      print(response.httpResponse.statusCode);
      print(data);
      return [];
    }

  }


}