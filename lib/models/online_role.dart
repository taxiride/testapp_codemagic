class onlineRole{

  int _role;
  bool _isOnLine;

  onlineRole({
    int role,
    bool isOnLine
}){
    _isOnLine = isOnLine;
    _role = role;
  }

  int get role => _role;
  bool get isOnLine => _isOnLine;

  setRole(int role) => _role = role;
  setIsOnLine(bool isOn) => _isOnLine = isOn;



}