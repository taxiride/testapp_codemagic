import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';

class FindArrivalCitiesRequest extends AbstractAuthenticatedRequest{
  final String countryCode;
  final String departureCity;
  final String language;
  final String token;

  FindArrivalCitiesRequest(this.countryCode, this.departureCity, this.language, this.token):super(token);

  @override
  String getUrl() {
    return "/api/carpooling/findArrivalCities/$departureCity/$countryCode/$language";
  }

  @override
  String getVerb() {
    return "GET";
  }


  @override
  Map<String, String> getParams() {
    return {
      "countryCode":countryCode,
      "departureCity":departureCity,
      "language":language
    };
  }

  @override
  List<PoolingTrip> parseResult(AbstractApiResponse response) {

    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print("----- data trouver -----");
      print(jsonDecode(data)['object']);
      return PoolingTrip.listFromJson(jsonDecode(data)['object']);
    }else{
      print("------- error  -----------");
      return [];
    }
  }
}