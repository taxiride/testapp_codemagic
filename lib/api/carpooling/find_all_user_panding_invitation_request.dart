import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';

class FindAllUserPandingInvitationRequest extends AbstractAuthenticatedRequest{
  final String token;
  final String emailOrPhone;
  final String language;
  final int travelIdOrBookingId;

  FindAllUserPandingInvitationRequest(this.token, this.emailOrPhone, this.language, this.travelIdOrBookingId):super(token);

  @override
  String getUrl() {
    return "/api/carpooling/findAllUserPendingInvitation/$emailOrPhone/$travelIdOrBookingId/$language";
  }

  @override
  String getVerb() {
    return "GET";
  }

  @override
  Map<String, String> getParams() {
    return {
      "emailOrPhone":emailOrPhone,
      "language":language,
      "travelIdOrBookingId":travelIdOrBookingId.toString()
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print("---------- tout c'est bien passe ----------------");
      List<dynamic> responce = jsonDecode(data)["object"];
      print(jsonDecode(data));
      print(responce);
      return responce;
    }else{
      print("------------- la requete a echouer Monsieur ------------");
      return [];
    }
  }


}