import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/models/apiresponse/profileresp.dart';
import 'package:taxiride_ios/models/profile.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

class FindProfileByPhoneNumber{
  final String token;
  final String phoneNumber;

  FindProfileByPhoneNumber(this.token, this.phoneNumber);
  String url = "https://app.taxiride.biz:81/api/profile/findbyphonenumber/";

  Future<Profil> findProfilByPhone() async {
    print(token);
    Response response = await get(url+phoneNumber+"/fr", headers: <String, String>{
      'Authorization': 'Bearer '+token,
      "Accept":"application/json",
      "Content-Type":"application/x-www-form-urlencoded"
    },
      );
    print(response.body);
    Profil profile = Profil.fromJson(jsonDecode(response.body));
    return profile;

  }

  Future<Profile> findByPhoneNumber() async {
    final prefs =await SharedPreferences.getInstance();

    var uri =Uri.https('app.taxiride.biz:81', '/api/profile/findbyphonenumber/'+prefs.get('username')+'/fr');
    final http.Response response = await http.get(
      uri,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
      },

    );

   // log('code'+response.statusCode.toString());

    if(response.statusCode == 200){
      Profile user = Profile.fromJson(json.decode(response.body));

      return user;
    }

  }


}