import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/models/apiresponse/available_trip.dart';
import 'package:taxiride_ios/ridescreens/navigation_driver.dart';
import 'package:taxiride_ios/widget/button/base_button.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:toast/toast.dart';

class FindRidesPage extends StatefulWidget{
  final String radius;
  FindRidesPage({this.radius});

  @override
  _FindRidesState createState()=> _FindRidesState(radius: this.radius);

}



class _FindRidesState extends State<FindRidesPage>{

  final String radius;
  _FindRidesState({this.radius});
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _city, _country;

  bool noRideFound = false, isRideApiCalled = false;

  List<Available_trip> availableTrips = new List();

  void goToNavigation(bool rideAccecpted){
    Navigator.push(
        context,
        // MaterialPageRoute(builder: (context)=>UserProfile('','')
        MaterialPageRoute(builder: (context)=>NavigationDriverPage(isRideAccepted:rideAccecpted )

        )
    );
  }

  Widget _buildRideRow(Available_trip trip){
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: Card(
        child: Container(
          margin: const EdgeInsets.only(top: 16, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // chaque éléments de la liste des ride est divisé en 2 colonnes
              //la première colonne contient une ligne ayant 3 éléments (2 colonnes et un divier)
              //la deuxième contient 1 ligne ayant 2 éléments (1 colonne et un bouton)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  
                  Column(
                    children: <Widget>[
                   if(trip.profileImageUrl != null)
                     Container(
                     width: 100,
                     height: 100,
                     decoration: BoxDecoration(
                         shape: BoxShape.circle,
                         image: DecorationImage(
                           image: NetworkImage('https://app.taxiride.biz:81/api/commons/download/'+trip.profileImageUrl),
                           fit: BoxFit.fill,
                         )
                     ),
                   )
                   else Container(
                       width: 75,
                       height: 75,
                       decoration: BoxDecoration(
                           color: Colors.yellow,
                           shape: BoxShape.circle
                       ),
                       child: Icon(  Icons.person)),
                      Text(trip.name, style: TextStyle(fontFamily: 'Poppins', fontSize: 11, color: Colors.black),)

                    ],
                  ),


                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Pick Up', style: TextStyle(fontFamily: 'Poppins', fontSize: 11,  color: Colors.grey),),
                      Text(trip.departure, style: TextStyle(fontFamily: 'Poppins', fontSize: 11, color: Colors.black),),

                      Text('Drop Off', style: TextStyle(fontFamily: 'Poppins', fontSize: 11,  color: Colors.grey),),
                      Text(trip.arrival, style: TextStyle(fontFamily: 'Poppins', fontSize: 11, color: Colors.black),),

                      Text('Estimated distance', style: TextStyle(fontFamily: 'Poppins', fontSize: 11,  color: Colors.grey),),
                      Text(trip.distance.toString(), style: TextStyle(fontFamily: 'Poppins', fontSize: 11, color: Colors.black),),

                    ],
                  ),
                  
                ],
              ),

              Padding(
                padding: const EdgeInsets.only(top: 32, bottom: 32,),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child:  Column(
                        children: <Widget>[
                          Text('Ride Price', style: TextStyle(fontFamily: 'Poppins', color: Colors.grey),),
                          Text(trip.cost.toString(), style: TextStyle(fontFamily: 'Poppins', fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black),)

                        ],
                      ),
                    ),

                    BaseButton(
                      onPressed: (){
                       _callAcceptRide(trip.requestId.toString(), trip.tripId.toString() );
                      },
                      text: 'ACCEPT RIDE',
                      color: Colors.green,
                      textColor: Colors.white,
                    )
                  ],
                ),
              ),

            ],
          ),
        )
      ),
    );
  }

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      _getAddressFromLatLng();


    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _city =place.locality;
        _country = place.country;
       // "${place.locality}, ${place.postalCode}, ${place.country}";
      });
      _findRides2();
    } catch (e) {
      print(e);
    }
  }
  
  @override
  void initState() {
    super.initState();
    
    _getCurrentLocation();
  }

  Future<List<Available_trip>> _findRides( double lat, double lng ) async {

    final prefs =await SharedPreferences.getInstance();

    var uri =Uri.https('app.taxiride.biz:81', '/api/rides/findAllAvailableTrip/'+prefs.get('username')+'/'+ lat.toString() + '/'+ lng.toString()+ '/'+_country+'/'+_city+'/'+radius);
    final http.Response response = await http.get(
      uri,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
      },

    );

    print(response.request.url.toString());
    log('code response '+response.statusCode.toString());

    print(response.body);
    if(response.statusCode == 200){

      var rb = response.body;

      // store json data into list
      var list = json.decode(rb) as List;

      // iterate over the list and map each object in list to Img by calling Img.fromJson
      availableTrips = list.map((i)=>Available_trip.fromJson(i)).toList();

    }
    isRideApiCalled = true;
    setState(() {
      if(availableTrips.length == 0)  noRideFound = true;
    });

    return availableTrips;

  }

  void _findRides2( ) async {

    final prefs =await SharedPreferences.getInstance();

    var uri =Uri.https('app.taxiride.biz:81', '/api/rides/findAllAvailableTrip/'+prefs.get('username')+'/'+ _currentPosition.longitude.toString() + '/'+ _currentPosition.latitude.toString()+ '/'+_country+'/'+_city+'/'+radius);
    final http.Response response = await http.get(
      uri,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
      },

    );

    print(response.request.url.toString());
    log('code response '+response.statusCode.toString());

    print(response.body);
    if(response.statusCode == 200){

      var rb = response.body;

      // store json data into list
      var list = json.decode(rb) as List;

      // iterate over the list and map each object in list to Img by calling Img.fromJson
      availableTrips = list.map((i)=>Available_trip.fromJson(i)).toList();

    }
    isRideApiCalled = true;
    setState(() {
      if(availableTrips.length == 0)  noRideFound = true;
    });


  }

  Widget _buildRide( List<Available_trip> trips ){
    return Container(
      width: 342,
      height: 267,
      child: ListView.builder(
        padding: EdgeInsets.all(16),
        itemCount: trips.length,
        itemBuilder: (context, i){
          return _buildRideRow(trips[i]);
        },
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);

    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text('FIND RIDES', style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Poppins'),),
      ),

      body: Container(
        margin: const EdgeInsets.all(16.0),
        child: Stack(
          children: <Widget>[

//            Visibility(
//              visible: !noRideFound,
//              child:FutureBuilder<List<Available_trip>>(
//                future: _findRides( _currentPosition.latitude, _currentPosition.longitude),
//                builder: (context, snapshot) {
//                  if(snapshot.hasData){
//                    _buildRide(snapshot.data);
//                  }
//                  return CircularProgressIndicator();
//                },
//              ),
//            ),

            Visibility(
              // on a pas encore appelé l api
              visible: !isRideApiCalled,
              child:Center(
                child: CircularProgressIndicator(),
              ),
            ),
            Visibility(
              //on a appelé l api et on a trouvé des ride
              visible: isRideApiCalled && !noRideFound,
              child:_buildRide(availableTrips),
            ),

         Visibility(
           //on a appelé l api et on n a pas trouvé de ride
           visible: noRideFound && isRideApiCalled,
           child: Center(
             child: Text('Aucune course disponible', ),
           ),
         ),

         Align(
           alignment: Alignment.bottomCenter,
           child:  Container(
             height: 75,
             child:   Card(
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                 children: <Widget>[
                   BaseButton(
                     onPressed: (){
                       setState(() {
                         noRideFound = false;
                         isRideApiCalled = false;
                       });
                     },
                     textColor: Colors.white,
                     color: Colors.black,
                     text: 'REFRESH FEED',
                   ),
                   BaseButton(
                     onPressed: (){
                       goToNavigation(false);
                     },
                     textColor: Colors.white,
                     color: Colors.black,
                     text: 'CHANGE RADIUS',
                   ),
                 ],

               ),
             ),
           ),

         ),

          ],
        ),
      )
    );
  }

  _callAcceptRide(String req, String tripid) async{

  //  await pr.show();
    final prefs =await SharedPreferences.getInstance();
    final http.Response response = await http.post('https://app.taxiride.biz:81/api/auth/register',
      headers: <String, String>{
        'Content-Type':'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
      },
      body: jsonEncode(<String, String>{
        "emailorphone": prefs.get('username'),
        "latitude": _currentPosition.latitude.toString(),
        "longitude": _currentPosition.longitude.toString(),
        "requestid": req,
        "tripID": tripid
      }),
    );

    log('code _callAcceptRide: '+ response.statusCode.toString());

   // await pr.hide();

    if(response.statusCode == 200){
      goToNavigation(true);
    }else{
      log('error ');
      Toast.show('An error occur', context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
    }

  }

}