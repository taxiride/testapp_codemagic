import 'package:flutter/material.dart';
import 'package:taxiride_ios/widget/button/base_button.dart';

class PrimaryButton extends BaseButton {
  final Function onPressed;
  final Widget child;
  final String text;
  final EdgeInsets padding;

  PrimaryButton({this.padding, @required this.onPressed, this.child, this.text})
      : super(
      padding: padding,
      onPressed: onPressed,
      color: Colors.green,
      text: text,
      child: child);
}
