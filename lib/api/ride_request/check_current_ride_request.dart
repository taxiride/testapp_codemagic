import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';

class CheckCurrentRideRequest extends AbstractAuthenticatedRequest{

  final AuthProvider authProvider;

  CheckCurrentRideRequest(this.authProvider):super(authProvider.token);

  @override
  String getUrl() {
   return "/api/rides/checkCurrentRide/"+authProvider.phone;
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  Map<String, String> getParams() {
    return {
      "emailOrPhone":authProvider.phone
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
   dynamic data = response.httpResponse.body;
   if(response.httpResponse.statusCode == 200){
     Map<String,dynamic> res = jsonDecode(data);
     return res;
   }else{
     return null;
   }
  }

}