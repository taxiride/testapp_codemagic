import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/option_coast.dart';

class FindOptionByCityRequest extends AbstractAuthenticatedRequest{
   final String cityCode;
   final String countryCode;
   final int estimatedTripDistance;
   final int estimatedTripDuration;
   final String language;
   final String token;

  FindOptionByCityRequest(this.cityCode, this.countryCode, this.estimatedTripDistance, this.estimatedTripDuration, this.language, this.token):super(token);

  @override
  String getUrl() {
   return "/api/rides/findOptionsByCost/$countryCode/$cityCode/$estimatedTripDistance/$estimatedTripDuration/$language";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  Map<String, String> getParams() {

    return {
      "cityCode":cityCode,
      "countryCode":countryCode,
      "estimatedTripDistance":estimatedTripDistance.toString(),
      "estimatedTripDuration":estimatedTripDuration.toString(),
      "language":"fr"
    };
  }

  @override
  List<OptionCoast> parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    List<OptionCoast> list_option = [];
    if(response.httpResponse.statusCode == 200){
      List<dynamic> list = jsonDecode(data);
     // print(list.length);
      list.forEach((element) {
        Map<String, dynamic> option = element;
       // print(option['optionCode']);
      //  print(option['travelCost']['tripCost']);
        OptionCoast opt = OptionCoast(optionCode: option['optionCode'],tripCost: option['travelCost']['tripCost']);
        list_option.add(opt);

      });
      return  list_option;

     // print(data);
    }else{
      print(response.httpResponse.statusCode);
     return list_option;
    }
  }






}