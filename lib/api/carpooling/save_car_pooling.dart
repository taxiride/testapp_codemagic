import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:taxiride_ios/models/post_lond_distance_trip_dto.dart';
class SaveCarPooling{
  String token;
  PostLongDistanceTripDto postLongDistanceTripDto;

  SaveCarPooling({this.token,this.postLongDistanceTripDto});


  Future<dynamic> saveCarPoolingDriver() async {

    final http.Response response = await http.post(
      'https://app.taxiride.biz:81/api/carpooling/postLongDistanceTrip',
      headers: <String, String>{
        'Content-Type':'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer '+token,
      },
      body: jsonEncode(<String, dynamic>{
        "arrival":postLongDistanceTripDto.arrival,
        "arrivalLatitude":postLongDistanceTripDto.arrivalLatitude,
        "arrivalLongitude":postLongDistanceTripDto.arrivalLongitude,
        "cost":postLongDistanceTripDto.cost,
        "costNegotiable":postLongDistanceTripDto.costNegotiable,
        "country":postLongDistanceTripDto.country,
        "departureDate":postLongDistanceTripDto.departureDate,
        "departure":postLongDistanceTripDto.departure,
        "departureLatitude":postLongDistanceTripDto.departureLatitude,
        "departureLongitude":postLongDistanceTripDto.departureLongitude,
        "emailOrPhone":postLongDistanceTripDto.emailOrPhone,
        "endCity":postLongDistanceTripDto.endCity,
        "language":postLongDistanceTripDto.language,
        "numberOfSeat":postLongDistanceTripDto.numberOfSeat,
        "postDate":postLongDistanceTripDto.postDate,
        "startCity":postLongDistanceTripDto.startCity,
        "travelCondition":postLongDistanceTripDto.travelCondition,
        "tripTimeOut":postLongDistanceTripDto.tripTimeOut
      }),
    );
    if(response.statusCode == 200){
      // print(jsonDecode(response.body));
      print("-------------tout c est bien passe------------");
    }else{
      print(response.statusCode);
      print(jsonDecode(response.body));
      print("------------- requette echouer  ------------");

    }

  }


  Future<dynamic> saveCarPoolingBooking(String phone) async {

    final http.Response response = await http.post(
      'https://app.taxiride.biz:81/api/carpooling/postLongDistanceTripBooking',
      headers: <String, String>{
        'Content-Type':'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer '+token,
      },
      body: jsonEncode(<String, dynamic>{
        "departurePeriodEndDate": postLongDistanceTripDto.departureDate,
        "departurePeriodStartDate": postLongDistanceTripDto.departureDate,
        "emailOrPhone": phone,
        "endCity": postLongDistanceTripDto.endCity,
        "language": "fr",
        "numberOfPlacesRequired": postLongDistanceTripDto.numberOfSeat,
        "seatCost": postLongDistanceTripDto.cost,
        "startCity": postLongDistanceTripDto.startCity,
        "travelId": 0


      }),
    );
    if(response.statusCode == 200){
      // print(jsonDecode(response.body));

      print("-------------tout c est bien passe------------");
    }else{
      print(response.statusCode);
      print(jsonDecode(response.body));
      print("------------- requette echouer  ------------");

    }


  }

  Future<dynamic> findAllTripBookingRequest(int booking,String endCity,String starCity,String language,String stardate) async {

    final http.Response response = await http.get(
      'https://app.taxiride.biz:81/api/carpooling/findAllLongDistanceTripBooking/$starCity/$endCity/$stardate/0/fr',
      headers: <String, String>{
        'Content-Type':'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer '+token,
      },

    );
    if(response.statusCode == 200){
      // print(jsonDecode(response.body));
      print("-------------tout c est bien passe------------");
    }else{
      print(response.statusCode);
      print(jsonDecode(response.body));
      print("------------- requette echouer  ------------");

    }

  }
}