import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/trip_responce.dart';

class FindTripInfosRequest extends AbstractAuthenticatedRequest{
  final String token;
  final int id;

  FindTripInfosRequest(this.token, this.id):super(token);

  @override
  String getUrl() {
   return "/api/rides/findTripInfo/$id/fr";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  Map<String, String> getParams() {
    return {
      "language":"fr",
      "tripId":id.toString()
    };
  }

  @override
  TripResponce parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print("--------- request are succefull--------------");
      TripResponce tripResponce = TripResponce.fromJson(jsonDecode(data));
      return tripResponce;
    }else{
      print(response.httpResponse.statusCode);
      print("--------- request are fail --------------");
      print(data);
      return null;
    }

  }

}