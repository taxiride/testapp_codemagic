import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/models/accesToken.dart';
import 'package:taxiride_ios/models/apiresponse/loginresp.dart';

class PrefsManager{

  Future<bool> saveAccesToken(LoginResponse l) async
  {

    final prefs = await SharedPreferences.getInstance();
     prefs.setString('acces_token', l.accessToken);
    prefs.setString('refresh_token', l.refreshToken);
    prefs.setString('username', l.username);

    return true;
  }

  Future<AccessToken> getAccessToken() async{
    final prefs = await SharedPreferences.getInstance();

    return new AccessToken(prefs.get('acces_token'), prefs.get('acces_token'), prefs.get('username'));

  }


}