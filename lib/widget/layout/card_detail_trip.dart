import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:taxiride_ios/models/card_detail_trip_args.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/providers/rides_provider.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/screens/driver/driver_profil_screen.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:taxiride_ios/widget/button/secondary_button.dart';

class CardDetailTrip extends StatelessWidget{
  final CardDetailTripArgs args;

  const CardDetailTrip({Key key, this.args}) : super(key: key);



  //@override
 // _CardDetailTripState createState() => _CardDetailTripState();
//}class _CardDetailTripState extends State<CardDetailTrip>{


  @override
  Widget build(BuildContext context) {
  //  TextEditingController myController = TextEditingController();
    final ProgressDialog pr = ProgressDialog(context);
    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Card(
              shadowColor: Colors.grey,
              shape: cardShape(),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 155,top: 20,bottom: 10),
                        child: Row(
                          children: [
                            Image.asset(
                                'images/shape_get_started.png'
                            ),
                            Container(
                                padding: EdgeInsets.only(right: 5,left: 5),
                                child: Text('To' , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),))
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width*0.7,
                        child: ListTile(
                          title: Text(args.arriv , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),maxLines: 1,),
                          trailing: IconButton(icon: Icon(Icons.add),),
                        ),
                      ),
                    ],
                  ),
                  Divider(height: 5,color: Colors.grey,),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 155,top: 20,bottom: 10),
                        child: Row(
                          children: [
                            Icon(Icons.location_on,color: Colors.orangeAccent,),
                            Container(
                                padding: EdgeInsets.only(right: 5,left: 5),
                                child: Text('From' , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),)
                            )
                          ],
                        ),
                      ),

                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width*0.7,
                        child: ListTile(
                          title: Text(args.depart, style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),maxLines: 1,),
                          trailing: IconButton(icon: Icon(Icons.add),),
                        ),
                      ),
                    ],
                  ),
                  (!args.isValided)?Container(
                    padding: EdgeInsets.all(10),
                    child: Text("waiting for Driver..."),
                  ):SizedBox(height: 5,)
                ],
              ),
            ),

            (!args.isValided)?InkWell(
              onTap: (){
                Navigator.push(context,MaterialPageRoute(
                  builder: (_) => DriverProfilScren(),
                ));
              },
              child: Card(
                shape: cardShape(),
                child: Container(
                  height: MediaQuery.of(context).size.height*0.13,
                  child: Row(

                    children: [
                      Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        ),
                        child: (args.driver != null && args.driver.imageUrl != null)?CircleAvatar(
                          radius: 100,
                          backgroundImage: NetworkImage('https://app.taxiride.biz:81/api/commons/download/'+args.driver.imageUrl) ,
                        ):Icon(
                          Icons.account_circle,
                          color: Colors.black,
                        ),
                      ),

                      Container(
                        child: Column(
                          children: [
                            Container(
                              child: Text((args.driver != null && args.driver.firstName != null)?args.driver.firstName:'no name'),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Icon(Icons.star_border),
                                  Icon(Icons.star_border),
                                  Icon(Icons.star_border),
                                  Icon(Icons.star_border),
                                  Icon(Icons.star_border),
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ):SizedBox(height: 8,),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width*0.4,
                    child: BlackButton(text: "REPORT ABUSE",onPressed: (){
                      _showDialogReportAbuse(context);
                    },)
                ),
                Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width*0.38,
                    child: SecondaryButton(text: "CANCEL TRIP",onPressed: (){
                      _showDialogCancelTrip(context);
                    },))
              ],
            )

          ],
        ),
      ),
    );
  }

  void _showDialogCancelTrip(BuildContext mycontext) {
    // flutter defined function
    showDialog(
      context: mycontext,
      builder: (BuildContext context) {
        AuthProvider authProvider = Provider.of<AuthProvider>(context);
        RidesProvider ridesProvider = Provider.of<RidesProvider>(context);

        final ProgressDialog pr = ProgressDialog(context);
        pr.style(
          message: 'Please wait...',
          backgroundColor: Colors.white,
          progressWidget: CircularProgressIndicator(),
          elevation: 10.0,
        );

        // return object of type Dialog
        return AlertDialog(
          title: Center(child: Icon(Icons.info,color: Colors.red,),),
          content: SizedBox(),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Center(
                  child: Column(
                    children: [
                      Text("Are you sure you want",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                      Text("to cancel the trip ?",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  width: MediaQuery.of(context).size.width*0.9,
                  child: Center(
                    child: Column(
                      children: [
                        Text("The Driver is already an this waw"),
                        Text("You will be changed")
                      ],
                    ),
                  ),
                  
                ),

                Container(
                  padding: EdgeInsets.only(top: 10,bottom: 30),
                  child: Center(
                    child: Text("N 100",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width*0.3,
                        child: PrimaryButton(text: 'GO BACK',onPressed: (){
                          Navigator.of(context).pop();

                        },),
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width*0.3,
                          child: SecondaryButton(text: 'YES CANCEL',onPressed: () async {
                            await pr.show();
                            ridesProvider.cancelTripById(authProvider,ridesProvider.tripId);
                           await pr.hide();
                            Navigator.of(context).pop();},))
                    ],
                  ),
                )
              ],
            ),

          ],
        );
      },
    );
  }

  void _showDialogReportAbuse(BuildContext mycontext) {
    // flutter defined function
    showDialog(
      context: mycontext,
      builder: (BuildContext context) {
        TextEditingController myController = TextEditingController();
        // return object of type Dialog
        return AlertDialog(
          title: Center(child: Icon(Icons.info,color: Colors.red,),),
          content:SizedBox(),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Center(child: Text("Report Abuse",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),),

                Container(
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width*0.9,
                  child: TextField(
                    minLines: 1,
                    maxLines: 6,
                    controller: myController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Your Review',
                    ),
                  ),
                ),

                Container(
                  padding: EdgeInsets.all(15),
                  child: PrimaryButton(text: 'REPORT',onPressed: (){
                    Navigator.of(context).pop();
                  },),
                )

              ],
            ),

          ],
        );
      },
    );
  }

}