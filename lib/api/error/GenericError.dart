

import 'package:taxiride_ios/api/abstract/AbstractApiError.dart';

import 'ApiErrorType.dart';

class GenericError extends AbstractApiError{

  String message;
  int code = 6;
  GenericError(this.message,{this.code}) : super(code, message, ApiErrorType.genericError);
}