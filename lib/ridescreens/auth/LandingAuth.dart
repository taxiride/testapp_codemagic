import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/ridescreens/auth/login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthChoicePage extends StatefulWidget{
  AuthChoicePage();

 @override
  _AuthChoicePageState createState() => _AuthChoicePageState();

}

class _AuthChoicePageState extends State<AuthChoicePage>{
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;


  @override
  Widget build(BuildContext context) {


    Widget authButton = Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 50, top: 20),
            child: _buildAuthButton(Colors.black, Icons.call, 'Get started with phone', 0),
          ),
          Container(
            margin: const EdgeInsets.only(top: 50, ),
            child:  orserction,
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, bottom: 25),
            child:  _buildAuthButton(Colors.red, Icons.email, 'Continue with Google',1),
          ),
          Container(
              child: _buildAuthButton(Colors.blue, Icons.face, 'Continue with Facebook',2)
          ),

        ],
      ),
    );

   return Scaffold(
     body:  Stack(
       children: <Widget>[
         Image.asset('images/login_backgroung.png',
           width: 375,
           height: 667,
           fit: BoxFit.cover,
         ),
         Align(
           alignment: Alignment.center,
           child: Column(
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[
               Center(
                 child: authButton,
               )
             ],
           ),
         ),
       ],
     ),
   );
  }

  RaisedButton _buildAuthButton(Color col, IconData icon, String label, int provider){
    return RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side:BorderSide(color: col  ),

        ),
        onPressed: () async{
         if(provider == 0) _navivgateToLogin();
         else if (provider == 1) _authWithGoogle();
        },
        color: col,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(icon, color: Colors.white,),
            Container(
              margin: const EdgeInsets.only(left: 16),
              child: Text(label, style: TextStyle(fontSize: 14, color: Colors.white, fontFamily: 'fonts/Poppins-Regular.ttf')),
            )
          ],
        )

    );

  }

  Widget orserction = Container(
    padding: const EdgeInsets.all(2),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Divider(height: 1, color: Colors.black,),
        Text('OR', style: TextStyle(color: Colors.white),),
        Divider(height: 1, color: Colors.black,)
      ],
    ),
  );

  void _navivgateToLogin(){
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()));
  }

  //auth avec google
  Future<FirebaseUser> _authWithGoogle() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
    print("signed in " + user.displayName);
    if (user == null) {
      Scaffold.of(context).showSnackBar(const SnackBar(
        content: Text('No one has signed in.'),
      ));
      return null;
    }
    return user;
  }


}