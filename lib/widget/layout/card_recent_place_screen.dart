import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taxiride_ios/models/validator.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';

class CardRecentPlaceScreen extends StatefulWidget{

  @override
  _CardRecentPlaceState createState() => _CardRecentPlaceState();
}

class _CardRecentPlaceState extends State<CardRecentPlaceScreen>{
  TextEditingController selectCity = TextEditingController();
  List<String> listCity = ["douala","bafia","ebolowa","bertoua","bamenda"];
  String ville = "";

  @override
  Widget build(BuildContext context) {

    return (ville == "")?Container(
      child: Column(
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: AutoCompleteTextField(
                  controller: selectCity,
                  suggestions: listCity,
                  style: TextStyle(color: Colors.black,fontSize: 16),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30)
                    )
                  ),
                  itemFilter: (item,query){
                    return item.toString().toLowerCase().startsWith(query.toLowerCase());
                  },
                  itemSorter: (a,b){
                    return a.toString().compareTo(b);
                  },
                  itemSubmitted: (item){
                    print("-------------------------");
                    selectCity.text = item;
                    print(selectCity.text.toString());
                  },
                  clearOnSubmit: false,
                  itemBuilder: (context,item){
                    return Container(
                      padding: EdgeInsets.all(5),
                      child: ListTile(
                        leading: Icon(Icons.near_me,color: Colors.yellow,),
                        title: Text(item,style: TextStyle(color: Colors.black),),
                        trailing: Icon(Icons.add,color: Colors.orange,),
                        onTap: (){
                          setState(() {
                            ville = item.toString();

                          });
                        },

                      ),
                    );
                  },

                ),
              ),
            ],
          ),
          (Validator.isNotEmpty(selectCity.text.toString()))?Container(
            padding: EdgeInsets.all(10),
            child: PrimaryButton(
              text: "SEARCH",
              onPressed: (){
                setState(() {
                  ville = selectCity.text.toString();
                });
              },
            ),
          ):SizedBox(height: 5,)

        ],
      ),

    ):Container(
      height: MediaQuery.of(context).size.height*0.4,
      margin: EdgeInsets.all(10),
      child: Card(
        shadowColor: Colors.grey,
        shape: cardShape(),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              child: Text("Result Search",style: TextStyle(fontWeight: FontWeight.bold),),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height*0.3,
              child: ListView(
                children: [
                  ListTile(
                    leading: Icon(Icons.near_me,color: Colors.yellow,),
                    title: Text("Yaounde: Total Bastos ",style: TextStyle(color: Colors.black),),
                    trailing: Icon(Icons.add,color: Colors.orange,),
                    onTap: (){
                      setState(() {});
                    },

                  ),
                  ListTile(
                    leading: Icon(Icons.near_me,color: Colors.yellow,),
                    title: Text("Yaounde: College Vogt",style: TextStyle(color: Colors.black),),
                    trailing: Icon(Icons.add,color: Colors.orange,),
                    onTap: (){
                      setState(() {});
                    },

                  ),
                  ListTile(
                    leading: Icon(Icons.near_me,color: Colors.yellow,),
                    title: Text("Yaounde: Total Nkolbisson ",style: TextStyle(color: Colors.black),),
                    trailing: Icon(Icons.add,color: Colors.orange,),
                    onTap: (){
                      setState(() {});
                    },

                  ),
                  ListTile(
                    leading: Icon(Icons.near_me,color: Colors.yellow,),
                    title: Text("Yaounde: ekounou",style: TextStyle(color: Colors.black),),
                    trailing: Icon(Icons.add,color: Colors.orange,),
                    onTap: (){
                      setState(() {});
                    },

                  ),
                  ListTile(
                    leading: Icon(Icons.near_me,color: Colors.yellow,),
                    title: Text("Yaounde: Fokou Etoudi",style: TextStyle(color: Colors.black),),
                    trailing: Icon(Icons.add,color: Colors.orange,),
                    onTap: (){
                      setState(() {});
                    },

                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}