import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/find_profil_by_phoneNumber_request.dart';
import 'package:taxiride_ios/api/ride_request/find_geocode_request.dart';
import 'package:taxiride_ios/api/ride_request/find_option_by_city_request.dart';
import 'package:taxiride_ios/models/apiresponse/o_s_m_places.dart';
import 'package:taxiride_ios/models/apiresponse/profileresp.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart'  as geoloc;
import 'package:taxiride_ios/models/matrix.dart';
import 'package:taxiride_ios/models/option_coast.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';
import 'package:taxiride_ios/models/profile.dart';
import 'package:taxiride_ios/models/ville.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/providers/rides_provider.dart';
import 'package:taxiride_ios/ridescreens/auth/LandingAuth.dart';
import 'package:taxiride_ios/screens/carpooling/card_trip_waiting_driver.dart';
import 'package:taxiride_ios/screens/carpooling/card_trip_waiting_rider.dart';
import 'package:taxiride_ios/screens/carpooling/form_infos_dest_screen.dart';
import 'package:taxiride_ios/screens/driver/form_infos_car_pooling_driver_screen.dart';
import 'package:taxiride_ios/widget/layout/card_recent_place_screen.dart';

import '../profile_infos_screen.dart';

class  PoolingDriverWaitingScreen extends StatefulWidget{
  final String role;
  final String origin;
  final List<PoolingTrip> listPoolings;

  const PoolingDriverWaitingScreen({Key key, this.role, this.origin,this.listPoolings}) : super(key: key);

  // CarPoolingScreen(String role, String origin);

  @override
  _CarPoolingState createState() => _CarPoolingState();

}

Future<Profile> findByPhoneNumber() async {

  final prefs =await SharedPreferences.getInstance();

  var uri =Uri.https('app.taxiride.biz:81', '/api/profile/findbyphonenumber/'+prefs.get('username')+'/fr');
  final http.Response response = await http.get(
    uri,
    headers: <String, String>{
      HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
    },

  );

  log('code'+response.statusCode.toString());

  if(response.statusCode == 200){
    Profile user = Profile.fromJson(json.decode(response.body));

    return user;
  }

}






Profile currentUserProfile;

class _CarPoolingState extends State<PoolingDriverWaitingScreen>{


  List<OSMPlaces> places;
  Future<Profile> userProfile;
  GoogleMapController mapController;

  LatLng _center = new LatLng(3.8731589, 11.4626538);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  geoloc.Location location = new geoloc.Location();

  bool _serviceEnabled;
  geoloc.PermissionStatus _permissionGranted;

  OSMPlaces selectedPlaces;
  bool isOSMPlacesFound = false, isFromPlacesFound = false, isOSMPlacesSelected = false, showRideDetails = false;

  Locale myLocale ;
  int step = 0,solde=500,amount=0;
  bool choix1 = false,choix2=false,choix3=false,valide=true,select=false;
  String adresse_riser,city_rider;
  Ville ville_rider;
  List<OptionCoast> options = [];
  OptionCoast _optionCoast;
  int indice = 0;

  Matrix matrice;

  Position position;
  var geolocator = Geolocator();
  Map<String,dynamic> data = new Map();





  //active les service de localisation et demande la permission pour accéder à la position courrante de l'utilisateur

  void _enableLocationService() async{

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == geoloc.PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != geoloc.PermissionStatus.granted) {
        //appel de la fonction pour récupérer la position
        //afichage de la map
        _getCurrentUserPosition();
        return;
      }
    }

  }


  void _getCurrentUserPosition() async{
    final prefs = await SharedPreferences.getInstance();
    if(prefs.getKeys().contains('depart')){
      data['depart'] = prefs.getString('depart');
      data['arriver'] = prefs.getString('arriver');
    }

    position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    _getAddressFromLatLng(position);
    print(position.latitude.toString() + ', ' + position.longitude.toString());
    GeocodeRequest geocodeRequest = GeocodeRequest();
    ville_rider =  await geocodeRequest.getNamePosition(position);
    adresse_riser = ville_rider.name;

  }

  _getAddressFromLatLng(Position position) async {
    String currentAddress;
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          position.latitude, position.longitude);
      Placemark place = p[0];
      city_rider = place.locality;
      currentAddress = "${place.locality}, ${place.postalCode}, ${place.country}";
      print(currentAddress);

      setState(() {
        currentAddress = "${place.locality}, ${place.postalCode}, ${place.country}";
      });

    } catch (e) {
      print(e);
    }
  }

  void _onMapCreated(GoogleMapController controller){
    mapController = controller;
    _center = LatLng(position.latitude, position.longitude);
  }

  //écoute le changement de position
  void _startListenningPosition(){
    var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);

    StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen((Position position) {
      print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
      this.position = position;
    });
  }

  @override
  void initState() {
    super.initState();
    userProfile = findByPhoneNumber();
    // _getCurrentLocation();

    _enableLocationService();
    _getCurrentUserPosition();
    _startListenningPosition();


  }

  void searchAdress(String adress) async {

    log('adress '+adress);
    print('country '+myLocale.countryCode);
    // GeocodeRequest geocodeRequest = GeocodeRequest();
    //print("---------------------------------- name ----------------------");
    // geocodeRequest.getNamePosition(position);

    final http.Response response = await http.get(
      'https://nominatim.openstreetmap.org/?addressdetails=1&q='+adress+'&format=json&countrycodes=cm&limit=10',
      headers: <String, String>{
        HttpHeaders.userAgentHeader: 'User-Agent: Retrofit-Sample-App',
      },
    );

    print(response.request.url.toString());
    log('code response '+response.statusCode.toString());

    print(response.body);
    if(response.statusCode == 200){
      var rb = response.body;
      // store json data into list
      var list = json.decode(rb) as List;
      // iterate over the list and map each object in list to Img by calling Img.fromJson
      places = list.map((i)=>OSMPlaces.fromJson(i)).toList();
      setState(() {
        isOSMPlacesFound = true;
      });
    }

  }



  //construction des cartes qui vonst afficher les détails de la course

  //bouton pour la recherche des places



  bool isGetStartedClicked = false;

  @override
  Widget build(BuildContext context) {


    myLocale = Localizations.localeOf(context);

    final ProgressDialog pr = ProgressDialog(context);
    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );
    // obbject pour avoir tous les informations d un user sans plus apeller les preferences chaque fois
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    RidesProvider ridesProvider = Provider.of<RidesProvider>(context);


    void navigateToProfile(Profil profil){
      authProvider.setProfil(profil);
      Navigator.push(
          context,
          // MaterialPageRoute(builder: (context)=>UserProfile('','')
          MaterialPageRoute(builder: (context)=>ProfileInfosScreen(profile: profil,)
          )
      );
    }

    void logout() async {
      final prefs = await SharedPreferences.getInstance();
      authProvider.logout();

      await prefs.setBool('is_connected', false);
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AuthChoicePage()));
    }

    return MaterialApp(
      home:   Scaffold(
        key: _scaffoldKey,
        body:Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                  target: _center,
                  zoom: 13.0
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin: const EdgeInsets.only(top: 16),
                  child: FloatingActionButton(
                    onPressed: ()=>_scaffoldKey.currentState.openDrawer(),
                    materialTapTargetSize: MaterialTapTargetSize.padded,
                    backgroundColor: Colors.amber,
                    child: const Icon(Icons.dehaze, size: 36.0, color: Colors.black,),
                  ),
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 80),
                child: (widget.role=="driver")?CardTripWaitingDriver(poolingTrip: widget.listPoolings[0],):CardTripWaitingRider(poolingTrip: widget.listPoolings[0],)),

          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              FutureBuilder<Profile>(
                future: userProfile,
                builder: (context, snapshot){
                  if(snapshot.hasData){
                    currentUserProfile = snapshot.data;
                    return  DrawerHeader(
                      child: Container(
                        margin: const EdgeInsets.only(top: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            if( snapshot.data.imageUrl != null)
                              Container(
                                width: 75,
                                height: 75,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      image: NetworkImage('https://app.taxiride.biz:81/api/commons/download/'+snapshot.data.imageUrl),
                                      fit: BoxFit.fill,
                                    )
                                ),
                              )
                            else Container(
                                width: 75,
                                height: 75,
                                decoration: BoxDecoration(
                                    color: Colors.yellow,
                                    shape: BoxShape.circle
                                ),
                                child: Icon(  Icons.person)),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text('Hi,', style: TextStyle(fontSize: 12, fontFamily: 'Poppins'),),
                                Text((snapshot.data.firstName != null)?snapshot.data.firstName:"No first name valid", style: TextStyle(fontSize: 12, fontFamily: 'Poppins'),)
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }
                  return CircularProgressIndicator();
                },
              ),

              ListTile(
                title:  Text('Profile', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: () async {

                  final prefs =await SharedPreferences.getInstance();
                  String token = prefs.getString('access_token');
                  String username = prefs.getString('username');
                  FindProfileByPhoneNumber profile = FindProfileByPhoneNumber(token,username);
                  profile.findProfilByPhone().then((value){
                    print(value.firstName);
                    navigateToProfile(value);

                  });
//                            userProfile.then((value){
//                                    print(value.firstName);
//                                    navigateToProfile(value);
//
//                                  });

                },
              ),
              ListTile(
                title:  Text('Payments', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Stats', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Promo code', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Settings', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:  Text('Logout', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                onTap: (){
                  logout();
                  Navigator.pop(context);
                },
              ),

            ],
          ),
        ),
      ),
    );

  }



  fiindOptionCity(AuthProvider authProvider) async {
    print(authProvider.token);
    FindOptionByCityRequest findOptionByCityRequest = FindOptionByCityRequest(city_rider,selectedPlaces.address.countryCode,matrice.distance.ceil(),matrice.time,"en",authProvider.token);
    return findOptionByCityRequest.parseResult(await ApiClient.execOrFail(findOptionByCityRequest));
  }


}