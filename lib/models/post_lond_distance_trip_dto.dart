class PostLongDistanceTripDto{
  String arrival;
  double arrivalLatitude;
  double arrivalLongitude;
  int cost;
  bool costNegotiable;
  String country;
  String departureDate;
  String departure;
  double departureLatitude;
  double departureLongitude;
  String emailOrPhone;
  String endCity;
  String language;
  int numberOfSeat;
  String postDate;
  String startCity;
  List<String> travelCondition;
  double tripTimeOut;

  PostLongDistanceTripDto({
    this.arrival,
    this.arrivalLatitude,
    this.arrivalLongitude,
    this.cost,
    this.costNegotiable,
    this.country,
    this.departureDate,
    this.departure,
    this.departureLatitude,
    this.departureLongitude,
    this.emailOrPhone,
    this.endCity,
    this.language,
    this.numberOfSeat,
    this.postDate,
    this.startCity,
    this.travelCondition,
    this.tripTimeOut
  });

}