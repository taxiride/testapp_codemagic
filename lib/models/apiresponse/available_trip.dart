/// name : "Jordan Toutsop"
/// cost : 17005.0
/// distance : 859.5260963354593
/// departure : "Mairie De Yaoundé 7"
/// arrival : "Étoug-ébé, Yaoundé VI, Communauté urbaine de Yaoundé, Mfoundi, Centre, 1686 YDÉ, Cameroon;Cameroun"
/// startPointLatitude : 3.87278505
/// startPointLongitude : 11.462852496309779
/// profileImageUrl : "PHOTO_55_699028439_af965004-862b-4229-b589-e96327d61728.png"
/// requestId : 7
/// tripId : 7
/// notation : 0.0
/// commentaire : null
/// inTheRange : true

class Available_trip {
  String _name;
  double _cost;
  double _distance;
  String _departure;
  String _arrival;
  double _startPointLatitude;
  double _startPointLongitude;
  String _profileImageUrl;
  int _requestId;
  int _tripId;
  double _notation;
  dynamic _commentaire;
  bool _inTheRange;

  String get name => _name;
  double get cost => _cost;
  double get distance => _distance;
  String get departure => _departure;
  String get arrival => _arrival;
  double get startPointLatitude => _startPointLatitude;
  double get startPointLongitude => _startPointLongitude;
  String get profileImageUrl => _profileImageUrl;
  int get requestId => _requestId;
  int get tripId => _tripId;
  double get notation => _notation;
  dynamic get commentaire => _commentaire;
  bool get inTheRange => _inTheRange;

  Available_trip({
      String name, 
      double cost, 
      double distance, 
      String departure, 
      String arrival, 
      double startPointLatitude, 
      double startPointLongitude, 
      String profileImageUrl, 
      int requestId, 
      int tripId, 
      double notation, 
      dynamic commentaire, 
      bool inTheRange}){
    _name = name;
    _cost = cost;
    _distance = distance;
    _departure = departure;
    _arrival = arrival;
    _startPointLatitude = startPointLatitude;
    _startPointLongitude = startPointLongitude;
    _profileImageUrl = profileImageUrl;
    _requestId = requestId;
    _tripId = tripId;
    _notation = notation;
    _commentaire = commentaire;
    _inTheRange = inTheRange;
}

  Available_trip.fromJson(dynamic json) {
    _name = json["name"];
    _cost = json["cost"];
    _distance = json["distance"];
    _departure = json["departure"];
    _arrival = json["arrival"];
    _startPointLatitude = json["startPointLatitude"];
    _startPointLongitude = json["startPointLongitude"];
    _profileImageUrl = json["profileImageUrl"];
    _requestId = json["requestId"];
    _tripId = json["tripId"];
    _notation = json["notation"];
    _commentaire = json["commentaire"];
    _inTheRange = json["inTheRange"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["name"] = _name;
    map["cost"] = _cost;
    map["distance"] = _distance;
    map["departure"] = _departure;
    map["arrival"] = _arrival;
    map["startPointLatitude"] = _startPointLatitude;
    map["startPointLongitude"] = _startPointLongitude;
    map["profileImageUrl"] = _profileImageUrl;
    map["requestId"] = _requestId;
    map["tripId"] = _tripId;
    map["notation"] = _notation;
    map["commentaire"] = _commentaire;
    map["inTheRange"] = _inTheRange;
    return map;
  }

}