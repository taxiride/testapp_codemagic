import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/location.dart';

class FindLoctionRequest extends AbstractAuthenticatedRequest{
  FindLoctionRequest(String token) : super(token);

  @override
  String getUrl() {
   return "/api/commons/location";
  }

  @override
  String getVerb() {
    return "GET";
  }

  @override
  Location parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      return Location.fromJson(jsonDecode(data));
      //return jsonDecode(data);
    }else{
      return null;
    }
  }

}