import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/condition_travel.dart';

class FindAllTravelsConditions extends AbstractAuthenticatedRequest{
   final String token;
   final String language;

  FindAllTravelsConditions(this.token,this.language) : super(language);

  @override
  String getUrl() {
   return "/api/carpooling/findAllTravelsConditions/$language";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  Map<String, String> getParams() {
    return {
      "language":language
    };
  }
  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      List<ConditionTravel> conditions = ConditionTravel.listFromJson(jsonDecode(data)["object"]);
      return conditions ;
    }
    return [];
  }

}