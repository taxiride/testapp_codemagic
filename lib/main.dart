

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/providers/carpooling_provider.dart';
import 'package:taxiride_ios/providers/rides_provider.dart';
import 'package:taxiride_ios/ridescreens/auth/LandingAuth.dart';
import 'package:taxiride_ios/ridescreens/auth/TestPage.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/ridescreens/navigation_driver.dart';
import 'package:taxiride_ios/theme/style.dart';

import 'models/online_role.dart';

void main() {
    runApp(
      MultiProvider(
          providers: [
              ChangeNotifierProvider(create: (context) => AuthProvider(),),
              ChangeNotifierProvider(create: (context) => RidesProvider(),),
              ChangeNotifierProvider(create: (context) => CarPoolingProvider(),)
      ],
      child: MyApp(),)
    );
}



class MyApp extends StatelessWidget {
    // This widget is the root of your application.

    Future<onlineRole> isOnligne() async {
        final prefs = await SharedPreferences.getInstance();
        bool isConnected = false;
        onlineRole onLinerole = new onlineRole(isOnLine: false, role: 0);
        if(prefs.getKeys().contains('role')){
         onLinerole.setRole(prefs.getInt('role'));
        }

        if(prefs.getKeys().contains('is_connected')){
          onLinerole.setIsOnLine(prefs.getBool('is_connected'));
        }
        return onLinerole;

    }

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Taxiride IOS',
            theme: appTheme(),
            home: Container(
                child: FutureBuilder(
                    future: isOnligne(),
                    builder: (BuildContext context,AsyncSnapshot<onlineRole> snapshot){
                       if(snapshot.hasData){
                         if(snapshot.data.isOnLine){
                            if(snapshot.data.role == 0) return  NavigationRide('rider', 'login' );
                            else return NavigationDriverPage();

                         }else{
                           return Container(child: AuthChoicePage(),);
                         }
                       }
                       return Center(child: CircularProgressIndicator(),);

                    })
            )
        );
    }
}