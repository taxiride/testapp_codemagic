import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart' ;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart'  as geoloc;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/carpooling/find_all_pending_travel_or_booking_request.dart';
import 'package:taxiride_ios/api/find_profil_by_phoneNumber_request.dart';
import 'package:taxiride_ios/api/ride_request/find_geocode_request.dart';
import 'package:taxiride_ios/api/ride_request/find_option_by_city_request.dart';
import 'package:taxiride_ios/api/ride_request/post_rides_request.dart';
import 'package:taxiride_ios/api/ride_request/post_trip_request.dart';
import 'package:taxiride_ios/models/apiresponse/o_s_m_places.dart';
import 'package:taxiride_ios/models/apiresponse/profileresp.dart';
import 'package:taxiride_ios/models/card_detail_trip_args.dart';
import 'package:taxiride_ios/models/matrix.dart';
import 'package:taxiride_ios/models/option_coast.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';
import 'package:taxiride_ios/models/profile.dart';
import 'package:taxiride_ios/models/ride.dart';
import 'package:taxiride_ios/models/trip_responce.dart';

import 'package:taxiride_ios/models/ville.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/providers/rides_provider.dart';
import 'package:taxiride_ios/ridescreens/auth/LandingAuth.dart';
import 'package:taxiride_ios/ridescreens/profil/profile.dart';
import 'package:http/http.dart' as http;
import 'package:taxiride_ios/screens/carpooling/car_pooling_screen.dart';
import 'package:taxiride_ios/screens/carpooling/pooling_driver_waiting_screen.dart';
import 'package:taxiride_ios/screens/profile_infos_screen.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:taxiride_ios/widget/button/secondary_button.dart';
import 'package:taxiride_ios/widget/layout/card_detail_trip.dart';
import 'package:toast/toast.dart';
import 'auth/login.dart';

class  NavigationRide extends StatefulWidget{

    NavigationRide(String role, String origin);

    @override
    _NavigationRideState createState() => _NavigationRideState();

}

Future<Profile> findByPhoneNumber() async {

  final prefs =await SharedPreferences.getInstance();

  var uri =Uri.https('app.taxiride.biz:81', '/api/profile/findbyphonenumber/'+prefs.get('username')+'/fr');
  final http.Response response = await http.get(
    uri,
    headers: <String, String>{
      HttpHeaders.authorizationHeader: 'Bearer '+prefs.get('access_token'),
    },

  );

  log('code'+response.statusCode.toString());

  if(response.statusCode == 200){
    Profile user = Profile.fromJson(json.decode(response.body));

    return user;
  }

}

TextFormField _buildRoundTextField(String hint, t, TextEditingController controller, bool obs){
  return TextFormField(
    validator: (value){
      if(value.isEmpty){
        return 'This field is required';
      }
    },
    controller: controller ,
    decoration: new InputDecoration(
        border: InputBorder.none,
        filled: true,
        hintText: hint,
        fillColor: Colors.white
    ),
    keyboardType: t,
    obscureText: obs,
    cursorColor: Colors.black,
  );
}


RoundedRectangleBorder cardShape(){

  return  RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(15),
        bottomRight: Radius.circular(15),
        topLeft: Radius.circular(15),
        topRight: Radius.circular(15.0)
    ),

  );

}

Profile currentUserProfile;

class _NavigationRideState extends State<NavigationRide>{


    List<OSMPlaces> places;
    Future<Profile> userProfile;
    GoogleMapController mapController;

    LatLng _center = new LatLng(3.8731589, 11.4626538);
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

    geoloc.Location location = new geoloc.Location();

    bool _serviceEnabled;
    geoloc.PermissionStatus _permissionGranted;

    OSMPlaces selectedPlaces;
    bool isOSMPlacesFound = false, isFromPlacesFound = false, isOSMPlacesSelected = false, showRideDetails = false;

    Locale myLocale ;
    int step = 0,solde=500,amount=0;
    bool choix1 = false,choix2=false,choix3=false,valide=true,select=false;
    String adresse_riser,city_rider;
    Ville ville_rider;
    List<OptionCoast> options = [];
    OptionCoast _optionCoast;
    int indice = 0;
    int  idTrip;

    Matrix matrice;

    Position position;
    var geolocator = Geolocator();
    Map<String,dynamic> data = new Map();





    //active les service de localisation et demande la permission pour accéder à la position courrante de l'utilisateur

    void _enableLocationService() async{

      _serviceEnabled = await location.serviceEnabled();
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService();
        if (!_serviceEnabled) {
          return;
        }
      }

      _permissionGranted = await location.hasPermission();
      if (_permissionGranted == geoloc.PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission();
        if (_permissionGranted != geoloc.PermissionStatus.granted) {
          //appel de la fonction pour récupérer la position
          //afichage de la map
            _getCurrentUserPosition();
          return;
        }
      }

    }


    void _getCurrentUserPosition() async{
      final prefs = await SharedPreferences.getInstance();
      if(prefs.getKeys().contains('depart')){
        data['depart'] = prefs.getString('depart');
        data['arriver'] = prefs.getString('arriver');
      }
      
      position = await geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      _getAddressFromLatLng(position);
      print(position.latitude.toString() + ', ' + position.longitude.toString());
      GeocodeRequest geocodeRequest = GeocodeRequest();
      ville_rider =  await geocodeRequest.getNamePosition(position);

      adresse_riser = ville_rider.name;

    }

    _getAddressFromLatLng(Position position) async {
      String currentAddress;
      try {
        List<Placemark> p = await geolocator.placemarkFromCoordinates(
            position.latitude, position.longitude);
        Placemark place = p[0];
        city_rider = place.locality;
        currentAddress = "${place.locality}, ${place.postalCode}, ${place.country}";
        print(currentAddress);

        setState(() {
          currentAddress = "${place.locality}, ${place.postalCode}, ${place.country}";
        });

      } catch (e) {
        print(e);
      }
    }

    void _onMapCreated(GoogleMapController controller){
        mapController = controller;
        _center = LatLng(position.latitude, position.longitude);
    }

    //écoute le changement de position
    void _startListenningPosition(){
      var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);

      StreamSubscription<Position> positionStream = geolocator.getPositionStream(locationOptions).listen((Position position) {
            print(position == null ? 'Unknown' : position.latitude.toString() + ', ' + position.longitude.toString());
            this.position = position;
          });
    }

    @override
    void initState() {
      super.initState();
      userProfile = findByPhoneNumber();
     // _getCurrentLocation();
    //RidesProvider ridesProvider = Provider.of<RidesProvider>(context,listen: false);
    _enableLocationService();
    _getCurrentUserPosition();
    _startListenningPosition();
    getTripId();


    }

    void searchAdress(String adress) async {

    log('adress '+adress);
    print('country '+myLocale.countryCode);
   // GeocodeRequest geocodeRequest = GeocodeRequest();
    //print("---------------------------------- name ----------------------");
   // geocodeRequest.getNamePosition(position);

      final http.Response response = await http.get(
        'https://nominatim.openstreetmap.org/?addressdetails=1&q='+adress+'&format=json&countrycodes=cm&limit=10',
        headers: <String, String>{
          HttpHeaders.userAgentHeader: 'User-Agent: Retrofit-Sample-App',
        },
      );

      print(response.request.url.toString());
      log('code response '+response.statusCode.toString());

      print(response.body);
      if(response.statusCode == 200){
        var rb = response.body;
        // store json data into list
        var list = json.decode(rb) as List;
        // iterate over the list and map each object in list to Img by calling Img.fromJson
        places = list.map((i)=>OSMPlaces.fromJson(i)).toList();
        setState(() {
          isOSMPlacesFound = true;
        });
      }

    }

    Widget _buildOsmResult(){
      return SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 15,bottom: 10,right: 100),
                child: Text("SEARCH RESULTS",style: TextStyle(fontWeight: FontWeight.bold),),

              ),
              Container(
                width: MediaQuery.of(context).size.width*0.8,
                height: MediaQuery.of(context).size.height*0.3,
                child: ListView.builder(
                  padding: EdgeInsets.all(16),
                  itemCount: (places == null)?0:places.length,
                  itemBuilder: (context, i){
                    //if(i.isOdd) return Divider(height: 8,);
                  //  else if(i < places.length)
                      return Column(
                        children: [
                          _buildOSMRow(places[i]),
                          Divider(height: 8,color: Colors.black,)
                        ],
                      );
                     // else return Divider();
                  },
                ),
              ),
            ],
          ),
        ),
      );
    }

    //construit les ligne du resultat de la recherche de OSM
    Widget _buildOSMRow(OSMPlaces osm){

      return ListTile(
            title: Text( osm.displayName, style: TextStyle(fontSize: 14, color: Colors.black, fontFamily: 'Poppins')),
            trailing: Icon(
              Icons.add,
              color: Colors.grey,
            ),
            leading: Image.asset(
                'images/shape_get_started.png'
            ),
        onTap: () async {
              print("--------- calcul de matrix ------------");
              GeocodeRequest geocodeRequest = GeocodeRequest();
          setState(() {
            //permet de masquer le résultat et affiché les détails
           // isOSMPlacesFound = false;
            selectedPlaces = osm;
            isOSMPlacesSelected = true;
          });
           geocodeRequest.getCalculMatrix(position,double.parse(selectedPlaces.lat),double.parse(selectedPlaces.lon),selectedPlaces.displayName,adresse_riser).then((value){
             setState(() {
               matrice = value;
             });
           });
          //double distanceInMetier = await Geolocator().distanceBetween(position.latitude,position.longitude,double.parse(selectedPlaces.lat),double.parse(selectedPlaces.lon));
         // print('------------ matrice detail ---------  '+matrice.distance.toString());
          //on cache tout l'écran et on affice les détails
        },
      );
    }

    //construction des cartes qui vonst afficher les détails de la course
    Widget _buildOSMRideDetails(){
      AuthProvider authProvider = Provider.of<AuthProvider>(context);
      RidesProvider ridesProvider = Provider.of<RidesProvider>(context);
      final ProgressDialog pr = ProgressDialog(context);
      pr.style(
        message: 'Please wait...',
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
      );

      //calcul de la distance et du temps estimatif
      return Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[

              Card(
                shadowColor: Colors.grey,
                shape: cardShape(),
                child: Column(
                  children: <Widget>[

                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 155,top: 20,bottom: 10),
                          child: Row(
                            children: [
                              Image.asset(
                                  'images/shape_get_started.png'
                              ),
                              Container(
                                padding: EdgeInsets.only(right: 5,left: 5),
                                  child: Text('To' , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),))
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                       Container(
                         width: MediaQuery.of(context).size.width*0.7,
                         child: ListTile(
                           title: Text((selectedPlaces != null ? selectedPlaces.displayName : '') , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),maxLines: 1,),
                           trailing: IconButton(icon: Icon(Icons.edit),),
                         ),
                       ),
                      ],
                    ),

                    Divider(height: 5,color: Colors.grey,),

                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 155,top: 20,bottom: 10),
                          child: Row(
                            children: [
                              Icon(Icons.location_on,color: Colors.orangeAccent,),
                              Container(
                                  padding: EdgeInsets.only(right: 5,left: 5),
                                  child: Text('From' , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),)
                              )
                            ],
                          ),
                        ),

                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width*0.7,
                          child: ListTile(
                            title: Text((adresse_riser!=null)?adresse_riser:'' , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins'),maxLines: 1,),
                            trailing: IconButton(icon: Icon(Icons.edit),),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),

              //carte du temps, distance
              // la variable step represente les etape a suivre pour poster, apres la premiere etape les details se cache
              (step < 1)?Container(
                padding: EdgeInsets.only(top: 10,bottom: 10),
                child: Card(
                  child: Column(
                    children: <Widget>[

                      Container(
                        padding: EdgeInsets.all(10),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Text('RIDE DETAILS' , style: TextStyle(fontSize: 10, color: Colors.black54, fontWeight: FontWeight.bold, fontFamily: 'Poppins')),
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.all(15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('RIDE DISTANCE' , style: TextStyle(fontSize: 10, color: Colors.black54,fontWeight: FontWeight.bold ,fontFamily: 'Poppins')),
                                Text( (matrice!=null)?matrice.distance.toString()+' KM':'' , style: TextStyle(fontSize: 24, color: Colors.black54, fontWeight: FontWeight.bold,fontFamily: 'Poppins')),
                              ],
                            ),

                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('ESTIMATED RIDE TIME' , style: TextStyle(fontSize: 10, color: Colors.black54,fontWeight: FontWeight.bold, fontFamily: 'Poppins')),
                                Text((matrice!=null)?matrice.time.toString()+' mins':'' , style: TextStyle(fontSize: 24, color: Colors.black54, fontWeight: FontWeight.bold, fontFamily: 'Poppins')),
                              ],
                            )

                          ],

                        ),
                      ),
                      Container(
                          padding: EdgeInsets.all(20),
                          child: _buildRoundButton(Colors.green, 'Continue', null,authProvider: authProvider),
                          //PrimaryButton(text: 'Continue',onPressed: (){},)
                      )
                      //_buildRoundButton(Colors.green, 'Continue', null),
                    ],
                  ),

                ),
              ):SizedBox(),

              //carte des options s'affice lorque le bouton des détails est clické
              Visibility(
                visible: showRideDetails,
                child: Card(
                  shape: cardShape(),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text('CHOOSE OPTION' , style: TextStyle(fontSize: 10, color: Colors.black54, fontWeight: FontWeight.bold, fontFamily: 'Poppins')),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height*0.25,
                        padding: EdgeInsets.all(5),
                        child: (options.length == 0)?SizedBox(height: 2,):Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(25)),
                                      border: Border.all(width: 2,color: Colors.black),
                                      color: (choix1)?Colors.yellow:null
                                  ),
                                  child: IconButton(icon: Icon(Icons.directions_car), onPressed: (){
                                    if(authProvider.profil.newBalance != null){
                                      solde = authProvider.profil.newBalance.ceil();
                                    }else{ solde = 0;}
                                    setState(() {
                                      _optionCoast = options[0];
                                      choix1=true;choix2=false;choix3=false;
                                      valide = solde>=options[0].tripCost;
                                      amount = options[0].tripCost;
                                      print(indice);
                                    });
                                  }),
                                ),
                                Text(options[0].optionCode,style: TextStyle(color: (choix1)?Colors.orangeAccent:null),),
                                Text("N"+options[0].tripCost.toString())
                              ],
                            ),

                            Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(25)),
                                      border: Border.all(width: 2,color: Colors.black),
                                      color: (choix2)?Colors.yellow:null
                                  ),
                                  child: IconButton(icon: Icon(Icons.directions_car), onPressed: (){
                                    if(authProvider.profil.newBalance != null){
                                      solde = authProvider.profil.newBalance.ceil();
                                    }else{ solde = 0;}
                                    setState(() {
                                      _optionCoast = options[1];
                                      choix2=true;choix1=false;choix3=false;
                                      valide = solde>=options[1].tripCost;
                                      amount = options[1].tripCost;
                                      print(indice);
                                    });
                                  }),
                                ),
                                Text(options[1].optionCode,style: TextStyle(color: (choix1)?Colors.orangeAccent:null),),
                                Text("N"+options[1].tripCost.toString())
                              ],
                            ),
                          ],
                        ),
                      ),
                      Divider(),

                      Container(
                        child: Card(
                          shape: cardShape(),
                          child: Center(
                            child:  Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text('WALLET BALANCE' , style: TextStyle(fontSize: 10, color: (valide)?Colors.black26:Colors.red,fontWeight: FontWeight.bold ,fontFamily: 'Poppins')),
                                Text((authProvider.profil != null && authProvider.profil.newBalance != null)?'N '+authProvider.profil.newBalance.toString():'no value' , style: TextStyle(fontSize: 36, color: (valide)?Colors.black26:Colors.red, fontWeight: FontWeight.bold,fontFamily: 'Poppins')),
                              ],
                            ),

                          ),
                        ),

                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: (valide)?PrimaryButton(text: "POST RIDE",onPressed: (){
                          if(amount > 0){
                            pr.show();

                            Ride _ride = Ride(
                                arrivalPoint: selectedPlaces.displayName,
                                codePays: selectedPlaces.address.countryCode,
                                codeOption: _optionCoast.optionCode,
                                codeVille: city_rider,
                                departurePoint: adresse_riser,
                                tripCost: _optionCoast.tripCost,
                                endPointLatitude: double.parse(selectedPlaces.lat),endPointLongitude: double.parse(selectedPlaces.lon),language: "en",
                                estimatedDuration: matrice.time,estimatedTripDistance: matrice.distance.ceil(), phoneNumber: authProvider.phone,riderLattitude: position.latitude.ceil(),riderLongitude: position.longitude.ceil());

                              ridesProvider.postTrip(authProvider, _ride);

                              pr.hide();
                            Toast.show("You trip has been saved successfully", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                          }else{
                            Toast.show("CHOOSE YOUR OPTION", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                          }


                        },):SecondaryButton(text: 'Invalid Solde',onPressed: (){
                          print(authProvider.profil.newBalance);
                          Toast.show("Recharge your compte", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
                        },),
                      ),

                       //_buildRoundButton(Colors.green, 'POST RIDE', null),

                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      );

    }

    //bouton pour la recherche des places
  RaisedButton _buildRoundButton(Color col, String label, TextEditingController searchC,{AuthProvider authProvider}){
    return RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side:BorderSide(color: col  ),

        ),
        onPressed: () async {
          //on appel osm
        if(label == 'Search')   searchAdress(searchC.text);
        else if(label == 'Continue'){
          print(selectedPlaces.address.countryCode);
          print(city_rider);
          options = await  findOptionCity(authProvider);

          print(options.length);

          setState(() {
            step++;
            print("-------------- step ---------- $step");
            showRideDetails = true;
          });
        }
        },
        color: col,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                margin: const EdgeInsets.only(left: 16),
                child: Center(child: Text( label, style: TextStyle(fontSize: 14, color: Colors.white, fontFamily: 'Poppins')),)
            )
          ],
        )

    );

  }

  //carte pour entrer les lieux et faire des recherches sur osm
  Widget _buildRoundCardForSearchPlaces(String hint, t, TextEditingController controller, bool obs){
    return SingleChildScrollView(
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[

              Visibility(
                visible: !isOSMPlacesFound,
                child:   Card(

                  shadowColor: Colors.grey,
                  shape: cardShape(),
                  child: _buildRoundTextField(hint, t, controller, obs),
                ),
              ),
              Visibility(
                //on a trouvé des lieux qui correspondent à notre recherche
                visible: isOSMPlacesFound && !isOSMPlacesSelected,
                child: Card(
                  shadowColor: Colors.grey,
                  shape: cardShape(),
                  child: _buildOsmResult(),
                ),

              ),

              Visibility(
                //on a trouvé des lieux qui correspondent à notre recherche
                visible: isOSMPlacesSelected,
                child: Card(
                  shadowColor: Colors.grey,
                  shape: cardShape(),
                  child:  _buildOSMRideDetails(),
                ),

              ),
              Visibility(
                visible: !isOSMPlacesFound,
                child: Center(
                  //bouton pour appeler osm
                  child: _buildRoundButton(Colors.green, 'Search', controller,),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

  bool isGetStartedClicked = false;

    @override
    Widget build(BuildContext context) {

      final searchPlaceController = TextEditingController();
      final String searchHint = 'Where are you going to ?';
      myLocale = Localizations.localeOf(context);

      final ProgressDialog pr = ProgressDialog(context);
      pr.style(
        message: 'Please wait...',
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
      );
      // obbject pour avoir tous les informations d un user sans plus apeller les preferences chaque fois
      AuthProvider authProvider = Provider.of<AuthProvider>(context);
      RidesProvider ridesProvider = Provider.of<RidesProvider>(context);
      //ridesProvider.findIdTrip();

      //construit la carte qui s'affiche lorsque le rider est connecté comme rider
      Widget getStartedRider = (idTrip ==0)?RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side:BorderSide(color: Colors.white  ),

          ),
          onPressed: (){
            //quand on clique dessus on rend ce bouton invisible et on affiche la zone de saisie en haut
           // _buildRoundCardForSearchPlaces(searchHint, TextInputType.text, searchPlaceController, false);
            setState((){
              isGetStartedClicked = true;
            });
          },
          color: Colors.white,
          child: Container(
            width: 342,
            height: 47,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                    'images/shape_get_started.png'
                ),
                Container(
                    margin: const EdgeInsets.only(left: 16),
                    child: Center(child: Text(searchHint , style: TextStyle(fontSize: 14, color: Colors.black54, fontFamily: 'Poppins')),)
                )
              ],
            ),
          )

      ):Padding(
        padding: const EdgeInsets.only(top: 100),
        child: CardDetailTrip(
          args: CardDetailTripArgs(
              depart: (data.containsKey('depart'))?data['depart']:'',
              arriv: (data.containsKey('arriver'))?data['arriver']:'',
              isValided: (data.containsKey('driver_check'))?data['driver_check']:false),
              
              ),
      );

        void navigateToProfile(Profil profil){
          authProvider.setProfil(profil);
            Navigator.push(
                context,
               // MaterialPageRoute(builder: (context)=>UserProfile('','')
                 MaterialPageRoute(builder: (context)=>ProfileInfosScreen(profile: profil,)
                )
            );
        }

        void logout() async {
            final prefs = await SharedPreferences.getInstance();
            authProvider.logout();

            await prefs.setBool('is_connected', false);
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AuthChoicePage()));
        }

        return MaterialApp(
            home:   Scaffold(
                key: _scaffoldKey,
                body:Stack(
                    children: <Widget>[
                        GoogleMap(
                            onMapCreated: _onMapCreated,
                            initialCameraPosition: CameraPosition(
                                target: _center,
                                zoom: 13.0
                            ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Align(
                                alignment: Alignment.topLeft,
                                child: Container(
                                    margin: const EdgeInsets.only(top: 16),
                                    child: FloatingActionButton(
                                        onPressed: ()=>_scaffoldKey.currentState.openDrawer(),
                                        materialTapTargetSize: MaterialTapTargetSize.padded,
                                        backgroundColor: Colors.amber,
                                        child: const Icon(Icons.dehaze, size: 36.0, color: Colors.black,),
                                    ),
                                ),
                            ),
                        ),
                      Visibility(
                        visible: !isGetStartedClicked,
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            margin: const EdgeInsets.all(32.0),
                            child: getStartedRider,
                          ),
                        ),
                      ),
                 Visibility(
                        visible:isGetStartedClicked,
                        child:Container(
                          margin: const EdgeInsets.only(top: 80),
                          child: _buildRoundCardForSearchPlaces(searchHint, TextInputType.text, searchPlaceController, false),
                        )
                      ),

                    ],
                ),
                drawer: Drawer(
                    child: ListView(
                        padding: EdgeInsets.zero,
                        children: <Widget>[
                          FutureBuilder<Profile>(
                            future: userProfile,
                            builder: (context, snapshot){
                              if(snapshot.hasData){
                                currentUserProfile = snapshot.data;
                                return  DrawerHeader(
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 16),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        if( snapshot.data.imageUrl != null)
                                        Container(
                                          width: 75,
                                          height: 75,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                              image: NetworkImage('https://app.taxiride.biz:81/api/commons/download/'+snapshot.data.imageUrl),
                                              fit: BoxFit.fill,
                                            )
                                          ),
                                        )
                                        else Container(
                                            width: 75,
                                            height: 75,
                                            decoration: BoxDecoration(
                                                color: Colors.yellow,
                                                shape: BoxShape.circle
                                            ),
                                            child: Icon(  Icons.person)),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Text('Hi,', style: TextStyle(fontSize: 12, fontFamily: 'Poppins'),),
                                            Text((snapshot.data.firstName != null)?snapshot.data.firstName:"No first name valid", style: TextStyle(fontSize: 12, fontFamily: 'Poppins'),)
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              }
                              return CircularProgressIndicator();
                            },
                          ),

                            ListTile(
                                title:  Text('Profile', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                                onTap: () async {

                                  final prefs =await SharedPreferences.getInstance();
                                  String token = prefs.getString('access_token');
                                  String username = prefs.getString('username');
                                  FindProfileByPhoneNumber profile = FindProfileByPhoneNumber(token,username);
                                  profile.findProfilByPhone().then((value){
                                    print(value.firstName);
                                    navigateToProfile(value);

                                  });
//                            userProfile.then((value){
//                                    print(value.firstName);
//                                    navigateToProfile(value);
//
//                                  });

                                },
                            ),
                            ListTile(
                                title:  Text('Payments', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                                onTap: (){
                                    Navigator.pop(context);
                                },
                            ),
                            ListTile(
                                title:  Text('Stats', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                                onTap: (){
                                    Navigator.pop(context);
                                },
                            ),
                            ListTile(
                                title:  Text('Promo code', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                                onTap: (){
                                    Navigator.pop(context);
                                },
                            ),
                            ListTile(
                                title:  Text('Settings', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                                onTap: (){
                                    Navigator.pop(context);
                                },
                            ),
                            ListTile(
                                title:  Text('Logout', style: TextStyle(fontSize: 14, fontFamily: 'Poppins'),) ,
                                onTap: (){
                                    logout();
                                    Navigator.pop(context);
                                },
                            ),
                            Container(
                                padding: EdgeInsets.all(15),
                                child: BlackButton(text: "Carpooling",onPressed: () async {
                                  pr.show();
                                  final prefs = await SharedPreferences.getInstance();
                                  int role = await prefs.getInt('role');
                                  String mytoken = await  prefs.getString('access_token');
                                  String phone = await prefs.getString("username");
                                  FindAllPendingTravelOrBookingRequest findallPendingBooking = FindAllPendingTravelOrBookingRequest(mytoken,phone,"fr");
                                  List<PoolingTrip> poolinTrips = await findallPendingBooking.parseResult(await ApiClient.execOrFail(findallPendingBooking));

                                  if(poolinTrips.length > 0){
                                    print("tout c est bien passe========");
                                    pr.hide();
                                     Navigator.push(context, MaterialPageRoute(builder: (context)=> PoolingDriverWaitingScreen(listPoolings: poolinTrips,role: "rider",)));
                                  }else{
                                    pr.hide();
                                    print("pas d element trouve");
                                     Navigator.push(context, MaterialPageRoute(builder: (context)=>CarPoolingScreen(role: (role==0)?"rider":"driver",)));
                                  }



                                },)
                                //_buildRoundButton(Colors.black, 'Carpooling',null,),
                            )
                        ],
                    ),
                ),
            ),
        );

    }

    post_ride(Ride ride,AuthProvider authProvider) async {
      PostTripRequest postTripRequest = PostTripRequest(ride,authProvider.token);
      await ApiClient.execOrFail(postTripRequest);

    }

    getTripId() async {
      final prefs = await SharedPreferences.getInstance();
      if (prefs.getKeys().contains('id_trip')) {
        idTrip = prefs.getInt('id_trip');
      } else {
        // ici il n a plus de trip en cours
        idTrip = 0;
      }
    }

    findOptionCity(AuthProvider authProvider) async {
      print(authProvider.token);
      FindOptionByCityRequest findOptionByCityRequest = FindOptionByCityRequest(city_rider,selectedPlaces.address.countryCode,matrice.distance.ceil(),matrice.time,"en",authProvider.token);
      return findOptionByCityRequest.parseResult(await ApiClient.execOrFail(findOptionByCityRequest));
    }
    saveRide(Ride ride,AuthProvider authProvider) async {

      final http.Response response = await http.post(
        'https://app.taxiride.biz:81/api/rides/postTrip',
        headers: <String,String>{
          'Authorization': 'Bearer '+authProvider.token,
          'Content-Type':'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String,dynamic>{
          "arrivalPoint": ride.arrivalPoint,
          "availableCashAmount": ride.availableCashAmount,
          "codeOption": ride.codeOption,
          "codePays": ride.codePays,
          "codeVille": ride.codeVille,
          "departurePoint": ride.departurePoint,
          "endPointLatitude": ride.endPointLatitude,
          "endPointLongitude": ride.endPointLongitude,
          "estimatedDuration": ride.estimatedDuration,
          "estimatedTripDistance": ride.estimatedTripDistance,
          "estimatedTripLength": ride.estimatedTripLength,
          "id": ride.id,
          "language": ride.language,
          "phoneNumber": ride.phoneNumber,
          "postDate": ride.postDate,
          "rayon": ride.rayon,
          "riderLattitude": ride.riderLattitude,
          "riderLongitude": ride.riderLongitude,
          "seatNumber": ride.seatNumber,
          "startPointLatitude": ride.startPointLatitude,
          "startPointLongitude": ride.startPointLongitude,
          "tripCost": ride.tripCost,
          "tripTimeOut": ride.tripTimeOut

        }),
      );

      if(response.statusCode == 200){
        print(response.body);
        print("--------------------- save succeffull ----------------------");


      }else{
        print("--------------------- save fail fail ----------------------");
        print(response.body);
        print(response.statusCode);
        print(ride.language);

      }


    }


}