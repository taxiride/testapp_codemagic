import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/vehicule.dart';

class SaveCabProfilInformation extends AbstractAuthenticatedRequest{
  final Vehicule vehicule;
  final String token;

  SaveCabProfilInformation(this.vehicule,this.token) : super(token);

  @override
  String getUrl() {
    return "/api/profile/saveProfileCabInformation";

  }

  @override
  String getVerb() {
    return "POST";
  }

  @override
  getBody() {

    return {
      "brand": vehicule.brand,
      "carconstructor": vehicule.carconstructor,
      "cityScope": vehicule.cityScope ?? "",
      "color": vehicule.color ?? "",
      "description": vehicule.description ?? "",
      "firstUseDate": vehicule.firstUseDate ?? "",
      "id": vehicule.id ?? 0,
      "language": vehicule.language??"",
      "matriculationNumber": vehicule.matriculationNumber??"",
      "model": vehicule.model??"",
      "numberOfSeat": vehicule.numberOfSea ?? 0,
      "numbersWheel": vehicule.numbersWheel ?? 0,
      "phoneNumber": vehicule.phoneNumber??"",
      "travelOption": vehicule.travelOption??"",
      "userId": vehicule.userId??"",
      "vehicleType": vehicule.vehicleType??""

    };

  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    print("------------------- voici le statut code ------------------------");
    print(response.httpResponse.statusCode);
    print(data);
  }

}