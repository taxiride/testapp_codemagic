/// place_id : 25091847
/// licence : "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright"
/// osm_type : "node"
/// osm_id : 2467973346
/// boundingbox : ["4.2517683","4.2917683","11.3145541","11.3545541"]
/// lat : "4.2717683"
/// lon : "11.3345541"
/// display_name : "Emana, Lekié, Centre, Cameroon;Cameroun"
/// class : "place"
/// type : "village"
/// importance : 0.375
/// icon : "https://nominatim.openstreetmap.org/images/mapicons/poi_place_village.p.20.png"
/// address : {"village":"Emana","county":"Lekié","state":"Centre","country":"Cameroon;Cameroun","country_code":"cm"}

class OSMPlaces {
  int _placeId;
  String _licence;
  String _osmType;
  int _osmId;
  List<String> _boundingbox;
  String _lat;
  String _lon;
  String _displayName;
  String _type;
  double _importance;
  String _icon;
  Address _address;

  int get placeId => _placeId;
  String get licence => _licence;
  String get osmType => _osmType;
  int get osmId => _osmId;
  List<String> get boundingbox => _boundingbox;
  String get lat => _lat;
  String get lon => _lon;
  String get displayName => _displayName;
  String get type => _type;
  double get importance => _importance;
  String get icon => _icon;
  Address get address => _address;

  OSMPlaces({
      int placeId, 
      String licence, 
      String osmType, 
      int osmId, 
      List<String> boundingbox, 
      String lat, 
      String lon, 
      String displayName,
      String type, 
      double importance, 
      String icon, 
      Address address}){
    _placeId = placeId;
    _licence = licence;
    _osmType = osmType;
    _osmId = osmId;
    _boundingbox = boundingbox;
    _lat = lat;
    _lon = lon;
    _displayName = displayName;
    _type = type;
    _importance = importance;
    _icon = icon;
    _address = address;
}

  factory OSMPlaces.fromJson(Map<String, dynamic> json) {
   return OSMPlaces(
       placeId : json["place_id"],
       licence : json["licence"],
       osmType : json["osm_type"],
   osmId : json["osm_id"],
   boundingbox : json["boundingbox"] != null ? json["boundingbox"].cast<String>() : [],
    lat : json["lat"],
    lon : json["lon"],
    displayName : json["display_name"],
    type : json["type"],
    importance : json["importance"],
    icon : json["icon"],
    address : json["address"] != null ? Address.fromJson(json["address"]) : null,
   );
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["place_id"] = _placeId;
    map["licence"] = _licence;
    map["osm_type"] = _osmType;
    map["osm_id"] = _osmId;
    map["boundingbox"] = _boundingbox;
    map["lat"] = _lat;
    map["lon"] = _lon;
    map["display_name"] = _displayName;
    map["type"] = _type;
    map["importance"] = _importance;
    map["icon"] = _icon;
    if (_address != null) {
      map["address"] = _address.toJson();
    }
    return map;
  }

}

/// village : "Emana"
/// county : "Lekié"
/// state : "Centre"
/// country : "Cameroon;Cameroun"
/// country_code : "cm"

class Address {
  String _village;
  String _county;
  String _state;
  String _country;
  String _countryCode;

  String get village => _village;
  String get county => _county;
  String get state => _state;
  String get country => _country;
  String get countryCode => _countryCode;

  Address({
      String village, 
      String county, 
      String state, 
      String country, 
      String countryCode}){
    _village = village;
    _county = county;
    _state = state;
    _country = country;
    _countryCode = countryCode;
}

  Address.fromJson(Map<String, dynamic> json) {
    _village = json["village"];
    _county = json["county"];
    _state = json["state"];
    _country = json["country"];
    _countryCode = json["country_code"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["village"] = _village;
    map["county"] = _county;
    map["state"] = _state;
    map["country"] = _country;
    map["country_code"] = _countryCode;
    return map;
  }

}