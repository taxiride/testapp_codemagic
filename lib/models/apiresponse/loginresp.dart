class LoginResponse{
  final String accessToken;
  final String refreshToken;
  final String username;
  // final List<String> autorities;
  LoginResponse({this.accessToken, this.refreshToken, this.username});

  factory LoginResponse.fromJson(Map<String, dynamic> json){
    return LoginResponse(
      accessToken: json['accessToken'],
      refreshToken: json['refreshToken'],
      username: json['username'],
      // autorities: json['authorities']
    );
  }

}