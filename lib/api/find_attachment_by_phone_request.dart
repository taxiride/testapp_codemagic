

import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/models/file_attachment.dart';

import 'abstract/AbstractAuthenticatedRequest.dart';

class FindAttachmentByPhone extends AbstractAuthenticatedRequest{
  final String token;
  final String language;
  final String phoneNumber;

  FindAttachmentByPhone(this.token, this.language, this.phoneNumber):super(token);


  @override
  String getUrl() {
   return "/api/profile/findAttachementsByPhone/$phoneNumber/$language";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  Map<String, String> getParams() {
    return {
      "language":language,
      "phone":phoneNumber

    };
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    return FileAttachment.listFromJson(jsonDecode(data));
  }

}