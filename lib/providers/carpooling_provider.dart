import 'package:flutter/cupertino.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/carpooling/find_all_travels_conditions_request.dart';
import 'package:taxiride_ios/api/carpooling/find_arrival_cities_request.dart';
import 'package:taxiride_ios/api/commons/find_location_request.dart';
import 'package:taxiride_ios/models/condition_travel.dart';
import 'package:taxiride_ios/models/location.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';

class CarPoolingProvider extends ChangeNotifier{
  Location location;
  List<PoolingTrip> poolingTrips;


  findArrivalCitie(String token,String countryCode,String departure,String language) async {
    FindArrivalCitiesRequest findArrivalCitiesRequest = FindArrivalCitiesRequest(countryCode,departure,language,token);
    List<PoolingTrip> poolings = await findArrivalCitiesRequest.parseResult( await ApiClient.execOrFail(findArrivalCitiesRequest));
    poolingTrips = poolings;
    notifyListeners();
    return poolings;
  }

  getLocation(AuthProvider authProvider) async {
    FindLoctionRequest findLoctionRequest = FindLoctionRequest(authProvider.token);
    Location position = await findLoctionRequest.parseResult(await ApiClient.execOrFail(findLoctionRequest));
   location = position;
   notifyListeners();
    return location;
  }

  getConditionsTravel(String token,String language) async {
   FindAllTravelsConditions findAllTravelsConditions = FindAllTravelsConditions(token,language);
   List<ConditionTravel> conditions = await findAllTravelsConditions.parseResult(await ApiClient.execOrFail(findAllTravelsConditions));
   return conditions;
  }





}