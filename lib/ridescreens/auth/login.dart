import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:taxiride_ios/Utils/Prefs/Preferences.dart';
import 'package:taxiride_ios/api/login_request.dart';
import 'package:taxiride_ios/models/apiresponse/loginresp.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/ridescreens/auth/register.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/Utils/Prefs/Preferences.dart';
import 'package:taxiride_ios/screens/select_role_screen.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget{
  LoginPage();

  @override
  _LoginPageState createState() => _LoginPageState();

}

class _LoginPageState extends State<LoginPage>{

  Card _buildRoundCardForTextField(String hint, t, TextEditingController controller, bool obs){
    return Card(
      
    elevation: 5,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(15),
        bottomRight: Radius.circular(15),
        topLeft: Radius.circular(15),
        topRight: Radius.circular(15.0)
      ),

    ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _buildRoundTextField(hint, t, controller, obs)
          ],
        ),
      ),
    );
  }

//  TextFormField _buildRoundTextField(String hint, t){
//    return TextFormField(
//      validator: (value){
//        if(value.isEmpty){
//          return 'This field is required';
//        }
//      },
//      decoration: new InputDecoration(
//          border: new OutlineInputBorder(
//              borderRadius: const BorderRadius.all(
//                  const Radius.circular(18.0)
//              )
//          ),
//          filled: true,
//          hintText: hint,
//          fillColor: Colors.white
//      ),
//      keyboardType: t,
//      cursorColor: Colors.black,
//    );
//  }

  TextFormField _buildRoundTextField(String hint, t, TextEditingController controller, bool obs){
    return TextFormField(
      validator: (value){
        if(value.isEmpty){
          return 'This field is required';
        }
      },
      controller: controller ,
    decoration: new InputDecoration(
      border: InputBorder.none,
          filled: true,
          hintText: hint,
          fillColor: Colors.white
      ),
      keyboardType: t,
      obscureText: obs,
      cursorColor: Colors.black,
    );
  }

  @override
  Widget build(BuildContext context) {

    final _loginkey = GlobalKey<FormState>();
    final emailorphonecontroller  = TextEditingController();
    final passcontroller  = TextEditingController();
    final ProgressDialog pr = ProgressDialog(context);
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

    //ouvre la page principale quand le login est ok
    void _navivgateToMain(){
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => NavigationRide('rider', 'login' )));
    }

    //appel l'api pour le login
    Future<LoginResponse> callLogin(String emailorphone, String password) async {
      await pr.show();
      
      final http.Response response = await http.post(
        'https://app.taxiride.biz:81/api/auth/login',
        headers: <String, String>{
          'Content-Type':'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'password': password,
          'username':emailorphone

        }),
      );

      log('code '+ response.statusCode.toString());

      await pr.hide();

      final prefs = await SharedPreferences.getInstance();

      if(response.statusCode == 200){
       // print(jsonDecode(response.body));
        LoginResponse resp = LoginResponse.fromJson(json.decode(response.body)["user"]);
        print("-----------login-------------");
        print(resp.username);
        print(resp.accessToken);
       await prefs.setString('username', resp.username);
       await  prefs.setString('access_token', resp.accessToken);
       await prefs.setBool('is_connected', true);
       authProvider.setInfosUser(resp.username, resp.accessToken);
       //_navivgateToMain();
        Toast.show("Connection Succefull", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
        Navigator.push(context, MaterialPageRoute(builder: (context) => SelectRoleScreen()));
       print(resp);

        return resp;
      }else{
        Toast.show("Connection Lost", context,duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);
        throw Exception('Error');
      }

    }

    //bouton pour le login gère aussi le click
    RaisedButton _buildRoundButton(Color col, IconData icon, String label){
      return RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side:BorderSide(color: col  ),

          ),
          onPressed: (){

           callLogin(emailorphonecontroller.text, passcontroller.text);

          },
          color: col,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  margin: const EdgeInsets.only(left: 16),
                  child: Center(child: Text( label, style: TextStyle(fontSize: 14, color: Colors.white, fontFamily: 'Poppins')),)
              )
            ],
          )

      );

    }

    //colone des edittext
    Widget roundTextFiel = Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 60, left: 16, right: 16),
            child:
            _buildRoundCardForTextField('Phone number', TextInputType.phone, emailorphonecontroller, false),
          ),
          Container(
            margin: const EdgeInsets.only(top: 25, bottom: 25, left: 16, right: 16),
            child: _buildRoundCardForTextField('Password', TextInputType.visiblePassword,passcontroller, true),
          )
        ],
      ),
    );

    //ouvre le register
    void _navivgateToRegister(){
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => RegisterPage()));
    }

    //construit la page principale du login
    return Scaffold(
      body:SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
             children: <Widget>[
               Image.asset('images/ellipse_auth.png',
                 fit: BoxFit.cover,
               ),
               Align(
                 alignment: Alignment.topLeft,
                 child: Container(
                   margin: const EdgeInsets.only(top: 64),
                   child: Icon(
                       Icons.chevron_left,
                     color: Colors.white,
                   ),
                 ),
               ),
               Align(
                 alignment: Alignment.topCenter,
                 child:  Container(
                   margin: const EdgeInsets.only(top: 64),
                   child: Text('Login', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35, fontFamily: 'Poppins'),
                   ),
                 ),
               ),

             ],
            ),
            Center(
              child: Form(
                key: _loginkey,
                child: Column (
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    roundTextFiel,
                    //_buildRoundButton(Colors.green, null, 'Login')
                    PrimaryButton(
                      text: 'Login',
                      onPressed: (){
                       callLogin(emailorphonecontroller.text, passcontroller.text);
                        //authProvider.login(passcontroller.text, emailorphonecontroller.text);
                      },
                    ),
                  ],
                ),
              ),
            ),

            Align(
              alignment: Alignment.bottomCenter,
              child:   GestureDetector(
                onTap: _navivgateToRegister,
                child: Container(
                  margin: const EdgeInsets.only(top: 25, right: 16, bottom: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text('Go to register', ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );

  }

  setInfosUser(LoginResponse resp) async {
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('username', resp.username);
      await  prefs.setString('access_token', resp.accessToken);
      await prefs.setBool('is_connected', true);

  }


}