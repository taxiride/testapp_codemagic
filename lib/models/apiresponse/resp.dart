class Resp{
  final  String data;
  final bool success;

  Resp({this.data,this.success});

  factory Resp.fromJson(Map<String, dynamic> json){
    return Resp(
      data: json['data'],
      success: json['success']
    );
  }
}