import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/image_car.dart';

class FindCabImagesByPhoneRequest extends AbstractAuthenticatedRequest{
  final String token;
  final String language;
  final String phone;

  FindCabImagesByPhoneRequest(this.token, this.language, this.phone):super(token);

  @override
  String getUrl() {
   return "/api/profile/findCabImagesByPhone/$phone/$language";
  }

  @override
  String getVerb() {
   return "GET";

  }

  @override
  Map<String, String> getParams() {
    return {
      "language":language,
      "phone":phone
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    return ImageCar.listFromJson(jsonDecode(data));

  }


}