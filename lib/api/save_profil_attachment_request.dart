import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

class SaveProfilAttachmentRequest {

  Future<Map<String, dynamic>> uploadImage(File image,String token,String phoneNumber,int typedocument) async {
    // String url = "https://app.taxiride.biz:81/api/profile/saveProfileCabImage";
    String url = "https://app.taxiride.biz:81/api/profile/saveProfileAttachements";
    final Uri uri = Uri.parse(url);
    final String prefix = "";
    Uri uri2 = Uri.https("app.taxiride.biz:81", prefix +"/api/profile/saveProfileCabImage");
    Map<String, String> headers = {
      'Authorization':'Bearer ' + token};

    print('Bearer ' + token);
    final http.MultipartRequest request = http.MultipartRequest("POST", uri);
    request.headers['Authorization'] ='Bearer ' + token ;
    final file = await http.MultipartFile.fromPath('file',image.path);
    request.files.add(file);
    request.fields['language'] = 'en';
    request.fields['phoneNumber'] = phoneNumber;
    //request.fields['pictureNumber'] = pictureNumber.toString();
    request.fields['number'] = typedocument.toString();

    final http.StreamedResponse response = await request.send();
    final rep = await http.Response.fromStream(response);
    print('statusCode => ${response.statusCode}');
    print(rep.body);

    if(response.statusCode != 200 ){
     // Toast.show("error Save Image",duration: Toast.LENGTH_LONG, gravity:  Toast.TOP);

      return null;
    }else{
    //  Toast.show("Connection Succefull", context,duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
    }
    final Map<String,dynamic> responseData = json.decode(rep.body);

    return responseData;

  }



}