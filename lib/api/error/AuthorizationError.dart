

import 'package:taxiride_ios/api/abstract/AbstractApiError.dart';
import 'package:taxiride_ios/api/error/ApiErrorType.dart';

class AuthorizationError extends AbstractApiError {
  String message;
  AuthorizationError(this.message)
      : super(3, message, ApiErrorType.authorisationError);

}