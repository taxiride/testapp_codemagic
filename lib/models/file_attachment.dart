class FileAttachment{
  int id;
  String phoneNumber;
  int userId;
  String url;
  int number;
  String mimeType;

  FileAttachment({this.id,this.phoneNumber,this.userId,this.url,this.number,this.mimeType});

  FileAttachment.fromJson(Map<String, dynamic> json){

    id = json['id'];
    phoneNumber = json['phoneNumber'];
    userId = json['userId'];
    url = json['url'];
    number = json['number'];
    mimeType = json['mimeType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['phoneNumber'] = this.phoneNumber;
    data['userId'] = this.userId;
    data['url'] = this.url;
    data['number'] = this.number;
    data['mimeType'] = this.mimeType;

  }

  static List<FileAttachment> listFromJson(List<dynamic> json) {
    return List<FileAttachment>.from(json.map((v) => FileAttachment.fromJson(v)));
  }
}