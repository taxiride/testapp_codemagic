class Profile{

final String email;
final String firstName;
final String lastName;
final String imageUrl;
final bool  isEmailVerified;
final bool isPhoneVerified;
final bool isVerified;
final String phoneNumber;
final String profession;
final String address;
final String language;
final String city;
final String gender;
final String referalCode;
final String country;
final double newBalance;
final String defaultTravelOption;
final String defaultRequestRadius;

Profile({this.email,this.address,this.city,this.newBalance,this.country,this.firstName,this.gender,this.imageUrl,this.isEmailVerified,this.isPhoneVerified,
this.isVerified,this.language,this.lastName,this.phoneNumber,this.profession,this.referalCode, this.defaultRequestRadius,this.defaultTravelOption});

factory Profile.fromJson(Map<String, dynamic> json){

  return Profile(
  address: json['address'],
  city: json['city'],
    country: json['country'],
    email: json['email'],
    firstName: json['firstName'],
    newBalance:json['newBalance'],
    gender: json['gender'],
    imageUrl: json['imageUrl'],
    isEmailVerified: json['isEmailVerified'],
    isPhoneVerified: json['isPhoneVerified'],
    isVerified: json['isVerified'],
    language: json['language'],
    lastName: json['lastName'],
    phoneNumber: json['phoneNumber'],
    profession: json['profession'],
    referalCode: json['referalCode'],
    defaultRequestRadius: json['defaultRequestRadius'],
    defaultTravelOption: json['defaultTravelOption'],
  );
}

}