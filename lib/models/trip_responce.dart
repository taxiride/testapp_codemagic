class TripResponce{
  double pickUpLatitude;
  double pickUpLongitude;
  double arrivalLongitude;
  double arrivalLatitude;
  String arrivalAddress;
  String departureAddress;
  double tripCost;

  TripResponce({this.pickUpLatitude,this.pickUpLongitude,this.arrivalLongitude,this.arrivalLatitude,this.arrivalAddress,this.departureAddress,this.tripCost});

  TripResponce.fromJson(Map<String, dynamic> json){
    pickUpLatitude = json['pickUpLatitude'];
    pickUpLongitude = json['pickUpLongitude'];
    arrivalLongitude = json['arrivalLongitude'];
    arrivalLatitude = json['arrivalLatitude'];
    arrivalAddress = json['arrivalAddress'];
    departureAddress = json['departureAddress'];
    tripCost = json['tripCost'];
  }
}