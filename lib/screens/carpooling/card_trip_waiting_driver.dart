import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/carpooling/find_all_distance_trip_request.dart';
import 'package:taxiride_ios/api/carpooling/find_all_long_distance_trip_booking_request.dart';
import 'package:taxiride_ios/api/carpooling/find_all_user_panding_invitation_request.dart';
import 'package:taxiride_ios/models/passager.dart';
import 'package:taxiride_ios/models/pooling_trip.dart';
import 'package:taxiride_ios/models/taximen_car.dart';
import 'package:taxiride_ios/providers/auth_provider.dart';
import 'package:taxiride_ios/ridescreens/navigation.dart';
import 'package:taxiride_ios/screens/carpooling/find_car_screen.dart';
import 'package:taxiride_ios/screens/carpooling/trip_invitation_screen.dart';
import 'package:taxiride_ios/widget/button/black_button.dart';
import 'package:taxiride_ios/widget/button/primary_button.dart';
import 'package:taxiride_ios/widget/button/secondary_button.dart';

import 'find_passagers_screen.dart';

class CardTripWaitingDriver extends StatelessWidget{
 final PoolingTrip poolingTrip;

  const CardTripWaitingDriver({Key key, this.poolingTrip}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    final ProgressDialog pr = ProgressDialog(context);
    pr.style(
      message: 'Please wait...',
      backgroundColor: Colors.white,
      progressWidget: CircularProgressIndicator(),
      elevation: 10.0,
    );

   return Column(
      children: [
        Card(
          shadowColor: Colors.grey,
          shape: cardShape(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,

            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8,left: 2),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text("PENDING TRIP",style: TextStyle(fontWeight: FontWeight.bold,),),),
                      IconButton(icon: Icon(Icons.close,color: Colors.red,), onPressed: (){
                        _showDialogCancelTrip(context);

                      })
                    ],
                  ),
                ),
              ),
              Padding(
                padding:const EdgeInsets.all(8.0),
                child: Container(
                  width: MediaQuery.of(context).size.width*0.9,
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(Icons.near_me,color: Colors.orange,),
                        title: Text("To",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                        subtitle: Text(this.poolingTrip.arrivalCity),
                        trailing: Icon(Icons.mode_edit),
                        onTap: (){

                        },
                      ),
                      Divider(height: 8,color: Colors.black,),
                      ListTile(
                        leading: Icon(Icons.place,color: Colors.orange,),
                        title: Text("From",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                        subtitle: Text((this.poolingTrip.departureCity==null)?"start city not found":this.poolingTrip.departureCity),
                        trailing: Icon(Icons.mode_edit),
                        onTap: (){

                        },
                      ),

                      Divider(height: 8,color: Colors.black,),
                      ListTile(
                        leading: Icon(Icons.place,color: Colors.orange,),
                        title: Text("Date/Time",style: TextStyle(fontSize: 10,color: Colors.blueGrey),),
                        subtitle: Text(this.poolingTrip.travelDate,maxLines: 1,),
                        trailing: Icon(Icons.mode_edit),
                        onTap: (){

                        },
                      ),

                      Divider(height: 8,color: Colors.black,),

                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width*0.8,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: PrimaryButton( child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              color: Colors.black,
                              shape: BoxShape.circle
                          ),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Center(child: Text("3",style: TextStyle(color: Colors.white),)),
                          )
                      ),
                      Container(
                        child: Text("TRIP REQUEST",style: TextStyle(color: Colors.white),),
                      )
                    ],
                  ),
                    onPressed: () async {
                      pr.show();
                      String mytoken = await authProvider.tokenKey;
                      final prefs = await SharedPreferences.getInstance();
                      String myphone = await prefs.getString('username');

                      //FindAllLongDistanceTripRequest findLongdistancetrip = FindAllLongDistanceTripRequest(mytoken,ville,"fr",current_position.city,date);
                      //List<dynamic> trips =  await findLongdistancetrip.parseResult(await ApiClient.execOrFail(findLongdistancetrip));
                      FindAllUserPandingInvitationRequest findallUser = FindAllUserPandingInvitationRequest(mytoken,myphone,"fr",this.poolingTrip.id);
                      List<dynamic> trips = await findallUser.parseResult(await ApiClient.execOrFail(findallUser));
                      pr.hide();
                      Navigator.push(context, MaterialPageRoute( builder: (_) => TripInvitation(tripsDispo: trips,)));

                    },),
                ),
              )

            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width*0.7,
          child: BlackButton(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(Icons.near_me,color: Colors.orange,),
                Text("FIND PASSAGERS",style: TextStyle(color: Colors.white),)
              ],
            ),
            onPressed: () async {

              pr.show();
              String mytoken = await authProvider.tokenKey;
              String newDate = this.poolingTrip.travelDate.split("T")[0];
              FindAllLongDistanceTripBookingRequest findLongdistancetrip = FindAllLongDistanceTripBookingRequest(mytoken,0,this.poolingTrip.arrivalCity,"fr",this.poolingTrip.departureCity,newDate);//FindAllLongDistanceTripRequest(mytoken,ville,"fr",current_position.city,date);
              List<Passager> passagers =  await findLongdistancetrip.parseResult(await ApiClient.execOrFail(findLongdistancetrip));
              pr.hide();
              Navigator.push(context, MaterialPageRoute( builder: (_) => FindPassagerScreen(passagers: passagers,)));

            },
          ),
        )
      ],
    ) ;
  }


 void _showDialogCancelTrip(BuildContext mycontext) {
   // flutter defined function
   showDialog(
     context: mycontext,
     builder: (BuildContext context) {

       final ProgressDialog pr = ProgressDialog(context);
       pr.style(
         message: 'Please wait...',
         backgroundColor: Colors.white,
         progressWidget: CircularProgressIndicator(),
         elevation: 10.0,
       );

       // return object of type Dialog
       return AlertDialog(
         title: Center(child: Icon(Icons.info,color: Colors.red,),),
         content: SizedBox(),
         actions: <Widget>[
           // usually buttons at the bottom of the dialog
           Column(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: [
               Center(
                 child: Column(
                   children: [
                     Text("Are you sure you want",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                     Text("to cancel the trip ?",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)
                   ],
                 ),
               ),
               Container(
                 padding: EdgeInsets.all(20),
                 width: MediaQuery.of(context).size.width*0.9,
                 child: Center(
                   child: Column(
                     children: [
                       Text("The Driver is already an this waw"),
                       Text("You will be changed")
                     ],
                   ),
                 ),

               ),

               Container(
                 padding: EdgeInsets.only(top: 10,bottom: 30),
                 child: Center(
                   child: Text("N 100",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                 ),
               ),

               Padding(
                 padding: const EdgeInsets.all(8.0),
                 child: Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: [
                     Container(
                       width: MediaQuery.of(context).size.width*0.3,
                       child: PrimaryButton(text: 'GO BACK',onPressed: (){
                         Navigator.of(context).pop();

                       },),
                     ),
                     Container(
                         width: MediaQuery.of(context).size.width*0.3,
                         child: SecondaryButton(text: 'YES CANCEL',onPressed: () async {
                           await pr.show();

                           await pr.hide();
                           Navigator.of(context).pop();},))
                   ],
                 ),
               )
             ],
           ),

         ],
       );
     },
   );
 }

}