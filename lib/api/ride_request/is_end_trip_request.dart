import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';

class IsEndTripRequest extends AbstractAuthenticatedRequest{
  final String token;
  final int id;

  IsEndTripRequest(this.token,this.id) : super(token);

  @override
  String getUrl() {
    return "/api/rides/isEndTrip/$id/fr";
  }

  @override
  String getVerb() {
   return "GET";
  }

  @override
  Map<String, String> getParams() {
    return {
      "language":"fr",
      "tripId":id.toString()
    };
  }

  @override
  parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    print(data);
    print(response.httpResponse.statusCode);
  }


}