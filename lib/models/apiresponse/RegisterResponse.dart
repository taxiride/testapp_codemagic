class RegisterResp{
  final int statusCodeValue;
  final String statusCode;

  RegisterResp({this.statusCodeValue,this.statusCode});

  factory RegisterResp.fromJson(Map<String, dynamic> json){
    return RegisterResp(
    statusCode: json['statusCode'],
    statusCodeValue: json['statusCodeValue']
    );
  }

}

class RegisterRespBody{
  String data;
  String success;
}