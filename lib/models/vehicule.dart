import 'dart:convert';

class Vehicule{
  String brand;
  String carconstructor;
  String cityScope;
  String color;
  String description;
  String firstUseDate;
  int id;
  String language;
  String matriculationNumber;
  String model;
  int numberOfSea;
  int numbersWheel;
  String phoneNumber;
  String travelOption;
  int userId;
  String vehicleType;

  Vehicule({
        this.brand,
        this.carconstructor,
        this.cityScope,
        this.color,
        this.description,
        this.firstUseDate,
        this.id,
        this.language,
        this.matriculationNumber,
        this.model,
        this.numberOfSea,
        this.numbersWheel,
        this.phoneNumber,
        this.travelOption,
        this.userId,
        this.vehicleType
  });

  Vehicule.fromJson(Map<String, dynamic> json){
    brand = json['brand'];
    carconstructor = json['carconstructor'];
    cityScope = json['cityScope'];
    color = json['color'];
    description = json['description'];
    firstUseDate = json['firstUseDate'];
    id = json['id'];
    language = json['language'];
    matriculationNumber = json['matriculationNumber'];
    model = json['model'];
    numberOfSea = json['numberOfSea'];
    numbersWheel = json['numbersWheel'];
    phoneNumber = json['phoneNumber'];
    travelOption = json['travelOption'];
    userId = json['userId'];
    vehicleType = json['vehicleType'];

  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['brand'] = this.brand;
    data['carconstructor'] = this.carconstructor;
    data['cityScope'] = this.cityScope;
    data['color'] = this.color;
    data['description'] = this.description;
    data['firstUseDate'] = this.firstUseDate;
    data['id'] = this.id;
    data['language'] = this.language;
    data['matriculationNumber'] = this.matriculationNumber;
    data['model'] = this.model;
    data['numberOfSea'] = this.numberOfSea;
    data['numbersWheel'] = this.numbersWheel;
    data['phoneNumber'] = this.phoneNumber;
    data['travelOption'] = this.travelOption;
    data['userId'] = this.userId;
    data['vehicleType'] = this.vehicleType;


    return data;
  }

  static List<Vehicule> listFromJson(List<dynamic> json) {
    return List<Vehicule>.from(json.map((v) => Vehicule.fromJson(v)));
  }
}