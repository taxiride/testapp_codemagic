import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/post_lond_distance_trip_dto.dart';

class SaveLongDistanceTripRequest extends AbstractAuthenticatedRequest{
  final PostLongDistanceTripDto postLongDistanceTripDto;
  final String token;

  SaveLongDistanceTripRequest(this.postLongDistanceTripDto, this.token):super(token);

  @override
  String getUrl() {
    return "/api/carpooling/postLongDistanceTrip";
  }

  @override
  String getVerb() {
    return "POST";
  }

  @override
  getBody() {
    return {
      "arrival":postLongDistanceTripDto.arrival,
      "arrivalLatitude":postLongDistanceTripDto.arrivalLatitude,
      "arrivalLongitude":postLongDistanceTripDto.arrivalLongitude,
      "cost":postLongDistanceTripDto.cost,
      "costNegotiable":postLongDistanceTripDto.costNegotiable,
      "country":postLongDistanceTripDto.country,
      "departureDate":postLongDistanceTripDto.departureDate,
      "departure":postLongDistanceTripDto.departure,
      "departureLatitude":postLongDistanceTripDto.departureLatitude,
      "departureLongitude":postLongDistanceTripDto.departureLongitude,
      "emailOrPhone":postLongDistanceTripDto.emailOrPhone,
      "endCity":postLongDistanceTripDto.endCity,
      "language":postLongDistanceTripDto.language,
      "numberOfSeat":postLongDistanceTripDto.numberOfSeat,
      "postDate":postLongDistanceTripDto.postDate,
      "startCity":postLongDistanceTripDto.startCity,
      "travelCondition":postLongDistanceTripDto.travelCondition,
      "tripTimeOut":postLongDistanceTripDto.tripTimeOut
    };

  }

  @override
 int parseResult(AbstractApiResponse response) {
    dynamic data = response.httpResponse.body;
    if(response.httpResponse.statusCode == 200){
      print(jsonDecode(data));
      Map<String,dynamic> responce = jsonDecode(data)["object"];
      return responce["travelId"];
    }else{
      print(response.httpResponse.statusCode);
      print(jsonDecode(data));
      print("--------------------- SAVE LOST ------------------------");
      return null;
    }
  }
}