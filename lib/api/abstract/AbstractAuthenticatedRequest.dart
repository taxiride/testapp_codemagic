

import 'package:taxiride_ios/api/abstract/AbstractApiRequest.dart';

abstract class AbstractAuthenticatedRequest extends AbstractApiRequest {
  final String token;

  AbstractAuthenticatedRequest(this.token);

  @override
  Map<String, String> getAdditionnalHeaders() {
    return {"Authorization": "Bearer ${this.token}"};

  }
}