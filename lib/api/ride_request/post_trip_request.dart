import 'dart:convert';

import 'package:taxiride_ios/api/abstract/AbstractApiResponse.dart';
import 'package:taxiride_ios/api/abstract/AbstractAuthenticatedRequest.dart';
import 'package:taxiride_ios/models/ride.dart';

class PostTripRequest extends AbstractAuthenticatedRequest{
  final Ride ride;
  final String token;

  PostTripRequest(this.ride, this.token):super(token);

  @override
  String getUrl() {
   return "/api/rides/postTrip";
  }

  @override
  String getVerb() {
   return "POST";
  }

  @override
  getBody() {
    return {
      "arrivalPoint": ride.arrivalPoint,
      "availableCashAmount": ride.availableCashAmount,
      "codeOption": ride.codeOption,
      "codePays": ride.codePays,
      "codeVille": ride.codeVille,
      "departurePoint": ride.departurePoint,
      "endPointLatitude": ride.endPointLatitude,
      "endPointLongitude": ride.endPointLongitude,
      "estimatedDuration": ride.estimatedDuration,
      "estimatedTripDistance": ride.estimatedTripDistance,
      "estimatedTripLength": ride.estimatedTripLength,
      "id": ride.id,
      "language": ride.language,
      "phoneNumber": ride.phoneNumber,
      "postDate": ride.postDate,
      "rayon": ride.rayon,
      "riderLattitude": ride.riderLattitude,
      "riderLongitude": ride.riderLongitude,
      "seatNumber": ride.seatNumber,
      "startPointLatitude": ride.startPointLatitude,
      "startPointLongitude": ride.startPointLongitude,
      "tripCost": ride.tripCost,
      "tripTimeOut": ride.tripTimeOut

    };

  }

  @override
  parseResult(AbstractApiResponse response) {
   dynamic data = response.httpResponse.body;
   if(response.httpResponse.statusCode == 200){
     print("----------- save ride succefful ------------");
     print(data);
     Map<String,dynamic> json = jsonDecode(data);
     int id_trip = json['tripId'];
     return id_trip;
   }else{
     print("----------- error error error ------------");
     print(data);
     return null;
   }
  }


}