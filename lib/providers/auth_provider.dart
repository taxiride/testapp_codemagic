import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:taxiride_ios/api/ApiClient.dart';
import 'package:taxiride_ios/api/login_request.dart';
import 'package:taxiride_ios/models/profile.dart';
import 'package:http/http.dart' as http;
class AuthProvider with ChangeNotifier{
  String token;
  int role;
  String phone;
  Profil profil;

  get tokenKey async {
    if(token != null){
      return token;
    }
    final prefs = await SharedPreferences.getInstance();

    String toke = await  prefs.getString('access_token');
    return toke;
  }

  get username async{
    if(phone !=null){
      return phone;
    }
    final prefs = await SharedPreferences.getInstance();

    String phoneN = await  prefs.getString('username');
    return phoneN;
  }

  setInfosUser(String _phone,String _token) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('username',_phone);
    await  prefs.setString('access_token',_token);
    await prefs.setBool('is_connected', true);
    token = _token;
    phone = _phone;
    notifyListeners();
  }

  setRole(int value){
    role = value;
    notifyListeners();
  }

  setProfil(Profil _profil){
    profil = _profil;
    notifyListeners();
  }
logout(){
    token = null;
    phone = null;
    profil = null;
    role = null;
    notifyListeners();
}

login(String pass,String username) async {
    LoginRequest loginRequest = LoginRequest(password: pass,username: username);
    loginRequest.parseResult(await ApiClient.execOrFail(loginRequest));
}

  Future<Profil> findProfilByPhoneNumber(String phone,String token) async {
   print("-----------  token ---------");
    print(token);

    var uri =Uri.https('app.taxiride.biz:81', '/api/profile/findbyphonenumber/$phone/fr');
    final http.Response response = await http.get(
      uri,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: 'Bearer '+token,
      },

    );


    if(response.statusCode == 200){
      print(response.body);
      Profil user = Profil.fromJson(jsonDecode(response.body));
      setProfil(user);
      print("----- profil found with succefull -------");
      return user;
    }else{
      print("---------  error ------------");
      return null;
    }

  }




}